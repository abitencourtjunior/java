/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lpoo1;

import java.io.PrintStream;
import java.util.Calendar;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Lpoo1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ProfessorEfetivo altamir = new ProfessorEfetivo("Altamir","ALT0001X",1998,40,1580f);
        altamir.dados();
        altamir.salario();
        
        ProfessorHorista nei = new ProfessorHorista("Nei","NEI0001X",1852,40,150.3f);
        nei.dados();
        nei.salario();
    }
    
}
