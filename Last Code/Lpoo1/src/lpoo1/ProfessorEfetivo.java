/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lpoo1;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class ProfessorEfetivo extends Professor {
    
    private int jornadaDeTrabalho;
    private float salario;
    
    //Metodo Construtor
    public ProfessorEfetivo(String nome, String matricula, int data, int jornadaDeTrabalho, float salario){
      super (nome, matricula, data);
      this.jornadaDeTrabalho = jornadaDeTrabalho;
      this.salario = salario;
    }
    
    public ProfessorEfetivo(){
        super();
    }
    
    // Método para Jornada de Trabalho
    private void setJornadaDeTrabalho( int jornadaDeTrabalho){
        this.jornadaDeTrabalho = jornadaDeTrabalho;
    }
    
    private int getJornadaDeTrabalho(){
        
        if (this.jornadaDeTrabalho == 20){
            return this.jornadaDeTrabalho;
        }
        else if( this.jornadaDeTrabalho == 40){
            return this.jornadaDeTrabalho;
        } 
        else if (this.jornadaDeTrabalho > 40) {
            return this.jornadaDeTrabalho;
        }
        return 0;
    }
    
    //Metodo Salário
    
    public void setSalario( float salario){
        this.salario = salario;
    }
    
    public float getSalario(){    
        return this.salario;
    }
    
    //Mostrar Dados

    public void salario(){
        System.out.println("O salario é: " + this.getSalario());
        System.out.println("Jornada de Trabalho: " + this.getJornadaDeTrabalho() + " horas.");
    }
    
    
    
}
