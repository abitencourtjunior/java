/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lpoo1;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class ProfessorHorista extends Professor {
    
    private int horasTrabalhadas;
    private float salarioHora;
    
    //Métodos contrutores
    public ProfessorHorista(String nome, String matricula, int data, int horasTrabalhadas, float salarioHora){
        
        super(nome,matricula,data);
        this.horasTrabalhadas = horasTrabalhadas;
        this.salarioHora = salarioHora;

    }
    
    public ProfessorHorista(){
        super();
    }
    
    //Métodos para HorasTrabalhadas
    
    public void setHorasTrabalhas( int horasTrabalhadas){
        this.horasTrabalhadas = horasTrabalhadas;
    }
    
    public int getHorasTrabalhas(){
       return this.horasTrabalhadas;
    }
    
    //Método para SalarioHora
    public void setSalarioHora (float salarioHora){
       
        this.salarioHora = salarioHora; 
    }
    
    public float getSalarioHora(){
        
        this.salarioHora = this.salarioHora * this.horasTrabalhadas;
        
        return this.salarioHora;
    }
    
    //Salário Dados
    public void salario(){
        System.out.println("O salario é: "+this.getSalarioHora());
    }
}
