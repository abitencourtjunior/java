/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lpoo1;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Professor {
    
    private String nome;
    private String matricula;
    private int data;
    
    
    public Professor (String nome, String matricula, int data){
        this.nome = nome;
        this.matricula = matricula;
        this.data = data;
    }
    
    public Professor(){}
    
    // Método Nome
    public void setNome(String nome){
        this.nome = nome; 
    }
    
    public String getNome(){
        return nome;
    }
    
    //Metodo Matricula
    public void setMatricula(String matricula){
        this.matricula = matricula;
    }

    public String getMatricula(){
        return matricula;
    }
    
    //Método da Data de Nascimento
    public void setData(int data){
        this.data = data; 
    }
   
    public int getData(){
        Calendar hoje = new GregorianCalendar();
        hoje.setTime(new Date());
        int idade = hoje.get(Calendar.YEAR) - this.data;
        
        return idade; 
    }
    
    public void dados(){
        System.out.println("Sobre o professor");
        System.out.println("Nome: "+ this.getNome());
        System.out.println("Matricula: "+ this.getMatricula());
        System.out.println("Idade: "+this.getData());
     }
    
 
}
            