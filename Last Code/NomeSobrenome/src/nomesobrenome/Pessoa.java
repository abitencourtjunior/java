/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nomesobrenome;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Pessoa {
    
    private String nome;
    private String sobreNome;
    private int idade;
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setSobreNome(String sobreNome){
        this.sobreNome = sobreNome;
    }
    
    public String getSobreNome(){
        return this.sobreNome;
    }
    
    public void setIdade( int idade){
        this.idade = idade;
    }
    
    public int getIdade(){
        return this.idade;
    }
    
    @Override
    public String toString(){
        String mensagem = this.nome +" "+ this.sobreNome +" "+ this.idade;
        return mensagem;
    }
}
