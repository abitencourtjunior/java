/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dicacftv;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;



/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Procedimentos {
    
    private String titulo;
    private String modelo;
    private String situacao;
    private String acao;
   // private Calendar data;
    
    public Procedimentos (){
        
    }
    public Procedimentos(String titulo, String modelo, String situacao, String acao){
        this.modelo = modelo;
        this.titulo = titulo;
        this.situacao = situacao;
        this.acao = acao;
     
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the situacao
     */
    public String getSituacao() {
        return situacao;
    }

    /**
     * @param situacao the situacao to set
     */
    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    /**
     * @return the acao
     */
    public String getAcao() {
        return acao;
    }

    /**
     * @param acao the acao to set
     */
    public void setAcao(String acao) {
        this.acao = acao;
    }


    
    
    
}
