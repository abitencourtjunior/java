/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package revisao;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Revisao {

    // Exercicio Slide 2 
    
    public int calculo(double valor, int x){
        int cont = (int) valor;
        int soma = 1;
        int funcao = 0;
        
        for (int aux = (int) (valor - 1); aux>=1 ;aux-- ){
            cont *= aux;
            
            int contador = 0;
            contador += 1;
            soma += Math.pow(valor, contador)/cont;                               
        } 
        
        return soma;
    }
    
    
    /**
     * @param args the command line arguments
     */
    
    
    
    //Exercicio 03
    
    public double densidade(double x, double m, double o){
        double e = 2.71828;
        
        double divisao = ((x-m)/o);
        divisao = Math.pow(e,(Math.pow(divisao, 2) * -0.5));
       
        double raiz = o*(Math.sqrt(2*3.14));
        
        double total = divisao / raiz;
        
        return total;
    }
    
    // Exercicio 02 
    
    public  double poisson ( double l, int k){
        int valor = k;
        double e = 2.71828;
        int cont = k;  
        
        for (int aux = (valor - 1); aux>=1; aux-- ){
           cont *=aux;        
        } 
        
        e = (Math.pow(e, -l)*Math.pow(l, k))/cont;
        
        return e;
    }
    
    // Exercicio 01
    
    public int telescopio(int numero){
        int total = 0;
        
        for (int i=1; i<=numero; i++){
            
            total += (2*i+3)/(i+1)*(i+2);   
        }
        
        return total;
    }
    
    // Strings
    
    // 01 
    
    public int contagem(String palavra){
        int cont =0;
        for (int i=0; i<palavra.length();i++){
            
            if (Character.isLetter(palavra.charAt(i))){
                cont++;
            } 
        }
        return cont;
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        Revisao n = new Revisao();
        
        System.out.println(n.calculo(5,2));
        System.out.println(n.densidade(1, 1, 1));
        System.out.println(n.poisson(4, 5));
        System.out.println(n.telescopio(1));
        System.out.println(n.contagem("Abacaxi"));
        
    }
    
}
