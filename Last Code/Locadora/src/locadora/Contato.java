/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Contato {
    
    private int idContato;
    private String TelefonePrincipal;
    private String TelefoneSecundario;
    private String email;
    
    public Contato(){
        
    }
        
    public Contato(int idContato, String TelefonePrincipal, String TelefoneSecundario, String email){
        this.idContato = idContato;
        this.TelefonePrincipal = TelefonePrincipal;
        this.TelefoneSecundario = TelefoneSecundario;
        this.email = email;
    }

    /**
     * @return the idContato
     */
    public int getIdContato() {
        return idContato;
    }

    /**
     * @param idContato the idContato to set
     */
    public void setIdContato(int idContato) {
        this.idContato = idContato;
    }

    /**
     * @return the TelefonePrincipal
     */
    public String getTelefonePrincipal() {
        return TelefonePrincipal;
    }

    /**
     * @param TelefonePrincipal the TelefonePrincipal to set
     */
    public void setTelefonePrincipal(String TelefonePrincipal) {
        this.TelefonePrincipal = TelefonePrincipal;
    }

    /**
     * @return the TelefoneSecundario
     */
    public String getTelefoneSecundario() {
        return TelefoneSecundario;
    }

    /**
     * @param TelefoneSecundario the TelefoneSecundario to set
     */
    public void setTelefoneSecundario(String TelefoneSecundario) {
        this.TelefoneSecundario = TelefoneSecundario;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
