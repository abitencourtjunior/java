/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import java.util.Date;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Autenticacao {
    
    private int idAutenticacao;
    private String Usuario;
    private String Senha;
    private Date DataCriacao;
    private String TipoUsuario;
    
    public Autenticacao(){
        
    }
    
    public Autenticacao(int idAutenticacao, String Usuario, String Senha, Date DataCriacao, String TipoUsuario){
        this.idAutenticacao = idAutenticacao;
        this.Usuario = Usuario;
        this.Senha = Senha;
        this.DataCriacao = DataCriacao;
        this.TipoUsuario = TipoUsuario;
    }

    /**
     * @return the idAutenticacao
     */
    public int getIdAutenticacao() {
        return idAutenticacao;
    }

    /**
     * @param idAutenticacao the idAutenticacao to set
     */
    public void setIdAutenticacao(int idAutenticacao) {
        this.idAutenticacao = idAutenticacao;
    }

    /**
     * @return the Usuario
     */
    public String getUsuario() {
        return Usuario;
    }

    /**
     * @param Usuario the Usuario to set
     */
    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    /**
     * @return the Senha
     */
    public String getSenha() {
        return Senha;
    }

    /**
     * @param Senha the Senha to set
     */
    public void setSenha(String Senha) {
        this.Senha = Senha;
    }

    /**
     * @return the DataCriacao
     */
    public Date getDataCriacao() {
        return DataCriacao;
    }

    /**
     * @param DataCriacao the DataCriacao to set
     */
    public void setDataCriacao(Date DataCriacao) {
        this.DataCriacao = DataCriacao;
    }

    /**
     * @return the TipoUsuario
     */
    public String getTipoUsuario() {
        return TipoUsuario;
    }

    /**
     * @param TipoUsuario the TipoUsuario to set
     */
    public void setTipoUsuario(String TipoUsuario) {
        this.TipoUsuario = TipoUsuario;
    }
    
    
    
}
