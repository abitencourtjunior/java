/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import java.util.Date;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Funcionario extends Pessoa {
    
    private int idFuncionario;
    private Endereco idEndereco;
    private Contato idContato;
    private Autenticacao idAutenticacao;
    private Cargo idCargo;
    
    public Funcionario(String Nome, Date DataNascimento, String CPF){
        super(Nome, DataNascimento, CPF);
        this.idFuncionario = idFuncionario;
        this.idContato = idContato;
        this.idEndereco = idEndereco;
        this.idAutenticacao = idAutenticacao;
        this.idCargo = idCargo;
    }
    
    public Funcionario(){
        super();
    }
    
    public int getIdFuncionario() {
        return idFuncionario;
    }

    public void setIdFuncionario(int idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

    /**
     * @return the idEndereco
     */
    public Endereco getIdEndereco() {
        return idEndereco;
    }

    /**
     * @param idEndereco the idEndereco to set
     */
    public void setIdEndereco(Endereco idEndereco) {
        this.idEndereco = idEndereco;
    }

    /**
     * @return the idContato
     */
    public Contato getIdContato() {
        return idContato;
    }

    /**
     * @param idContato the idContato to set
     */
    public void setIdContato(Contato idContato) {
        this.idContato = idContato;
    }

    /**
     * @return the idAutenticacao
     */
    public Autenticacao getIdAutenticacao() {
        return idAutenticacao;
    }

    /**
     * @param idAutenticacao the idAutenticacao to set
     */
    public void setIdAutenticacao(Autenticacao idAutenticacao) {
        this.idAutenticacao = idAutenticacao;
    }

    public void setIdCargo(Cargo idCargo) {
        this.idCargo = idCargo;
    }

    public Cargo getIdCargo() {
        return idCargo;
    }
    
    
    
}
