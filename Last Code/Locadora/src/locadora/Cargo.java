/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Cargo {
    
    private int idCargo;
    private String Cargo;
    private float Salario;
    
    public Cargo (){
        
    }
    
    public Cargo(int idCargo, String Cargo, float Salario){
        this.idCargo = idCargo;
        this.Cargo = Cargo;
        this.Salario = Salario;
    }

    /**
     * @return the idCargo
     */
    public int getIdCargo() {
        return idCargo;
    }

    /**
     * @param idCargo the idCargo to set
     */
    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    /**
     * @return the Cargo
     */
    public String getCargo() {
        return Cargo;
    }

    /**
     * @param Cargo the Cargo to set
     */
    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    /**
     * @return the Salario
     */
    public float getSalario() {
        return Salario;
    }

    /**
     * @param Salario the Salario to set
     */
    public void setSalario(float Salario) {
        this.Salario = Salario;
    }
    
    
    
}
