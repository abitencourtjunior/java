/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import java.util.Date;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public abstract class Pessoa {
    
    private String Nome;
    private Date DataNascimento;
    private String CPF;
    
    
    public Pessoa(){
        
    }
    
    public Pessoa(String Nome, Date DataNascimento, String CPF){
        this.Nome = Nome;
        this.DataNascimento = DataNascimento;
        this.CPF = CPF;
    }
    
    // Métodos 
 
   
    /**
     * @return the nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    /**
     * @return the dataNascimento
     */
    public Date getDataNascimento() {
        return DataNascimento;
    }

    /**
     * @param DataNascimento
     */
    
    public void setDataNascimento(Date DataNascimento) {
        this.DataNascimento = DataNascimento;
    }

    /**
     * @return the cpf
     */
    public String getCpf() {
        return CPF;
    }

    /**
     * @param CPF
     */
    
    public void setCpf(String CPF) {
        this.CPF = CPF;
    }
    
    
    
    
}
