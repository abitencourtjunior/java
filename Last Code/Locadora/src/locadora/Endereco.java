/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Endereco {
    
    private int idEndereco;
    private String NomeRua;
    private String CEP;
    private String Bairro;
    private String Cidade;
    private String Complemento;
    
    
    public Endereco(){
        
    }
    
    public Endereco(int idEndereco, String NomeRua, String CEP, String Bairro, String Cidade, String Complemento){
        this.idEndereco = idEndereco;
        this.NomeRua = NomeRua;
        this.CEP = CEP;
        this.Bairro = Bairro;
        this.Cidade = Cidade;
        this.Complemento = Complemento;
    }
    
    
    
    public void setIdEndereco(int idEndereco){
        this.idEndereco = idEndereco;
    }
    
    public int getIdEndereco(){
        return idEndereco;
    }
    
    
    
    public void setNomeRua(String NomeRua){
        this.NomeRua = NomeRua;
    }
    
    public String getNomeRua(){
        return NomeRua;
    }
    
    
    
    public void setCEP(String CEP){
        this.CEP = CEP;
    }
    
    public String CEP(){
        return CEP;
    }



    public void setBairro(String Bairro) {
        this.Bairro = Bairro;
    }

    public String getBairro() {
        return Bairro;
    }

    
    
    public void setCidade(String Cidade) {
        this.Cidade = Cidade;
    }

    public String getCidade() {
        return Cidade;
    }

    
    
    public void setComplemento(String Complemento) {
        this.Complemento = Complemento;
    }
      
    public String getComplemento() {
        return Complemento;
    }
    
    
    
    
    
        
    
       
    
}
