/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class PessoaJuridica {
    
    private int idPessoaJuridica;
    private String Nome;
    private String NomeFantasia;
    private String CNPJ;
    private PessoaFisica idPessoaFisica;
    private Contato idContato;
    private Endereco idEndereco;
    private boolean Seguradora;
    
    public PessoaJuridica(){
        
    }
    
    public PessoaJuridica (int idPessoaJuridica, String Nome,String NomeFantasia, String CNPJ, PessoaFisica idPessoaFisica, Contato idContato,Endereco idEndereco, boolean Seguradora){
        this.idPessoaJuridica = idPessoaJuridica;
        this.Nome = Nome;
        this.NomeFantasia = NomeFantasia;
        this.CNPJ = CNPJ;
        this.idPessoaFisica = idPessoaFisica;
        this.idEndereco = idEndereco;
        this.idContato = idContato;
        this.Seguradora = Seguradora;
    }

    /**
     * @return the idPessoaJuridica
     */
    public int getIdPessoaJuridica() {
        return idPessoaJuridica;
    }

    /**
     * @param idPessoaJuridica the idPessoaJuridica to set
     */
    public void setIdPessoaJuridica(int idPessoaJuridica) {
        this.idPessoaJuridica = idPessoaJuridica;
    }

    /**
     * @return the Nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    /**
     * @return the NomeFantasia
     */
    public String getNomeFantasia() {
        return NomeFantasia;
    }

    /**
     * @param NomeFantasia the NomeFantasia to set
     */
    public void setNomeFantasia(String NomeFantasia) {
        this.NomeFantasia = NomeFantasia;
    }

    /**
     * @return the CNPJ
     */
    public String getCNPJ() {
        return CNPJ;
    }

    /**
     * @param CNPJ the CNPJ to set
     */
    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    /**
     * @return the idPessoaFisica
     */
    public PessoaFisica getIdPessoaFisica() {
        return idPessoaFisica;
    }

    /**
     * @param idPessoaFisica the idPessoaFisica to set
     */
    public void setIdPessoaFisica(PessoaFisica idPessoaFisica) {
        this.idPessoaFisica = idPessoaFisica;
    }

    /**
     * @return the idContato
     */
    public Contato getIdContato() {
        return idContato;
    }

    /**
     * @param idContato the idContato to set
     */
    public void setIdContato(Contato idContato) {
        this.idContato = idContato;
    }

    /**
     * @return the idEndereco
     */
    public Endereco getIdEndereco() {
        return idEndereco;
    }

    /**
     * @param idEndereco the idEndereco to set
     */
    public void setIdEndereco(Endereco idEndereco) {
        this.idEndereco = idEndereco;
    }

    /**
     * @return the Seguradora
     */
    public boolean isSeguradora() {
        return Seguradora;
    }

    /**
     * @param Seguradora the Seguradora to set
     */
    public void setSeguradora(boolean Seguradora) {
        this.Seguradora = Seguradora;
    }
    
}
