/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import java.util.Date;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class PessoaFisica extends Pessoa {
    
    private int idPessoaFisica;
    private String CNH;
    private String CHA;
    private String CategoriaHab;
    private Contato idContato;
    private Endereco idEndereco;
    
    
    public PessoaFisica(){
        super();
    }
    
    public PessoaFisica(String Nome, Date DataNascimento, String CPF){
        super (Nome,DataNascimento, CPF);
        this.idPessoaFisica = idPessoaFisica;
        this.CHA = CHA;
        this.CNH = CNH;
        this.CategoriaHab = CategoriaHab;
        this.idContato = idContato;
        this.idEndereco = idEndereco;
    }

    /**
     * @return the idPessoaFisica
     */
    public int getIdPessoaFisica() {
        return idPessoaFisica;
    }

    /**
     * @param idPessoaFisica the idPessoaFisica to set
     */
    public void setIdPessoaFisica(int idPessoaFisica) {
        this.idPessoaFisica = idPessoaFisica;
    }
    
    /**
     * @return the CNH
     */
    public String getCNH() {
        return CNH;
    }

    /**
     * @param CNH the CNH to set
     */
    public void setCNH(String CNH) {
        this.CNH = CNH;
    }

    /**
     * @return the CHA
     */
    public String getCHA() {
        return CHA;
    }

    /**
     * @param CHA the CHA to set
     */
    public void setCHA(String CHA) {
        this.CHA = CHA;
    }

    /**
     * @return the CategoriaHab
     */
    public String getCategoriaHab() {
        return CategoriaHab;
    }

    /**
     * @param CategoriaHab the CategoriaHab to set
     */
    public void setCategoriaHab(String CategoriaHab) {
        this.CategoriaHab = CategoriaHab;
    }

    /**
     * @return the idContato
     */
    public Contato getIdContato() {
        return idContato;
    }

    /**
     * @param idContato the idContato to set
     */
    public void setIdContato(Contato idContato) {
        this.idContato = idContato;
    }

    /**
     * @return the idEndereco
     */
    public Endereco getIdEndereco() {
        return idEndereco;
    }

    /**
     * @param idEndreco the idEndereco to set
     */
    public void setIdEndereco(Endereco idEndereco) {
        this.idEndereco = idEndereco;
    }

   
}
