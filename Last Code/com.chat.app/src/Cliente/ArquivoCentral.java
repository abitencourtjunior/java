/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public abstract class ArquivoCentral {
    
    private  int central;
    private String nome;
    private Cliente outputStream;
    private String tipo;
    private String extensao;
    private File file;
    
    public ArquivoCentral (String nome, int central, String tipo, String extensao) throws IOException { // Método para enviar os arquivos 
            
       JFileChooser fileChooser = new JFileChooser();
        
        fileChooser.setFileFilter(new FileNameExtensionFilter( tipo, extensao));      
        fileChooser.setAcceptAllFileFilterUsed(false);
        
        int opt = fileChooser.showOpenDialog(null); // Abre a janela para selecionar o arquivo.
        
        if(opt == JFileChooser.APPROVE_OPTION){
           file = fileChooser.getSelectedFile();
        }
 
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(File file) {
        this.file = file;
    }
    
}
