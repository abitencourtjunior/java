/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import bean.FileMessage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.channels.FileChannel;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Cliente {
    
   private Socket socket;
   private ArquivoAudio audio;
   private ArquivoVideo video;
   private ArquivoFoto foto;
   private ArquivoTexto texto;
   ObjectOutputStream outputStream;
   

    public Cliente() throws IOException {
        this.socket = new Socket("localhost",5555);
        this.outputStream = new ObjectOutputStream(socket.getOutputStream());
        
        new Thread(new ListenerSocket(socket)).start();
        
        menu();
    }

    private void menu() throws IOException {
        
        Scanner scanner  = new Scanner(System.in);
        System.out.println(" Digite seu nome: ");
        
        String nome = scanner.nextLine();
        
        this.outputStream.writeObject(new FileMessage(nome));
        
            int option = 0;
                while(option != 1){
                    System.out.print("1 - Sair | 2 - Enviar Arquivos | 3 - Mais Opções: ");
                    option = scanner.nextInt();
            switch (option) {
                case 2:
                    //send(nome);
                    System.out.print("Selecione o tipo de arquivo: \n");
                    System.out.print("1 - Audio | 2 - Video | 3 - Foto | 4 - Texto | 5 - Sair");
                    // TRATAMENTO DE ARQUIVOS TIPO AUDIO,FOTO,TEXTO,VIDEO E SAIR.
                    
                    int suboption = 0;
                    suboption = scanner.nextInt();
                    switch (suboption) {
                        case 1:
                            @SuppressWarnings("LocalVariableHidesMemberVariable")
                            ArquivoAudio audio = new ArquivoAudio(nome, suboption,"Audio","m4a");
                            this.outputStream.writeObject(new FileMessage(nome,audio.getFile()));
                            break;
                        case 2:
                            @SuppressWarnings("LocalVariableHidesMemberVariable")
                            ArquivoVideo video = new ArquivoVideo(nome, suboption, "Video","avi");
                            this.outputStream.writeObject(new FileMessage(nome,video.getFile()));
                            break;
                        case 3:
                            @SuppressWarnings("LocalVariableHidesMemberVariable")
                            ArquivoFoto foto = new ArquivoFoto(nome, suboption, "Foto", "png");
                            this.outputStream.writeObject(new FileMessage(nome,foto.getFile()));
                            break;
                        case 4:
                            @SuppressWarnings("LocalVariableHidesMemberVariable")
                            ArquivoTexto texto = new ArquivoTexto(nome, suboption, "Texto", "doc");
                            this.outputStream.writeObject(new FileMessage(nome,texto.getFile()));
                            break;
                        default:
                            System.exit(0);
                    }
                    break;
                case 1:
                    System.exit(0);
                case 3:
                    System.out.print("Digite a mensagem: ");
                    String msg = scanner.nextLine();
                    this.outputStream.writeObject(msg);
                    break;
                default:
                    System.exit(0);
            }
            }
    }

    void writeObject(FileMessage fileMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private class ListenerSocket implements Runnable {

        
        private ObjectInputStream inputStream;
        
        
        public ListenerSocket(Socket socket) throws IOException {
            this.inputStream = new ObjectInputStream(socket.getInputStream());
        }

        @Override
        public void run() {
            FileMessage message = null;
                
                try {
                    while((message = (FileMessage) inputStream.readObject()) != null){
                        System.out.println("\nVocê recebeu um arquivo de" + message.getCliente());
                        System.out.println("O arquivo é: " + message.getFile().getName());
                        
                        imprime(message);
                        
                        salvarArquivo(message);
                        
                        System.out.print("1 - Sair | 2 - Enviar: ");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
        }

        private void imprime(FileMessage message) {
            try {
                FileReader fileReader = new FileReader(message.getFile());
                BufferedReader bufferReader = new BufferedReader(fileReader);
                
                String linha;
                while((linha = bufferReader.readLine())!= null){
                    System.out.println(linha);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @SuppressWarnings("null")
        private void salvarArquivo(FileMessage message) {
            FileOutputStream fileOutputStream = null;
            try {
                
                Thread.sleep(new Random().nextInt(1000));
                
                long time = System.currentTimeMillis();
                
                FileInputStream fileInputStream = new FileInputStream(message.getFile());
                fileOutputStream = new FileOutputStream("c:\\Teste\\"+time+"_"+ message.getFile().getName());
                
                FileChannel fin = fileInputStream.getChannel();
                FileChannel fout = fileOutputStream.getChannel();
                
                long size = fin.size();
                
                fin.transferTo(0, size, fout);
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException | InterruptedException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }




    }
   
    public static void main(String[] args) {
       try {
           new Cliente();
       } catch (IOException ex) {
           Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
}
