/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.File;
import java.io.Serializable;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class FileMessage implements Serializable { 
    
    private String cliente;
    private File file;
    private String mensagem;
    
    public FileMessage(String cliente, File file){
       this.cliente = cliente;
       this.file = file;
    }
    
   
    public FileMessage(String cliente){
        this.cliente = cliente;
    }
    
    public FileMessage(){
    }

    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }
    
    public FileMessage(String cliente, String mensagem){
        this.cliente = cliente;
        this.mensagem = mensagem;
    }
    
    
}
