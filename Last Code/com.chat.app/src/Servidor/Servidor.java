/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import bean.FileMessage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Servidor {
    
    private ServerSocket serverSocket;
    private Socket socket;
    @SuppressWarnings("Convert2Diamond")
    private Map<String, ObjectOutputStream> streamMap = new HashMap<String, ObjectOutputStream>(); 
    // Adicionando todos os usuário a uma lista que por parametro irá entrar a String com o nome do cliente 
    // e também o fluxo da mensagem

    public Servidor() {
        try {
            serverSocket = new ServerSocket(5555); // Inicializando o servidor!!!
            System.out.println("Server on!!");
            
            while(true){
                socket = serverSocket.accept();
                new Thread(new ListenerSocket(socket)).start(); // Criação sempre de uma nova Thread quando 
                                                                // um usuário se conecta no servidor;
            }
   
                    
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private class ListenerSocket implements Runnable {
        
        private ObjectOutputStream outputStream;
        private ObjectInputStream inputStream;
        
        public ListenerSocket(Socket socket) throws IOException {
            this.inputStream = new ObjectInputStream(socket.getInputStream());
            this.outputStream = new ObjectOutputStream(socket.getOutputStream());
        }

        @Override
        public void run() {
            FileMessage message = null;
            try {
                
                while((message = (FileMessage) inputStream.readObject()) != null){
                    streamMap.put(message.getCliente(), outputStream);
                    if(message.getFile() != null){
                        for(Map.Entry<String, ObjectOutputStream> kv: streamMap.entrySet()){ // Valida para que usuário vai mandar a mensagem de acordo com a sua lista.
                            if(!message.getClass().equals(kv.getKey())){
                                kv.getValue().writeObject(message);
                            }
                        }
                    }
                
                }
            } catch (IOException ex) {
               streamMap.remove(message.getCliente());
                System.out.println(message.getCliente() + " saiu!");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        
    }
  public static void main(String[] args) {
        new Servidor();
 
    }
}
  
