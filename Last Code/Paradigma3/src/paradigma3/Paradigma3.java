/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paradigma3;

import java.util.Scanner;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */

public class Paradigma3 {

    /**
     * @param var
     * @return 
     */
  
    public static String trocar(String var) {
        String temp = "";
        
        for(int i= 0;i<var.length();i++){
        
            char aux = var.charAt(i);
        
                if (aux == '0'){
                    temp += "1";
                } else if (aux == '1'){
                     temp += "0";
                } else {
                    temp += var.charAt(i);
                }
        
            }
        return temp;
    }
    
    /**
     *
     * @param args
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        String var = new String();
        Scanner ler = new Scanner(System.in);
        
        System.out.print("Digite uma string: ");
        var = ler.next();

        System.out.println(trocar(var));
        
        
    }
    
}
