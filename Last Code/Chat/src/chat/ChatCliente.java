/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */



public class ChatCliente extends JFrame {
    
    JTextField textoParaEnviar;
    Socket socket;
    PrintWriter escreve;
    String nome;
    JTextArea textoRecebido;
    Scanner leitor;
    
    private class EscutaServidor implements Runnable{
    
    @Override
    public void run() {
        try {
            String text;
        while ((text = leitor.nextLine()) != null){
            textoRecebido.append(text + "\n");
            }
        } catch(Exception x){}
       
    }
   
}
    
    public ChatCliente(String nome) throws IOException{
        super("Chat" + nome);
        
          
        
        textoParaEnviar = new JTextField();
        JButton btn = new JButton("Enviar");
        Container envio = new JPanel();
        envio.setLayout(new BorderLayout());
        envio.add(BorderLayout.CENTER, textoParaEnviar);
        envio.add(BorderLayout.EAST, btn);
        btn.addActionListener(new EnviarListener());
        
        textoRecebido = new JTextArea();
        JScrollPane scroll = new JScrollPane(textoRecebido);
        
        getContentPane().add(BorderLayout.CENTER, scroll);
        getContentPane().add(BorderLayout.SOUTH, envio);
        
        configurarRede();
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        setVisible(true);
        
    }
    
    private class EnviarListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            escreve.println(textoParaEnviar.getText());
            escreve.flush();
            textoParaEnviar.setText("");
            textoParaEnviar.requestFocus();
        }
    
    
    }

    private void configurarRede() throws IOException{
     try{
        socket = new Socket("127.0.0.1",4000);
        escreve = new PrintWriter(socket.getOutputStream());
        leitor = new Scanner(socket.getInputStream());
        new Thread(new EscutaServidor()).start();
        
     }catch(IOException e){}
     
    }

    
    public static void main(String[] args) throws IOException {
        new ChatCliente("Altamir");
        new ChatCliente("Vitoria");
    }
}
