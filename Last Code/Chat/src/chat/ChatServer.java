/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class ChatServer {
        
    List<PrintWriter> escritores = new ArrayList<>();
    
    public ChatServer(){
        
        
        // Criação do Servidor 
        ServerSocket server;
        Scanner ler;

            try {
                server = new ServerSocket(4000);

                while(true){
                    Socket s = server.accept();
                    new Thread(new EscutaCliente(s)).start();
                    PrintWriter p = new PrintWriter(s.getOutputStream());
                    escritores.add(p);
                }
            } catch (IOException e) {}
        }
    private void encaminharParaTodos(String texto){
    
    for( PrintWriter w : escritores){
        try {
                w.print(texto);
                w.flush();
        } catch ( Exception e){}
    }
}
    private class EscutaCliente implements Runnable{
       
        Scanner leitor;
        String texto;
       
        public EscutaCliente(Socket socket){
            try{
            leitor = new Scanner(socket.getInputStream());
            }
            catch ( Exception e){}
        }
        
        
        @Override
        public void run() {
            try{
                    while((texto = leitor.nextLine()) != null){
                        System.out.println("Mensagem: " + texto);
                        encaminharParaTodos(texto);
                        
                }
            } catch(Exception e){}
            
        }
        
    }
    
    public static void main(String[] args) {
     
        new ChatServer();
        

    }
}

        
    
    

