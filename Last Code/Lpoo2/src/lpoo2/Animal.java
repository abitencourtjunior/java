/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lpoo2;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Animal {
    
    private String nomeAnimal;
    private String classeAnimal;

 
    public Animal(String nomeAnimal, String classeAnimal){
        this.classeAnimal = classeAnimal;
        this.nomeAnimal = nomeAnimal;
    }
    public Animal(){}
    
    
    public void imprimirDados(){
        System.out.println("Nome: "+this.getNomeAnimal());
        System.out.println("Classe: "+this.getClasseAnimal());
    }
    
    //Método para NomeAnimal
    public void setNomeAnimal(String nomeAnimal){
        this.nomeAnimal = nomeAnimal;
    }
    
    public String getNomeAnimal(){
        return this.nomeAnimal;                
    }
    
    //Método para ClasseAnimal
    public void setClasseAnimal(String classeAnimal){
        this.classeAnimal = classeAnimal;
    }
    
    public String getClasseAnimal(){
        return this.classeAnimal;
    }
    
    public String falar(){
        return "";        
    }
}
