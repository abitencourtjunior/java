/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lpoo2;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Cachorro extends Mamifero {
    
    private String latido;
    private int correr;
    
    public Cachorro (String nomeAnimal, String classeAnimal){
        super(nomeAnimal, classeAnimal);
        this.correr = correr;
        this.latido = latido;
    }
    
    public Cachorro(){
        super();
    }
    
    public void setVolumeLatido(String latido){
        this.latido = latido;
    }
    
    public String getVolumeLatido(){
        return this.latido;
    }
    
    public void setCorrer(int correr){
        this.correr = correr;
    }
    public int getCorrer(){
        return this.correr;
    }
    
}
