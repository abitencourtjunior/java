/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatnovo;

import chatnovo.FileMessage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class Cliente {
    
   private Socket socket;
   private ObjectOutputStream outputStream;

    public Cliente() throws IOException {
        this.socket = new Socket("127.0.0.1",6000);
        this.outputStream = new ObjectOutputStream(socket.getOutputStream());
        
        new Thread(new ListenerSocket(socket)).start();
        
        menu();
    }

    private void menu() throws IOException {
        
        Scanner scanner  = new Scanner(System.in);
        System.out.println(" Digite seu nome: ");
        
        String nome = scanner.nextLine();
        
        this.outputStream.writeObject(new FileMessage(nome));
        
            int option = 1;
                while(option != 1){
                    System.out.print("1 - Sair | 2 - Enviar");
                    option = scanner.nextInt();
                    if (option == 2){
                        send(nome); // Metodo send -- Criar uma classe;
                    } else if(option == 1){
                        System.exit(0);
                    }
            }
    }

    private void send(String nome) throws IOException { // Método para enviar os arquivos 
            
        JFileChooser fileChooser = new JFileChooser();
        
        int opt = fileChooser.showOpenDialog(null); // Abre a janela para selecionar o arquivo.
        
        if(opt == JFileChooser.APPROVE_OPTION){
            File file = fileChooser.getSelectedFile();
            
            this.outputStream.writeObject(new FileMessage(nome,file));
        }
 
    }

    
    
    private class ListenerSocket implements Runnable {

        
        private ObjectInputStream inputStream;
        
        
        public ListenerSocket(Socket socket) throws IOException {
            this.inputStream = new ObjectInputStream(socket.getInputStream());
        }

        @Override
        public void run() {
            FileMessage message = null;
                
                try {
                    while((message = (FileMessage) inputStream.readObject()) != null){
                        System.out.println("\nVocê recebeu um arquivo de" + message.getCliente());
                        System.out.println("O arquivo é" + message.getFile().getName());
                        
                        imprime(message);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
        }

        private void imprime(FileMessage message) {
            try {
                FileReader fileReader = new FileReader(message.getFile());
                BufferedReader bufferReader = new BufferedReader(fileReader);
                
                String linha;
                while((linha = bufferReader.readLine())!= null){
                    System.out.println(linha);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }
   
    public static void main(String[] args) {
       try {
           new Cliente();
       } catch (IOException ex) {
           Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
}
