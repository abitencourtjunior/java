/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package altsapp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 *
 * @author Lab1
 */
public class AudioMessage extends Message {
    
    private FileInputStream input;
    private FileOutputStream output;

    /**
     * @return the input
     */
    public FileInputStream getInput() {
        return this.input;
    }

    /**
     * @param input the input to set
     */
    public void setInput(FileInputStream input) {
        this.input = input;
    }

    /**
     * @return the output
     */
    public FileOutputStream getOutput() {
        
        return this.output;
        
    }

    /**
     * @param output the output to set
     */
    public void setOutput(FileOutputStream output) {
        this.output = output;
    }
    
    
    
}
