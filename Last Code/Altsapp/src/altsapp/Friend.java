/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package altsapp;

/**
 *
 * @author Lab1
 */
public class Friend {
    private String nome;
    private String email;
    private String mobileNo;
    
    public Friend(){
        
    }
    
    public Friend(String nome, String email, String mobileNo){
        this.nome = nome;
        this.email = email;
        this.mobileNo = mobileNo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
    
    
    
    
}
