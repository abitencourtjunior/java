/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package altsapp;

/**
 *
 * @author Lab1
 */
public class TextMessage extends Message {
    
    private String input;
    private String output;

    /**
     * @return the input
     */
    public String getInput() {
        return this.input;
    }

    /**
     * @param input the input to set
     */
    public void setInput(String input) {
        this.input = input;
    }

    /**
     * @return the output
     */
    public String getOutput() {
        return this.output;
    }

    /**
     * @param output the output to set
     */
    public void setOutput(String output) {
        this.output = output;
    }
    
    public String parseMsgOutput(String output){
       String MsgOutput = this.output;       
       return MsgOutput;
    }
    
    public String parseMsgInput(String input){
        String MsgInput = this.input;        
        return MsgInput;
    }
    
}
