

package altsapp;
import java.util.ArrayList;




public class FriendList {
    private int id;
    private ArrayList friendList;
    private int contID = 1;
    
    public FriendList(){
        this.id = contID;
        contID++;
    }
    
    public FriendList(ArrayList friendList){
        this.id = contID;
        this.friendList = friendList;
        contID++;
    }
   
    
    public void addFriendByEmail(Friend friend, String email){
         if(friend.getEmail().equals(email)){
             friendList.add(friend);
         }
    }
    
    public void removeFriend(Friend friend, String mobileNo){
        if(friend.getMobileNo().equals(mobileNo)){
            friendList.remove(friend);
        }
    }
    
    public void addFriendByPhone(Friend friend, String mobileNo){
        if(friend.getMobileNo().equals(mobileNo)){
            friendList.add(friend);
        }
    }
    
    public Object getFriend(String mobileNo){
        for(int cont = 0; cont <= friendList.size(); cont++){
            if(friendList.get(cont).equals(mobileNo)){
                return friendList.get(cont);   
            }
            
        }
        return null;
    } 
    
    public Object getList(){
        for(int cont = 0; cont <= friendList.size(); cont++){
            return friendList.get(cont);
        }
        return null;
    }
}
