
package altsapp;

import java.util.ArrayList;


public class UserAccount {
    private String email;
    private String password;
    private String nome;
    private String mobileNo;
    public ArrayList friendList;
    public ArrayList groupList;

    
    public UserAccount(){
        
    }
    
    public UserAccount(String email, String password, String nome, String mobileNo, ArrayList friendList, ArrayList groupList){
       this.email = email;
       this.password = password;
       this.nome = nome;
       this.mobileNo = mobileNo;
       this.friendList = friendList;
       this.groupList = groupList;
    }
    
    public void editAccount(String email, String password, String nome, String mobileNo, ArrayList friendList, ArrayList groupList){
        setEmail(email);
        setPassword(password);
        setNome(nome);
        setMobileNo(mobileNo);
        setFriendList(friendList);
        setGroupList(groupList);
    }
    
    public boolean login(String email, String password){
        if(getEmail().equals(email) == false || getPassword().equals(password) == false){
            return false;
        }
        return true;
    }
    
    public void createGroup(Group group){
        groupList.add(group);
    }
    
    public void deleteGroup(Group group){
        groupList.remove(group);
    }
    
    public void editGroup(Group group){
        
    }
    
    public void editPersonalInfo(String nome, String email){
        setNome(nome);
        setEmail(email);
    }
    
    public void changePassword(String password, String newPassword){
        if(getPassword().equals(password)){
            setPassword(newPassword);
        }
        System.out.println("Erro! Senha incorreta!");
    }

    
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    public String getPassword() {
        return this.password;
    }

   
    public void setPassword(String password) {
        this.password = password;
    }

  
    public String getNome() {
        return this.nome;
    }

  
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    public String getMobileNo() {
        return this.mobileNo;
    }

    
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public void setFriendList(ArrayList friendList) {
        this.friendList = friendList;
    }

    public void setGroupList(ArrayList groupList) {
        this.groupList = groupList;
    }
     
}
    

    
    
    


    

