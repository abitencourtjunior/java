/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package altsapp;
import java.util.ArrayList;
/**
 *
 * @author Lab1
 */
public class Group {
    private int id;
    public ArrayList memberList;
    private UserAccount owner;
    public int contID = 1;
    
    public Group(){
        contID++;
    }
    
    public Group(ArrayList memberList, UserAccount owner){
        this.id = contID;
        this.memberList = memberList;
        this.owner = owner;
        contID++;
    }
    
    public void addMember(Friend friend){
        memberList.add(friend);
    }
    
    public void removeMember(Friend friend){
        memberList.remove(friend);
    }
    
    public UserAccount getOwner(){
        return owner;
    }
    
    
}
