/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package altsapp;
import java.io.*;

/**
 *
 * @author Lab1
 */
public class VideoMessage extends Message {
    
    private FileInputStream input;
    private FileInputStream output;

    /**
     * @return the input
     */
    public FileInputStream getInput() {
        return input;
    }

    /**
     * @param input the input to set
     */
    public void setInput(FileInputStream input) {
        this.input = input;
    }

    /**
     * @return the output
     */
    public FileInputStream getOutput() {
        return output;
    }

    /**
     * @param output the output to set
     */
    public void setOutput(FileInputStream output) {
        this.output = output;
    }
    
    
    
}
