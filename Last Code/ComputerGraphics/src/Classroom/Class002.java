package Classroom;

import javax.swing.Timer;
import javax.swing.JPanel;
import javax.swing.JFrame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Class002 extends JPanel implements ActionListener, KeyListener {

    Timer animcao = new Timer(5, this);
    int posicaox = 0, posicaoy = 0, velX = 0, vely = 0;

    public Class002 (){
        animcao.start();
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.fillRect(posicaox,posicaoy,60,110);
        g.setColor(Color.RED);
    }
    @Override
    public void actionPerformed(ActionEvent e) {

        if( posicaox < 0 ){
            velX = 0;
            posicaox = 0;
        }
        if (posicaox > 280){
            velX = 0;
            posicaox = 280;
        }

        if( posicaoy < 0 ){
            vely = 0;
            posicaoy = 0;
        }
        if (posicaoy > 265){
            vely = 0;
            posicaoy = 265;
        }
        posicaox = posicaox + velX;
        posicaoy = posicaoy + vely;
        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int tecla = e.getKeyCode();

        if(tecla == KeyEvent.VK_LEFT){
            velX = -1;
            vely = 0;
        }
        if (tecla == KeyEvent.VK_UP){
            velX = 0;
            vely = -1;
        }
        if (tecla == KeyEvent.VK_RIGHT){
            velX =  1;
            vely = 0;
        }
        if (tecla == KeyEvent.VK_DOWN){
            vely = 1;
            velX = 0;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        velX = 0;
        vely = 0;
    }

    public static void main(String[] args) {
        Class002 cl2 = new Class002();
        JFrame jframe = new JFrame();
        jframe.setTitle("Teste");
        jframe.setSize(350,400);
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.add(cl2);
    }
}
