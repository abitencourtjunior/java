package Classroom;

import javax.swing.*;
import java.awt.Graphics;
import java.awt.Color;

public class Class001 extends JFrame {


    public Class001(){
        setTitle("Game");
        setSize(660,760);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void paint(Graphics g){
        g.setColor(Color.GREEN);
        g.fillRect(200, 200,100,100);
    }


    public static void main(String[] args) {
        Class001 cl = new Class001();
        cl.paint(null);
    }
}
