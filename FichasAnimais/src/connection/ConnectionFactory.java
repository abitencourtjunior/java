package connection;

import java.sql.*;

public class ConnectionFactory {

    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/sistema_cadastro";
    private static final String USER = "root";
    private static final String PASS = "root";

    public static Connection getConnection() {

        try {
            Class.forName(DRIVER);

            return DriverManager.getConnection(URL,USER, PASS);

        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException("Erro ao conectar no banco", ex);
        }

    }

    public static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                System.err.println("Error:" + e);
            }
        }
    }

    public static void closeConnection(Connection conn, PreparedStatement stmt) {

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                System.err.println("Error:" + e);
            }
        }
        closeConnection(conn);
    }


    public static void closeConnection(Connection conn, PreparedStatement stmt, ResultSet rs) {

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                System.err.println("Error:" + e);
            }
        }
        closeConnection(conn, stmt);
    }
}