package model.dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.bean.Animal;

public class AnimalDAO {

    private Connection conn = null;
    private Animal animal;

    public AnimalDAO(Animal animal){
        conn = ConnectionFactory.getConnection();
        this.animal = animal;
    }

    public boolean insertAnimail(){

        String sql = "INSERT INTO animais (nome) values (?); ";

        PreparedStatement stmt = null;

        try {

            stmt = conn.prepareStatement(sql);
            stmt.setString(1,animal.getNome());
            stmt.executeUpdate();

            return true;

        } catch (SQLException e) {

            e.printStackTrace();

            return false;

        } finally {
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }

    public List<Animal> selectAminal(){

        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM animais;";

        List<Animal> todosAnimais = new ArrayList<>();

        try {

            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

                while (rs.next()){
                    Animal animal = new Animal();
                    animal.setId(rs.getInt("id_animal"));
                    animal.setNome(rs.getString("nome"));
                    todosAnimais.add(animal);
                }

        } catch (SQLException e) {

            e.printStackTrace();

        } finally {

            ConnectionFactory.closeConnection(conn, stmt, rs);
        }

        return todosAnimais;
    }

    public boolean updateAnimal(){

        String sql = "UPDATE animais SET nome = ? WHERE id_animal = ?;";

        PreparedStatement stmt = null;

        try {

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, animal.getNome());
            stmt.setInt(2,animal.getId());
            stmt.executeUpdate();
            return true;

        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        } finally {

            ConnectionFactory.closeConnection(conn, stmt);
        }
    }

    public boolean deleteAnimal(){

        String sql = "DELETE FROM animais WHERE id_animal = ?;";

        PreparedStatement stmt = null;

        try {

            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, animal.getId());
            stmt.executeUpdate();
            return true;

        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        } finally {

            ConnectionFactory.closeConnection(conn, stmt);
        }
    }

}
