package model.dao;

import connection.ConnectionFactory;
import model.bean.Animal;
import model.bean.Fichas;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FichasDAO {

    private Connection conn = null;

    private Fichas fichas;

    public FichasDAO(Fichas fichas){
        conn = ConnectionFactory.getConnection();
        this.fichas = fichas;
    }

    public boolean insert(){

        PreparedStatement stmt = null;

        String sql = "INSERT INTO fichas_cadastros ( dt_cadastro, observacao, status_cad, id_animal) VALUES (?,?,?,?);";

        try {

            stmt = conn.prepareStatement(sql);
            stmt.setDate(1, Date.valueOf(this.fichas.getDataCadastro()));
            stmt.setString(2, this.fichas.getObservacao());
            stmt.setBoolean(3,this.fichas.isStatusCad());
            stmt.setInt(4,this.fichas.getAnimal().getId());
            stmt.executeUpdate();
            return true;

        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        } finally {

            ConnectionFactory.closeConnection(conn, stmt);
        }
    }

    public List<Fichas> select() {

        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Fichas> fichaAnimais = new ArrayList<>();
        String sql = "select fichas_cadastros.id_ficha as FID, fichas_cadastros.dt_cadastro as REGISTRO, \n" +
                "fichas_cadastros.observacao as DESCRICAO, fichas_cadastros.status_cad as SITUACAO,\n" +
                "animais.id_animal as AID, animais.nome as NOME \n" +
                "from fichas_cadastros \n" +
                "inner join animais \n" +
                "on fichas_cadastros.id_animal = animais.id_animal;";
        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();



            while (rs.next()) {

                Fichas fichas = new Fichas();
                fichas.setId(rs.getInt("FID"));
                fichas.setDataCadastro(rs.getDate("REGISTRO"));
                fichas.setObservacao(rs.getString("DESCRICAO"));
                fichas.setStatusCad(rs.getBoolean("SITUACAO"));

                Animal animal = new Animal();
                animal.setId(rs.getInt("AID"));
                animal.setNome(rs.getString("NOME"));

                fichas.setAnimal(animal);

                fichaAnimais.add(fichas);

            }


        } catch (SQLException e) {

            e.printStackTrace();

        } finally {

            ConnectionFactory.closeConnection(conn, stmt, rs);
        }

        return fichaAnimais;

    }

    public  boolean update(){

        String sql = "update fichas_cadastros set dt_cadastro = ? , observacao = ?, status_cad = ?, id_animal = ? where id_ficha = ?";

            PreparedStatement stmt = null;

            try {

                stmt = conn.prepareStatement(sql);
                stmt.setDate(1,Date.valueOf(this.fichas.getDataCadastro()));
                stmt.setString(2,this.fichas.getObservacao());
                stmt.setBoolean(3, this.fichas.isStatusCad());
                stmt.setInt(4, this.fichas.getAnimal().getId());
                stmt.setInt(5,this.fichas.getId());
                stmt.executeUpdate();
                return true;

            } catch (SQLException e) {

                e.printStackTrace();
                return false;

            } finally {

                ConnectionFactory.closeConnection(conn, stmt);
            }
        }

    public boolean delete(){

            String sql = "DELETE FROM fichas_cadastros WHERE id_ficha = ?;";

            PreparedStatement stmt = null;

            try {

                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, this.fichas.getId());
                stmt.executeUpdate();
                return true;

            } catch (SQLException e) {

                e.printStackTrace();
                return false;

            } finally {

                ConnectionFactory.closeConnection(conn, stmt);
            }

        }

    public List<Fichas> selectData(){

        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Fichas> fichaAnimais = new ArrayList<>();
        String sql = "select id_ficha, dt_cadastro, status_cad from fichas_cadastros where dt_cadastro = ?;";
        try {

            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {

                Fichas fichas = new Fichas();
                fichas.setId(rs.getInt("id_ficha"));
                fichas.setDataCadastro(rs.getDate("dt_cadastro"));
                fichas.setStatusCad(rs.getBoolean("SITUACAO"));

                fichaAnimais.add(fichas);

            }


        } catch (SQLException e) {

            e.printStackTrace();

        } finally {

            ConnectionFactory.closeConnection(conn, stmt, rs);
        }

        return fichaAnimais;

    }

}



