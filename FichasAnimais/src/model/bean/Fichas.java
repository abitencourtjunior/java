package model.bean;

import java.sql.Date;
import java.time.LocalDate;

public class Fichas {

    private int id;
    private LocalDate dataCadastro;
    private String observacao;
    private boolean statusCad;
    private Animal animal;

    public Fichas(LocalDate dataCadastro, String observacao, boolean statusCad, Animal animal){
        this.dataCadastro = dataCadastro;
        this.observacao = observacao;
        this.statusCad = statusCad;
        this.animal = animal;
    }

    public Fichas(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDataCadastro() {
        return this.dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro){
        this.dataCadastro = dataCadastro.toLocalDate();
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public boolean isStatusCad() {
        return statusCad;
    }

    public void setStatusCad(boolean statusCad) {
        this.statusCad = statusCad;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

}
