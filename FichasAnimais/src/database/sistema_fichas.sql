
create database sistema_cadastro;

USE sistema_cadastro;

CREATE TABLE IF NOT EXISTS animais(
id_animal int NOT NULL auto_increment,
nome varchar(255) not null,
primary key(id_animal)
);

CREATE TABLE IF NOT EXISTS  fichas_cadastros (
id_ficha int not null auto_increment, 
dt_cadastro date not null, 
observacao varchar(400) not null , 
status_cad tinyint(1) not null,
id_animal int,
PRIMARY KEY(id_ficha),
FOREIGN KEY (id_animal) references animais(id_animal)
);

INSERT INTO animais (nome) values ('Lula');
INSERT INTO animais (nome) values ('Polvo');
INSERT INTO animais (nome) values ('Gato');
INSERT INTO animais (nome) values ('Cachorro');
INSERT INTO animais (nome) values ('Pássaro');

INSERT INTO fichas_cadastros ( dt_cadastro, observacao, status_cad, id_animal) VALUES ('2018-10-10','Doente',0,1);
INSERT INTO fichas_cadastros ( dt_cadastro, observacao, status_cad, id_animal) VALUES ('2018-06-10','Doente',0,2);
INSERT INTO fichas_cadastros ( dt_cadastro, observacao, status_cad, id_animal) VALUES ('2018-04-10','Doente',0,3);
INSERT INTO fichas_cadastros ( dt_cadastro, observacao, status_cad, id_animal) VALUES ('2018-10-20','Doente',0,4);
INSERT INTO fichas_cadastros ( dt_cadastro, observacao, status_cad, id_animal) VALUES ('2018-09-23','Doente',0,5);



