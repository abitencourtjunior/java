import model.Produto;
import dao.ProdutoDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TestProduto {

    public static void main(String[] args) {

        Produto produto1 = new Produto("Altamor", "Raynovac");

        try(Connection connection = new Database().getConnection()){
            ProdutoDAO dao =  new ProdutoDAO(connection);

            List<Produto> produtos = dao.list();
            for (Produto produto : produtos) {
                System.out.println("Existe o produto: " + produto);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
