import org.postgresql.ds.PGConnectionPoolDataSource;
import org.postgresql.ds.common.BaseDataSource;
import java.sql.Connection;
import java.sql.SQLException;


public class Database {

    private BaseDataSource dataSource;

    Database() throws SQLException {

        PGConnectionPoolDataSource pool = new PGConnectionPoolDataSource();
        pool.setURL("jdbc:postgresql://localhost:5432/namedatabase");
        pool.setUser("user");
        pool.setPassword("password");
        this.dataSource = pool;
        dataSource.getConnection();

    }

    Connection getConnection() throws SQLException {

        Connection connection = dataSource.getConnection();

        return connection;
    }
}
