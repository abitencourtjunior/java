package model;

import java.util.ArrayList;
import java.util.List;

public class Categoria {

    private int id;
    private String nome;
    private final List<Produto> produtos = new ArrayList<>();

    public Categoria(String nome){
        this.nome = nome;
    }

    public Categoria(int id, String nome){
        this.id = id;
        this.nome = nome;
    }

    public void adiciona(Produto produto) {
        produtos.add(produto);
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String toString(){
        return String.format("[Categoria: %d,  %s]", id, nome);
    }
}
