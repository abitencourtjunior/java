import dao.CategoriaDAO;
import dao.ProdutoDAO;
import model.Categoria;
import model.Produto;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TesteCategorias {

    public static void main(String[] args) throws SQLException {
        try(Connection con = new Database().getConnection()) {
            List<Categoria> categorias = new CategoriaDAO(con).buscaComProdutos();
            for(Categoria categoria : categorias) {
                System.out.println(categoria.getNome());

                for(Produto produto : categoria.getProdutos()) {
                    System.out.println(categoria.getNome() + " - " + produto.getNome());
                }

            }
        }

    }
}
