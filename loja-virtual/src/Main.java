import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {

        Connection connection = new Database().getConnection();

        Statement statement = connection.createStatement();

        boolean resultado = statement.execute("select * from produto");

        ResultSet resultSet = statement.getResultSet();

        while (resultSet.next()) {

            String nome = resultSet.getString("nome");
            System.out.println(nome);

            String descricao = resultSet.getString("descricao");
            System.out.println(descricao);

            System.out.println(resultado);


        }
        resultSet.close();
        connection.close();
        statement.close();

    }
}
