package dao;

import model.Categoria;
import model.Produto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO {

    private final Connection conn;

    public ProdutoDAO(Connection conn){
        this.conn = conn;
    }

    public void insert(Produto produto) {

        String sql = "INSERT INTO produto(nome, descricao) values (?,?)";

        try (PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)){

            stmt.setString(1, produto.getNome());
            stmt.setString(2, produto.getDescricao());
            stmt.execute();

            ResultSet resultSet = stmt.getGeneratedKeys();

            while (resultSet.next()){
                String id = resultSet.getString("id");
                System.out.println(id + ": id gerado");
            }

        } catch (Exception ex){
            ex.printStackTrace();
            System.out.println("Rollback efetuado!");
        }
    }

    public List<Produto> list(){

        List<Produto> produtos = new ArrayList<>();

        String sql = "SELECT * FROM produto";

        try (PreparedStatement stmt = conn.prepareStatement(sql)){

            stmt.execute();

            ResultSet resultSet = stmt.getResultSet();

            while (resultSet.next()){

                int id = resultSet.getInt("id");
                String nome = resultSet.getString("nome");
                String descricao = resultSet.getString("descricao");

                Produto produto = new Produto(id, nome, descricao);

                produtos.add(produto);
            }

        } catch (Exception ex){
            ex.printStackTrace();
        }
        return produtos;
    }

    public List<Produto> buscarCategoria(Categoria categoria){

        List<Produto> produtos = new ArrayList<>();

        String sql = "SELECT * FROM produto where categoria_id = ?";

        try (PreparedStatement stmt = conn.prepareStatement(sql)){

            stmt.setInt(1,categoria.getId());
            stmt.execute();

            ResultSet resultSet = stmt.getResultSet();

            while (resultSet.next()){

                int id = resultSet.getInt("id");
                String nome = resultSet.getString("nome");
                String descricao = resultSet.getString("descricao");

                Produto produto = new Produto(id, nome, descricao);

                produtos.add(produto);
            }

        } catch (Exception ex){
            ex.printStackTrace();
        }
        return produtos;
    }

    public void update(int id){

    }

    public void delete(){

        String sql = "delete from categoria where id = ?;";
    }
}
