package dao;

import model.Categoria;
import model.Produto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDAO {

    private final Connection con;

    public CategoriaDAO(Connection con) {
        this.con = con;
    }

    public void insert(Categoria categoria){

        String sql = "INSERT INTO categoria (nome) values (?);";

        try(PreparedStatement stmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)){

            stmt.setString(1,categoria.getNome());
            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();

            while (rs.next()){
                String id = rs.getString("id");
                System.out.println("Valor novo: " + id);
            }

        } catch (Exception ex){
            ex.printStackTrace();
            System.out.println("Rollback efetuado!");
        }

    }

    public List<Categoria> list (){

        List<Categoria> categorias = new ArrayList<>();

        String sql = "SELECT * FROM categoria; ";

        try(PreparedStatement stmt = con.prepareStatement(sql)){

            stmt.execute();

            try(ResultSet rs = stmt.getResultSet()){

                while (rs.next()) {

                    int id = rs.getInt("id");
                    String nome = rs.getString("nome");

                    Categoria categoria = new Categoria(id, nome);

                    categorias.add(categoria);
                }
            } catch (SQLException ex){
                ex.printStackTrace();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return categorias;
    }

    public List<Categoria> buscaComProdutos() throws SQLException{

        List<Categoria> categorias = new ArrayList<>();

        Categoria ultima = null;

        String sql = "select p.id as p_id, p.nome as p_nome, p.descricao as p_descricao, p.categoria_id as p_categoria, " +
                "c.id as c_id, c.nome as c_nome from produto p join categoria c on p.categoria_id = c.id;";

        try(PreparedStatement stmt = con.prepareStatement(sql)){
            stmt.execute();

            try(ResultSet rs = stmt.getResultSet()){
                while (rs.next()){

                    int id = rs.getInt("c_id");
                    String nome = rs.getString("c_nome");

                    if(ultima == null || !ultima.getNome().equals(nome)){
                        Categoria categoria = new Categoria(nome);
                        categorias.add(categoria);
                        ultima = categoria;
                    }

                    int id_produto = rs.getInt("p_id");
                    String nome_produto = rs.getString("p_nome");
                    String descricao = rs.getString("p_descricao");

                    Produto produto = new Produto(id_produto, nome_produto, descricao);
                    ultima.adiciona(produto);
                }
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return categorias;
    }
}
