package Janela;

public class Linha extends Thread {

    Layout layout;

    public Linha( Layout layout){
        this.layout = layout;
    }

    public void run(){
        while (true){
            try {
                Thread.sleep(5) ;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            layout.repaint();
        }
    }
}
