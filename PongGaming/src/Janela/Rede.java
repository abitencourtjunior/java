package Janela;

import java.awt.geom.Rectangle2D;

public class Rede {

    private int pontoX, pontoY;
    private final int largura = 10, altura = 500;

    public Rede(int pontoX, int pontoY){
        this.pontoX = pontoX;
        this.pontoY = pontoY;
    }

    public Rectangle2D getRede(){
        return new Rectangle2D.Double(pontoX, pontoY, this.largura, this.altura);
    }

}
