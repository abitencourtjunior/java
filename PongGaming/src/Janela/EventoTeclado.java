package Janela;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class EventoTeclado extends KeyAdapter {

    static boolean w, s, up, down;

    public void keyPressed(KeyEvent e){

        int idTecla = e.getKeyCode();

        if (idTecla == KeyEvent.VK_W){
            w = true;
        }

        if (idTecla == KeyEvent.VK_S){
            s = true;
        }

        if (idTecla == KeyEvent.VK_UP){
            up = true;
        }

        if(idTecla == KeyEvent.VK_DOWN){
            down = true;
        }

    }

    public void keyReleased(KeyEvent e){

        int idTecla = e.getKeyCode();

        if (idTecla == KeyEvent.VK_W){
            w = false;
        }

        if (idTecla == KeyEvent.VK_S){
            s = false;
        }

        if (idTecla == KeyEvent.VK_UP){
            up = false;
        }

        if(idTecla == KeyEvent.VK_DOWN){
            down = false;
        }
    }
}
