package Janela;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Layout extends JPanel {

    Bola bola = new Bola(0,0);
    Rede rede = new Rede(400,0);
    Jogadores jogadores = new Jogadores(10,200);
    Jogadores jogadores2 = new Jogadores(800-10-20,200);

    public Layout(){
        setBackground(Color.LIGHT_GRAY);
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        this.desenhar(g2);
        atualizar();


    }

    public void desenhar(Graphics2D g){

        this.placar(g);
        // Para desenhar os objetos dos jogo
        this.jogadores.jogador(g);
        this.atualizar();
        this.jogadores2.jogador(g);
        this.atualizar();
        this.bola.bolaJogo(g);

    }



    public void atualizar(){
        bola.mover(getBounds(), colisao(jogadores.getJogadores()), colisao(jogadores2.getJogadores()));
        jogadores.movimento(getBounds());
        jogadores2.movimento2(getBounds());
    }

    private boolean colisao (Rectangle2D r){
        return bola.getBola().intersects(r);
    }

    public void placar(Graphics2D g){
        Font score = new Font("Arial", 1,30);
        g.setFont(score);
        g.drawString("Placar",(float)this.getBounds().getCenterX() - 50.0F, 30.0F);
        g.drawString(Integer.toString(this.bola.getScore1()), (float)this.getBounds().getCenterX() - 40.0F, 70.0F);
        g.drawString(Integer.toString(this.bola.getScore2()), (float)this.getBounds().getCenterX() - (-20.0F), 70.0F);
        g.setColor(Color.BLUE);
    }
}
