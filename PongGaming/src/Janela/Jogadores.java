package Janela;

// Criação dos Objetos dos Jogadores

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Jogadores {

    private int posicaoX, posicaoY;
    private static final int largura = 20, altura = 100;

    public Jogadores(int posicaoX, int posicaoY){
        this.posicaoX = posicaoX;
        this.posicaoY = posicaoY;
    }

    public Rectangle2D getJogadores(){
        return new Rectangle2D.Double(this.posicaoX, this.posicaoY, this.largura, this.altura);
    }

    public void movimento(Rectangle limites){
        if(EventoTeclado.w &&  posicaoY > limites.getMinY()){
            posicaoY--;
        }
        if(EventoTeclado.s && posicaoY < limites.getMaxY()-this.altura){
            posicaoY++;
        }
    }

    public void movimento2(Rectangle limites){
        if(EventoTeclado.up &&  posicaoY > limites.getMinY()){
            posicaoY--;
        }
        if(EventoTeclado.down && posicaoY < limites.getMaxY()-this.altura){
            posicaoY++;
        }
    }

    public void jogador(Graphics2D b){
        b.setColor(Color.GREEN);
        b.fill(this.getJogadores());
    }

}
