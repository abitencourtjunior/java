package Janela;

import javax.swing.*;

public class Janela extends JFrame {

    private final int largura=800, altura=500;
    private Layout layout;
    private Linha linha;


    public Janela(){
        setTitle("Pong Gaming");
        setSize(largura, altura);
        setLocationRelativeTo(null); // Irá abrir no centro da tela.
        setResizable(false);
        layout = new Layout();
        add(layout);
        addKeyListener(new EventoTeclado());
        linha = new Linha(layout);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        linha.start();
    }
}
