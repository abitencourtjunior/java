package Janela;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Bola {

    private double x, y;
    private final int largura = 20, altura = 20;
    private int limiteX = 1, limiteY = 1;
    private int score1 = 0, score2 = 0;

    public Bola(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Rectangle2D getBola(){
        return new Rectangle2D.Double(x, y, this.largura, this.altura);
    }

    public void mover(Rectangle limites, boolean colisao1, boolean colisao2){

        x+=limiteX;
        y+=limiteY;

        if(colisao1){
            limiteX = - limiteX;
            x = 40;
        }

        if(colisao2){
            limiteX = - limiteX;
            x = 710;
        }

        if(this.x < limites.getMinX()){
            this.score1 ++;
            this.x = limites.getCenterX();
            this.y = limites.getCenterY();
            this.limiteX = - this.limiteX;
        }

        if (this.x + 15.0D >= limites.getMaxX()) {
            this.score2 ++;
            this.x = limites.getCenterX();
            this.y = limites.getCenterY();
            limiteX = - limiteX;
        }

        if( x > limites.getMaxX()){
            limiteX = -limiteX;
        }

        if( y > limites.getMaxY()){
            limiteY= - limiteY;
        }

        if( x < 0){
            limiteX= - limiteX;
        }

        if( y < 0){
            limiteY= - limiteY;
        }
    }

    public int getScore1(){
        return score1;
    }

    public int getScore2(){
        return score2;
    }

    public void bolaJogo(Graphics2D b){
        b.setColor(Color.RED);
        b.fill(this.getBola());
    }

}
