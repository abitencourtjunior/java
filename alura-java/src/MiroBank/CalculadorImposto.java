package MiroBank;

public class CalculadorImposto {

    private double valorTotal;

    public void registrar(Tributavel tributavel){
        double valor = tributavel.getImposto();
        this.valorTotal += valor;
    }

    public double getValorTotal(){
        return this.valorTotal;
    }
}
