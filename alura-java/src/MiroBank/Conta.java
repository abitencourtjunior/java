package MiroBank;

import SistemaBancario.Funcionario;

public abstract class Conta {

    protected double saldo;
    private int agencia;
    private int numero;
    private Funcionario titular;
    private static int total;

    public Conta(double saldo, int agencia, int numero, Funcionario titular){
        Conta.total++;
        this.saldo = saldo;
        this.agencia = agencia;
        this.numero = numero;
        this.titular = titular;
    }

    public double getSaldo(){
        return this.saldo;
    }

    public int getAgencia(){
        return this.agencia;
    }

    public int getNumero(){
        return this.numero;
    }

    public Funcionario getTitular(){
        return this.titular;
    }

    public void setSaldo(double saldo){
        this.saldo += saldo;
    }

    public void setAgencia(int agencia){
        this.agencia = agencia;
    }

    public void setNumero(int numero){
        this.numero = numero;
    }

    public boolean transfere(double valor, Conta conta ){
        if(this.saca(valor)) {
          conta.deposita(valor);
          return true;
        }
        return false;
    }

    public abstract void mostrarConta();

    public abstract void deposita(double valor);

    public boolean saca(double valor){
        if(this.saldo >= valor){
            this.saldo-=valor;
            return true;
        } else {
            return false;
        }
    }

    public static int getTotal(){
        return Conta.total;
    }

}
