package MiroBank;

import SistemaBancario.Funcionario;

public class ContaCorrente extends Conta implements Tributavel{

    public ContaCorrente(double saldo, int agencia, int numero, Funcionario titular){
        super(saldo,agencia,numero,titular);
    }

    public void mostrarConta() {

        System.out.println("MiroBank - MiroConta"
                + "\nTitular: " + this.getTitular().getNome()
                + " | Saldo: " + super.getSaldo()
                + " | Agência: " + super.getAgencia()
                + " | Número: " + super.getNumero());
    }

    public void deposita(double valor) {
        super.saldo += valor;
    }

    @Override
    public double getImposto() {
        return super.saldo * 0.01;
    }
}
