package MiroBank;

import SistemaBancario.Funcionario;

class ContaPoupanca extends Conta {

    public ContaPoupanca (double salario, int agencia, int numero, Funcionario titular){
        super(salario,agencia,numero,titular);
    }

    @Override
    public void mostrarConta() {
        return;
    }

    @Override
    public void deposita(double valor) {
        super.saldo += valor;
    }
}