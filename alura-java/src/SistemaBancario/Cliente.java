package SistemaBancario;

public class Cliente implements Autenticavel{

    private AutenticaUsuario autenticacao;

    public Cliente(){
        this.autenticacao = new AutenticaUsuario();
    }

    @Override
    public void setSenha(int senha) {
        this.autenticacao.setSenha(senha);
    }

    @Override
    public boolean autentica(int senha) {
        return this.autenticacao.autentica(senha);
    }
}
