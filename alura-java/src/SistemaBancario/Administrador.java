package SistemaBancario;

public class Administrador extends Funcionario implements Autenticavel {

    private AutenticaUsuario autenticacao;

    public Administrador(String nome, String cpf, double salario){
        super(nome, cpf, salario);
        this.autenticacao = new AutenticaUsuario();
    }

    @Override
    public double getBonificacao() {
        return super.getSalario() * 0.4;
    }

    @Override
    public void setSenha(int senha) {
        this.autenticacao.setSenha(senha);
    }

    @Override
    public boolean autentica(int senha) {
        return this.autenticacao.autentica(senha);
    }
}
