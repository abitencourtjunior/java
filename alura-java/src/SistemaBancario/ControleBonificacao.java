package SistemaBancario;

public class ControleBonificacao {

    private double soma;

    public double getSoma() {
        return this.soma;
    }

    // Polimorfismo

    public void registra(Funcionario funcionario){
        double boni = funcionario.getTipo();
        this.soma = this.soma + boni;
    }

}
