package SistemaBancario;

import MiroBank.CalculadorImposto;
import MiroBank.ContaCorrente;
import MiroBank.SeguroDeVida;

public class Main {

     public static void main(String[] args) {

         SeguroDeVida seguroDeVida = new SeguroDeVida();

         CalculadorImposto calc = new CalculadorImposto();

         ContaCorrente cc = new ContaCorrente(100,1,1,new Diretor("a","a",123));

         calc.registrar(cc);
         calc.registrar(seguroDeVida);

         System.out.println(calc.getValorTotal());


    }
}
