package SistemaBancario;

public class SistemaInterno {

    private int senha = 12345;

    public void autentica(Autenticavel autenticavel){

        boolean autenticar = autenticavel.autentica(this.senha);

        if(autenticar){
            System.out.println("Autenticação Concluída");
        } else {
            System.out.println("Falha na Autenticação");
        }
    }
}
