package SistemaBancario;

public class Diretor extends Funcionario implements Autenticavel {

    private AutenticaUsuario autenticacao;

    public Diretor(String nome, String cpf, double salario){
        super(nome, cpf, salario);
        this.autenticacao = new AutenticaUsuario();
    }

    @Override
    public double getBonificacao() {
        return super.getSalario() + 1000;
    }

    @Override
    public void setSenha(int senha) {
        this.autenticacao.setSenha(senha);
    }

    @Override
    public boolean autentica(int senha) {
        return this.autenticacao.autentica(senha);
    }
}
