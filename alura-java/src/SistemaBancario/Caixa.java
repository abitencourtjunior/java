package SistemaBancario;

public class Caixa extends Funcionario {

    public Caixa(String nome, String cpf, double salario){
        super(nome, cpf, salario);
    }

    @Override
    public double getBonificacao() {
        return 0;
    }


}
