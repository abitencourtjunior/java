DELIMITER $$
create procedure guardaLog(in idFuncionario int, in tabela varchar(45), in comando varchar(45), in idTupla int)
begin
	set idFuncionario = idFuncionario;
    set tabela = tabela;
    set comando = comando;
    set idTupla = idTupla;
    
    insert into Registro(DataModificacao, Hora, idFuncionario, NomeTabela, TipoDeRegistro, idDaTupla)
    values (now(), now(), idFuncionario, tabela, comando, idTupla);
end $$
DELIMITER ;