package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.ModeloAquatico;

public class ModeloAquaticoDAO extends DAO {
	private ModeloAquatico modelo;

	public ModeloAquaticoDAO() {
	}

	public ModeloAquaticoDAO(ModeloAquatico modelo) {
		this.modelo = modelo;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from ModeloAquatico;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				ModeloAquatico modelo = new ModeloAquatico();
				modelo.setIdModeloAquatico(rs.getInt("idModeloAquatico"));
				modelo.setNomeModelo(rs.getString("NomeModelo"));
				modelo.setPotenciaMotor(rs.getShort("PotenciaMotor"));
				modelo.setTipoMotor(rs.getByte("TipoMotor"));
				modelo.setLugares(rs.getByte("Lugares"));
				modelo.setTipoDeConducao(rs.getString("TipoDeConducao"));
				modelo.setTipoDeIgnicao(rs.getString("TipoDeIgnicao"));
				modelo.setTipoDeAgua(rs.getString("TipoDeAgua"));
				modelo.setVolumeTanque(rs.getShort("VolumeTanque"));
				modelo.setMarca(new MarcaDAO().selectMarca(rs.getInt("Marca_idMarca")));

				lista.add(modelo);
			}
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into ModeloAquatico (NomeModelo, PotenciaMotor, TipoMotor, Lugares, TipoDeConducao, TipoDeIgnicao, TipoDeAgua, "
				+ "VolumeTanque, Marca_idMarca) values (?, ?, ?, ?, ?, ?, ?, ?, ?);";
		String[] id = { "idModeloAquatico" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, modelo.getNomeModelo());
			stmt.setShort(2, modelo.getPotenciaMotor());
			stmt.setByte(3, modelo.getTipoMotor());
			stmt.setByte(4, modelo.getLugares());
			stmt.setString(5, modelo.getTipoDeConducao());
			stmt.setString(6, modelo.getTipoDeIgnicao());
			stmt.setString(7, modelo.getTipoDeAgua());
			stmt.setShort(8, modelo.getVolumeTanque());
			stmt.setInt(9, new MarcaDAO(modelo.getMarca()).insert(idFuncionario));
			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.modelo.setIdModeloAquatico(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "ModeloAquatico", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.modelo.getIdModeloAquatico();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update ModeloAquatico set NomeModelo = ?, PotenciaMotor = ?, TipoMotor = ?, Lugares = ?, TipoDeCondcao = ?, TipoDeIgnicao = ?, "
				+ "TipoDeAgua = ?, VolumeTanque = ?, Marca_idmarca = ? where idModeloAquatico = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, modelo.getNomeModelo());
			stmt.setShort(2, modelo.getPotenciaMotor());
			stmt.setByte(3, modelo.getTipoMotor());
			stmt.setByte(4, modelo.getLugares());
			stmt.setString(5, modelo.getTipoDeConducao());
			stmt.setString(6, modelo.getTipoDeIgnicao());
			stmt.setString(7, modelo.getTipoDeAgua());
			stmt.setShort(8, modelo.getVolumeTanque());
			stmt.setInt(9, modelo.getIdModeloAquatico());
			stmt.setInt(10, new MarcaDAO(modelo.getMarca()).insert(idFuncionario));
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "ModeloAquatico", "UPDATE", modelo.getIdModeloAquatico());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.modelo.getIdModeloAquatico();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from ModeloAquatico where idModeloAquatico = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, modelo.getIdModeloAquatico());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "ModeloAquatico", "DELETE", modelo.getIdModeloAquatico());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public ModeloAquatico selectModeloAquatico(int id) {
		String sql = "select * from ModeloAquatico where idModeloAquatico = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				
				ModeloAquatico modelo = new ModeloAquatico();
				modelo.setIdModeloAquatico(rs.getInt("idModeloAquatico"));
				modelo.setNomeModelo(rs.getString("NomeModelo"));
				modelo.setPotenciaMotor(rs.getShort("PotenciaMotor"));
				modelo.setTipoMotor(rs.getByte("TipoMotor"));
				modelo.setLugares(rs.getByte("Lugares"));
				modelo.setTipoDeConducao(rs.getString("TipoDeConducao"));
				modelo.setTipoDeIgnicao(rs.getString("TipoDeIgnicao"));
				modelo.setTipoDeAgua(rs.getString("TipoDeAgua"));
				modelo.setVolumeTanque(rs.getShort("VolumeTanque"));
				modelo.setMarca(new MarcaDAO().selectMarca(rs.getInt("Marca_idMarca")));

				this.setModelo(modelo);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.modelo;
	}

	public ModeloAquatico getModelo() {
		return modelo;
	}

	public void setModelo(ModeloAquatico modelo) {
		this.modelo = modelo;
	}
}
