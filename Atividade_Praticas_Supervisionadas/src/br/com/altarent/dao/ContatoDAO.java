package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Contato;

public class ContatoDAO extends DAO {
	private Contato contato;

	public ContatoDAO() {
	}

	public ContatoDAO(Contato contato) {
		this.contato = contato;
	}
	
	
	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Contato;";
		
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Contato contato = new Contato(rs.getString("TelefonePrincipal"), rs.getString("TelefoneSecundario"),
						rs.getString("E-mail"));
				contato.setIdContato(rs.getInt("idContato"));
				lista.add(contato);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Contato (TelefonePrincipal, TelefoneSecundario, `E-mail`) values (?, ?, ?);";
		String[] id = { "idContato" };
		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, contato.getTelefonePrincipal());
			stmt.setString(2, contato.getTelefoneSecundario());
			stmt.setString(3, contato.getEmail());

			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.contato.setIdContato(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Contato", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.contato.getIdContato();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Contato set TelefonePrincipal = ?, TelefoneSecundario = ?, `E-mail` = ? where idContato = ?;";
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, contato.getTelefonePrincipal());
			stmt.setString(2, contato.getTelefoneSecundario());
			stmt.setString(3, contato.getEmail());
			stmt.setInt(4, contato.getIdContato());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Contato", "UPDATE", contato.getIdContato());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.contato.getIdContato();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Contato where idContato = ?;";
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, contato.getIdContato());
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Contato", "DELETE", contato.getIdContato());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Contato selectContato(int id) {
		String sql = "select * from Contato where idContato = ?;";
		
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Contato contato = new Contato(rs.getString("TelefonePrincipal"), rs.getString("TelefoneSecundario"),
						rs.getString("E-mail"));
				contato.setIdContato(rs.getInt("idContato"));

				this.setContato(contato);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.contato;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}
}
