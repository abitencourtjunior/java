package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Diaria;

public class DiariaDAO extends DAO {
	private Diaria diaria;
	
	public DiariaDAO(Diaria diaria) {
		this.diaria = diaria;
	}
	
	public DiariaDAO() {}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Diaria;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				Diaria diaria = new Diaria();
				
				diaria.setId(rs.getInt("idDiaria"));
				diaria.setQuantidade(rs.getByte("QtdeDias"));
				diaria.setPrecoFinal(rs.getDouble("ValorFinal"));
				
				lista.add(diaria);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Diaria (QtdeDias, ValorFinal) values (?,?);";
		String[] id = {"idDiaria"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setByte(1, diaria.getQuantidade());
			stmt.setDouble(2, diaria.getPrecoFinal());
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.diaria.setId(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Diaria", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.diaria.getId();
	}

	@Override
	public int update(int idFuncionario) {
		//M�TODO N�O UTILIZADO NESTA DAO
		return this.diaria.getId();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Diaria where idDiaria = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, diaria.getId());
			
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Diaria", "DELETE", diaria.getId());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public Diaria selectDiaria(int id) {
		String sql = "select * from Diaria where idDiaria = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Diaria diaria = new Diaria();
			
			while(rs.next()) {
				diaria.setId(rs.getInt("idDiaria"));
				diaria.setQuantidade(rs.getByte("QtdeDias"));
				diaria.setPrecoFinal(rs.getDouble("ValorFinal"));
				
				this.setDiaria(diaria);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.diaria;
	}

	public Diaria getDiaria() {
		return diaria;
	}

	public void setDiaria(Diaria diaria) {
		this.diaria = diaria;
	}
}
