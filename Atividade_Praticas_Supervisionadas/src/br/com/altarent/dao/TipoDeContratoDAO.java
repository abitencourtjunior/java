package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.TipoDeContrato;

public class TipoDeContratoDAO extends DAO {
	private TipoDeContrato tipoDeContrato;

	public TipoDeContratoDAO(TipoDeContrato tipoDeContrato) {
		this.tipoDeContrato = tipoDeContrato;
	}

	public TipoDeContratoDAO() {
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from TipoDeContrato;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				TipoDeContrato tipoDeContrato = new TipoDeContrato();
				
				tipoDeContrato.setIdTipoDeContrato(rs.getInt("idTipoDeContrato"));
				tipoDeContrato.setDiaria(new DiariaDAO().selectDiaria(rs.getInt("Diaria_idDiaria")));
				tipoDeContrato.setMensal(new MensalDAO().selectMensal(rs.getInt("Mensal_idMensal")));
				tipoDeContrato.setSemanal(new SemanalDAO().selectSemanal(rs.getInt("Semanal_idSemanal")));
				tipoDeContrato.setValor(rs.getDouble("Valor"));
				
				lista.add(tipoDeContrato);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into TipoDeContrato (Diaria_idDiaria, Semanal_idSemanal, Mensal_idMensal, Valor) values (?,?,?,?);";
		String[] id = {"idTipoDeContrato"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {	
			int diaria = verificaDiaria(idFuncionario);
			int semanal = verificaSemanal(idFuncionario);
			int mensal = verificaMensal(idFuncionario);
			double valor = 0;
			
			stmt.setInt(1, diaria);
			stmt.setInt(2, semanal);
			stmt.setInt(3, mensal);
			
			if(diaria > 1) {
				valor = new DiariaDAO().selectDiaria(diaria).getPrecoFinal();
			} else if(semanal > 1) {
				valor = new SemanalDAO().selectSemanal(semanal).getPrecoFinal();
			} else {
				valor = new MensalDAO().selectMensal(mensal).getPrecoFinal();
			}
			
			stmt.setDouble(4, valor);
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.tipoDeContrato.setIdTipoDeContrato(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "TipoDeContrato", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.tipoDeContrato.getIdTipoDeContrato();
	}

	@Override
	public int update(int idFuncionario) {
		//M�TODO N�O UTILIZADO NESTA DAO
		return 0;
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from TipoDeContrato where idTipoDeContrato = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, tipoDeContrato.getIdTipoDeContrato());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "TipoDeContrato", "DELETE", tipoDeContrato.getIdTipoDeContrato());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}

	}

	public TipoDeContrato selectTipoDeContrato(int id) {
		String sql = "select * from TipoDeContrato where idTipoDeContrato = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			TipoDeContrato tipoDeContrato = new TipoDeContrato();
			
			while(rs.next()) {
				tipoDeContrato.setIdTipoDeContrato(rs.getInt("idTipoDeContrato"));
				tipoDeContrato.setDiaria(new DiariaDAO().selectDiaria(rs.getInt("Diaria_idDiaria")));
				tipoDeContrato.setMensal(new MensalDAO().selectMensal(rs.getInt("Mensal_idMensal")));
				tipoDeContrato.setSemanal(new SemanalDAO().selectSemanal(rs.getInt("Semanal_idSemanal")));
				tipoDeContrato.setValor(rs.getDouble("Valor"));
				
				setTipoDeContrato(tipoDeContrato);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.tipoDeContrato;
	}
	
	private int verificaDiaria(int idFuncionario) {
		if(tipoDeContrato.getSemanal().getId() == 1 && tipoDeContrato.getMensal().getId() == 1) {
			return new DiariaDAO(tipoDeContrato.getDiaria()).insert(idFuncionario);
		}
		
		return 1;
	}
	
	private int verificaSemanal(int idFuncionario ) {
		if(tipoDeContrato.getDiaria().getId() == 1 && tipoDeContrato.getMensal().getId() == 1) {
			return new SemanalDAO(tipoDeContrato.getSemanal()).insert(idFuncionario);
		}
		
		return 1;
	}
	
	private int verificaMensal(int idFuncionario) {
		if(tipoDeContrato.getDiaria().getId() == 1 && tipoDeContrato.getSemanal().getId() == 1) {
			return new MensalDAO(tipoDeContrato.getMensal()).insert(idFuncionario);
		}
		
		return 1;
	}

	public TipoDeContrato getTipoDeContrato() {
		return tipoDeContrato;
	}

	public void setTipoDeContrato(TipoDeContrato tipoDeContrato) {
		this.tipoDeContrato = tipoDeContrato;
	}
}
