package br.com.altarent.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Autenticacao;

public class AutenticacaoDAO extends DAO {
	private Autenticacao autenticacao;

	public AutenticacaoDAO() {
	}

	public AutenticacaoDAO(Autenticacao autenticacao) {
		this.autenticacao = autenticacao;
	}

	@Override
	public List<Object> select() {

		List<Object> lista = new ArrayList<>();
		String sql = "select * from Autenticacao;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				Autenticacao autenticacao = new Autenticacao();
				autenticacao.setIdAutenticacao(rs.getInt("idAutenticacao"));
				autenticacao.setUsuario(rs.getString("Usuario"));
				autenticacao.setSenha(rs.getString("Senha"));
				autenticacao.setDataCriacao(rs.getDate("DataCriacao"));
				autenticacao.setTipoUsuario(rs.getString("TipoDeUsuario"));

				lista.add(autenticacao);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Autenticacao (usuario, senha, dataCriacao, tipoUsuario) values (?,?,?,?);";
		String[] id = { "idAutenticacao" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, autenticacao.getUsuario());
			stmt.setString(2, autenticacao.getSenha());
			stmt.setDate(3, Date.valueOf(autenticacao.getDataCriacao()));
			stmt.setString(4, autenticacao.getTipoUsuario());
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.autenticacao.setIdAutenticacao(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Autenticacao", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.autenticacao.getIdAutenticacao();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Autenticacao set usuario = ?, senha = ?, dataCriacao = ?, tipoUsuario = ? where idAutenticao = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, autenticacao.getUsuario());
			stmt.setString(2, autenticacao.getSenha());
			stmt.setDate(3, Date.valueOf(autenticacao.getDataCriacao()));
			stmt.setString(4, autenticacao.getTipoUsuario());
			stmt.setInt(5, autenticacao.getIdAutenticacao());

			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Autenticacao", "UPDATE", autenticacao.getIdAutenticacao());

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.autenticacao.getIdAutenticacao();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from autenticao where idAutenticao = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, autenticacao.getIdAutenticacao());
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Autenticacao", "DELETE", autenticacao.getIdAutenticacao());

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Autenticacao selectAutentica(int id) {
		String sql = "select * from Autenticacao where idAutenticacao = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();
			
			Autenticacao autenticacao = new Autenticacao();
			
			while (rs.next()) {

				autenticacao.setIdAutenticacao(rs.getInt("idAutenticacao"));
				autenticacao.setUsuario(rs.getString("Usuario"));
				autenticacao.setSenha(rs.getString("Senha"));
				autenticacao.setDataCriacao(rs.getDate("DataCriacao"));
				autenticacao.setTipoUsuario(rs.getString("TipoUsuario"));
				
				this.setAutenticacao(autenticacao);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.autenticacao;
	}
	
	public int login(String usuario, String senha) {
		String sql = "select idAutenticacao from Autenticacao where Usuario = ? and Senha = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, usuario);
			stmt.setString(2, senha);
			
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()){
               return 1;
            } else{
               return 0; 
            }
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);                      
		}
		
	}

	public Autenticacao getAutenticacao() {
		return autenticacao;
	}

	public void setAutenticacao(Autenticacao autenticacao) {
		this.autenticacao = autenticacao;
	}

}
