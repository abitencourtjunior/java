package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Endereco;

public class EnderecoDAO extends DAO {
	private Endereco endereco;
	
	public EnderecoDAO() {}
	
	public EnderecoDAO(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public List<Object> select() {
		
		List<Object> lista = new ArrayList<>();
		
		String sql = "select * from Endereco;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				Endereco endereco = new Endereco();
				endereco.setIdEndereco(rs.getInt("idEndereco"));
				endereco.setNomeRua(rs.getString("NomeRua"));
				endereco.setCEP(rs.getString("CEP"));
				endereco.setBairro(rs.getString("Bairro"));
				endereco.setCidade(rs.getString("Cidade"));
				endereco.setComplemento(rs.getString("Complemento"));
				endereco.setNumero(rs.getInt("Numero"));
				lista.add(endereco);
				
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Endereco (NomeRua, CEP, Bairro, Cidade, Complemento, Numero) values (?, ?, ?, ?, ?, ?);";
		String[] id = { "idEndereco" };

		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, endereco.getNomeRua());
			stmt.setString(2, endereco.getCep());
			stmt.setString(3, endereco.getBairro());
			stmt.setString(4, endereco.getCidade());
			stmt.setString(5, endereco.getComplemento());
			stmt.setInt(6, endereco.getNumero());
			
			stmt.execute();
				
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.endereco.setIdEndereco(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Endereco", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		return this.endereco.getIdEndereco();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Endereco set NomeRua = ?, CEP = ?, Bairro = ?, Cidade = ?, Complemento = ?, Numero = ? "
				+ "where idEndereco = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, endereco.getNomeRua());
			stmt.setString(2, endereco.getCep());
			stmt.setString(3, endereco.getBairro());
			stmt.setString(4, endereco.getCidade());
			stmt.setString(5, endereco.getComplemento());
			stmt.setInt(6, endereco.getNumero());
			stmt.setInt(7, endereco.getIdEndereco());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Endereco", "UPDATE", endereco.getIdEndereco());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.endereco.getIdEndereco();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Endereco where idEndereco = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, endereco.getIdEndereco());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Endereco", "DELETE", endereco.getIdEndereco());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public Endereco selectEndereco(int id) {
		String sql = "select * from Endereco where idEndereco = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
			Endereco endereco = new Endereco();
			endereco.setIdEndereco(rs.getInt("idEndereco"));
			endereco.setNomeRua(rs.getString("NomeRua"));
			endereco.setCEP(rs.getString("CEP"));
			endereco.setBairro(rs.getString("Bairro"));
			endereco.setCidade(rs.getString("Cidade"));
			endereco.setComplemento(rs.getString("Complemento"));
			endereco.setNumero(rs.getInt("Numero"));
			
			this.setEndereco(endereco);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.endereco;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}
