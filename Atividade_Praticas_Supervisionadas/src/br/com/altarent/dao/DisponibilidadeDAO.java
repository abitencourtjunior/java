package br.com.altarent.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Disponibilidade;

public class DisponibilidadeDAO extends DAO {
	private Disponibilidade disponibilidade;
	
	public DisponibilidadeDAO() {}
	
	public DisponibilidadeDAO(Disponibilidade disponibilidade) {
		this.disponibilidade = disponibilidade;
	}
	
	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		
		String sql = "select * from Disponibilidade;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				
				Disponibilidade disponibilidade = new Disponibilidade();
				
				disponibilidade.setIdDisponibilidade(rs.getInt("idDisponibilidade"));
				disponibilidade.setData(rs.getDate("Data"));
				disponibilidade.setHora(rs.getTime("Hora"));
				disponibilidade.setDisponivel(new DisponivelDAO().selectDisponivel(rs.getInt("Disponivel_idDisponivel")));
				
				lista.add(disponibilidade);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Disponibilidade (Data, Hora, Disponivel_idDisponivel) values (?,?,?);";
		String [] id = {"idDisponibilidade"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setDate(1, Date.valueOf(disponibilidade.getData()));
			stmt.setTime(2, Time.valueOf(disponibilidade.getHora()));
			stmt.setInt(3, new DisponivelDAO().selectDisponivel(disponibilidade.getDisponivel().getId()).getId());
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.disponibilidade.setIdDisponibilidade(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Disponibilidade", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		return this.disponibilidade.getIdDisponibilidade();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Disponibilidade set Data = ?, Hora = ?, Disponivel_idDisponivel = ? where idDisponibilidade = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setDate(1, Date.valueOf(disponibilidade.getData()));
			stmt.setTime(2, Time.valueOf(disponibilidade.getHora()));
			stmt.setInt(3, new DisponivelDAO().selectDisponivel(disponibilidade.getDisponivel().getId()).getId());
			stmt.setInt(4, disponibilidade.getIdDisponibilidade());
			
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Disponibilidade", "UPDATE", disponibilidade.getIdDisponibilidade());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.disponibilidade.getIdDisponibilidade();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Disponibilidade where idDisponibilidade = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, disponibilidade.getIdDisponibilidade());
			
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Disponibilidade", "DELETE", disponibilidade.getIdDisponibilidade());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public int updateDisponibilidade(int idFuncionario, int idDisponibilidade, LocalDate data) {
		
		String sql = "update Disponibilidade set Data = ?, Hora = ?, Disponivel_idDisponivel = '4' where idDisponibilidade = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			stmt.setDate(1, Date.valueOf(data));
			stmt.setTime(2, Time.valueOf(LocalTime.now()));
			stmt.setInt(3, idDisponibilidade);
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Disponibilidade", "UPDATE", idDisponibilidade);
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return idDisponibilidade;
	}
	
	public Disponibilidade selectDisponibilidade(int id) {
		String sql = "select * from Disponibilidade where idDisponibilidade = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Disponibilidade disponibilidade = new Disponibilidade();
			
			while(rs.next()) {
				disponibilidade.setIdDisponibilidade(rs.getInt("idDisponibilidade"));
				disponibilidade.setData(rs.getDate("Data"));
				disponibilidade.setHora(rs.getTime("Hora"));
				disponibilidade.setDisponivel(new DisponivelDAO().selectDisponivel(rs.getInt("Disponivel_idDisponivel")));
				
				this.setDisponibilidade(disponibilidade);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.disponibilidade;
	}

	public Disponibilidade getDisponibilidade() {
		return disponibilidade;
	}

	public void setDisponibilidade(Disponibilidade disponibilidade) {
		this.disponibilidade = disponibilidade;
	}
}
