package br.com.altarent.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Cliente;
import br.com.altarent.modelo.Diaria;
import br.com.altarent.modelo.Funcionario;
import br.com.altarent.modelo.Loja;
import br.com.altarent.modelo.Mensal;
import br.com.altarent.modelo.Orcamento;
import br.com.altarent.modelo.Produto;
import br.com.altarent.modelo.Semanal;
import br.com.altarent.modelo.TipoDeContrato;

public class OrcamentoDAO extends DAO {
	private Orcamento orcamento;
	
	public OrcamentoDAO() {}
	
	public OrcamentoDAO(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		
		String sql = "select * from Orcamento;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				
				Orcamento orcamento = new Orcamento();
				
				orcamento.setIdOrcamento(rs.getInt("idOrcamento"));
				orcamento.setDataEntrega(rs.getDate("DataEntrega"));
				orcamento.setDataRetorno(rs.getDate("DataRetorno"));
				orcamento.setProduto(new ProdutoDAO().selectProduto(rs.getInt("Produto_idProduto")));
				orcamento.setTipoDeContrato(new TipoDeContratoDAO().selectTipoDeContrato(rs.getInt("TipoDeContrato_idTipoDeContrato")));
				orcamento.setLoja(new LojaDAO().selectLoja(rs.getInt("Loja_idLoja")));
				orcamento.setFuncionario(new FuncionarioDAO().selectFuncionario(rs.getInt("Funcionario_idFuncionario")));
				orcamento.setCliente(new ClienteDAO().selectCliente(rs.getInt("Cliente_idCliente")));
				
				lista.add(orcamento);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
		
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Orcamento (Produto_idProduto, TipoDeContrato_idTipoDeContrato, Loja_idLoja, Funcionario_idFuncionario, "
				+ "Cliente_idCliente, DataEntrega, DataRetorno) values (?,?,?,?,?,?,?);";
		String[] id = {"idOrcamento"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, new ProdutoDAO().selectProduto(orcamento.getProduto().getIdProduto()).getIdProduto());
			stmt.setInt(2, new TipoDeContratoDAO(orcamento.getTipoDeContrato()).insert(idFuncionario));
			stmt.setInt(3, new LojaDAO().selectLoja(orcamento.getLoja().getIdLoja()).getIdLoja());
			stmt.setInt(4, new FuncionarioDAO().selectFuncionario(orcamento.getFuncionario().getIdade()).getIdFuncionario());
			stmt.setInt(5, new ClienteDAO().selectCliente(orcamento.getCliente().getIdCliente()).getIdCliente());
			stmt.setDate(6, Date.valueOf(orcamento.getDataEntrega()));
			stmt.setDate(7, Date.valueOf(orcamento.getDataRetorno()));
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.orcamento.setIdOrcamento(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Orcamento", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.orcamento.getIdOrcamento();
	}
	
	public int insertOrcamentoCarro(int idFuncionario, String documentoPessoa, String placa, TipoDeContrato tipoDeContrato) {
		String sql = "insert into Orcamento (Produto_idProduto, TipoDeContrato_idTipoDeContrato, Loja_idLoja, Funcionario_idFuncionario, "
				+ "Cliente_idCliente, DataEntrega, DataRetorno) values (?,?,?,?,?,?,?);";
		String[] id = {"idOrcamento"};
		int idOrcamento = 1;
		int idCliente = 1;
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			
			if(documentoPessoa.length() == 14) {
				idCliente = new ClienteDAO().selectIdClienteCpf(documentoPessoa);
			} else {
				idCliente = new ClienteDAO().selectIdClienteCnpj(documentoPessoa);
			}
			
			stmt.setInt(1, new ProdutoDAO().selectIdProdutoPlaca(placa));
			stmt.setInt(2, new TipoDeContratoDAO(tipoDeContrato).insert(idFuncionario));
			stmt.setInt(3, orcamento.getLoja().getIdLoja());
			stmt.setInt(4, idFuncionario);
			stmt.setInt(5, idCliente);
			stmt.setDate(6, Date.valueOf(orcamento.getDataEntrega()));
			stmt.setDate(7, Date.valueOf(orcamento.getDataRetorno()));
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				idOrcamento = rs.getInt(1);
				ProceduresDAO.registraLog(idFuncionario, "Orcamento", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return idOrcamento;
	}
	
	public int insertOrcamentoJetski(int idFuncionario, String documentoPessoa, int documento, TipoDeContrato tipoDeContrato) {
		String sql = "insert into Orcamento (Produto_idProduto, TipoDeContrato_idTipoDeContrato, Loja_idLoja, Funcionario_idFuncionario, "
				+ "Cliente_idCliente, DataEntrega, DataRetorno) values (?,?,?,?,?,?,?);";
		String[] id = {"idOrcamento"};
		int idOrcamento = 1;
		int idCliente = 1;
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			
			if(documentoPessoa.length() == 14) {
				idCliente = new ClienteDAO().selectIdClienteCpf(documentoPessoa);
			} else {
				idCliente = new ClienteDAO().selectIdClienteCnpj(documentoPessoa);
			}
			
			stmt.setInt(1, new ProdutoDAO().selectIdProdutoDocumento(documento));
			stmt.setInt(2, new TipoDeContratoDAO(tipoDeContrato).insert(idFuncionario));
			stmt.setInt(3, orcamento.getLoja().getIdLoja());
			stmt.setInt(4, idFuncionario);
			stmt.setInt(5, idCliente);
			stmt.setDate(6, Date.valueOf(orcamento.getDataEntrega()));
			stmt.setDate(7, Date.valueOf(orcamento.getDataRetorno()));
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				idOrcamento = rs.getInt(1);
				ProceduresDAO.registraLog(idFuncionario, "Orcamento", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return idOrcamento;
	}


	@Override
	public int update(int idFuncionario) {
		String sql = "update Orcamento set Produto_idProduto = ?, TipoDeContrato_idTipoDeContrato = ?, DataEntrega = ?, DataRetorno = ? "
				+ "where idOrcamento= ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new ProdutoDAO().selectProduto(orcamento.getProduto().getIdProduto()).getIdProduto());
			stmt.setInt(2, new TipoDeContratoDAO(orcamento.getTipoDeContrato()).update(idFuncionario));
			stmt.setDate(3, Date.valueOf(orcamento.getDataEntrega()));
			stmt.setDate(4, Date.valueOf(orcamento.getDataRetorno()));
			stmt.setInt(5, orcamento.getIdOrcamento());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Orcamento", "UPDATE", orcamento.getIdOrcamento());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.orcamento.getIdOrcamento();
	}
	
	public int updateTipoDeContratoOrcamento(int idFuncionario) {
		String sql = "update Orcamento set TipoDeContrato_idTipoDeContrato = ?, DataEntrega = ?, DataRetorno = ? where idOrcamento = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new TipoDeContratoDAO(orcamento.getTipoDeContrato()).insert(idFuncionario));
			stmt.setDate(2, Date.valueOf(orcamento.getDataEntrega()));
			stmt.setDate(3, Date.valueOf(orcamento.getDataRetorno()));
			stmt.setInt(4, orcamento.getIdOrcamento());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Orcamento", "UPDATE", orcamento.getIdOrcamento());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.orcamento.getIdOrcamento();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Orcamento where idOrcamento = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, orcamento.getIdOrcamento());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Orcamento", "DELETE", orcamento.getIdOrcamento());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public Orcamento selectOrcamento(int id) {
		String sql = "select * from Orcamento where idOrcamento = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Orcamento orcamento = new Orcamento();
			
			while(rs.next()) {
				
				orcamento.setIdOrcamento(rs.getInt("idOrcamento"));
				orcamento.setDataEntrega(rs.getDate("DataEntrega"));
				orcamento.setDataRetorno(rs.getDate("DataRetorno"));
				orcamento.setProduto(new ProdutoDAO().selectProduto(rs.getInt("Produto_idProduto")));
				orcamento.setTipoDeContrato(new TipoDeContratoDAO().selectTipoDeContrato(rs.getInt("TipoDeContrato_idTipoDeContrato")));
				orcamento.setLoja(new LojaDAO().selectLoja(rs.getInt("Loja_idLoja")));
				orcamento.setFuncionario(new FuncionarioDAO().selectFuncionario(rs.getInt("Funcionario_idFuncionario")));
				orcamento.setCliente(new ClienteDAO().selectCliente(rs.getInt("Cliente_idCliente")));
				
				this.setOrcamento(orcamento);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
		
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.orcamento;
	}
	
	public List<Orcamento> selectOrcamento() {
		List<Orcamento> lista = new ArrayList<>();
		List<Cliente> listaCliente = new ClienteDAO().selectCliente();
		List<Funcionario> listaFuncionario = new FuncionarioDAO().selectFuncionario();
		List<Produto> listaProduto = new ProdutoDAO().selectProduto();
		List<Loja> listaLoja = new LojaDAO().selectLoja();
		
		String sql = "select * from orcamento\r\n" + 
				"join tipodecontrato on tipodecontrato.idTipoDeContrato = orcamento.TipoDeContrato_idTipoDeContrato\r\n" + 
				"join diaria on diaria.idDiaria = tipodecontrato.Diaria_idDiaria\r\n" + 
				"join semanal on semanal.idSemanal = tipodecontrato.Semanal_idSemanal\r\n" + 
				"join mensal on mensal.idMensal = tipodecontrato.Mensal_idMensal;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Orcamento orcamento = new Orcamento();
				orcamento.setCliente(new Cliente());
				orcamento.setFuncionario(new Funcionario());
				orcamento.setLoja(new Loja());
				orcamento.setProduto(new Produto());
				orcamento.setTipoDeContrato(new TipoDeContrato(new Diaria(), new Semanal(), new Mensal()));
				
				orcamento.setIdOrcamento(rs.getInt("idOrcamento"));
				orcamento.getProduto().setIdProduto(rs.getInt("Produto_idProduto"));
				orcamento.getTipoDeContrato().setIdTipoDeContrato(rs.getInt("TipoDeContrato_idTipoDeContrato"));
				orcamento.getLoja().setIdLoja(rs.getInt("Loja_idLoja"));
				orcamento.getFuncionario().setIdFuncionario(rs.getInt("Funcionario_idFuncionario"));
				orcamento.getCliente().setIdCliente(rs.getInt("Cliente_idCliente"));
				orcamento.setDataEntrega(rs.getDate("DataEntrega"));
				orcamento.setDataRetorno(rs.getDate("DataRetorno"));
				orcamento.getTipoDeContrato().getDiaria().setId(rs.getInt("Diaria_idDiaria"));
				orcamento.getTipoDeContrato().getSemanal().setId(rs.getInt("Semanal_idSemanal"));
				orcamento.getTipoDeContrato().getMensal().setId(rs.getInt("Mensal_idMensal"));
				orcamento.getTipoDeContrato().setValor(rs.getInt("Valor"));
				orcamento.getTipoDeContrato().getDiaria().setQuantidade(rs.getByte("QtdeDias"));
				orcamento.getTipoDeContrato().getDiaria().setPrecoFinal(rs.getDouble("ValorFinal"));
				orcamento.getTipoDeContrato().getSemanal().setQuantidade(rs.getByte("QtdeSemanas"));
				orcamento.getTipoDeContrato().getSemanal().setPrecoFinal(rs.getDouble("ValorFinal"));
				orcamento.getTipoDeContrato().getMensal().setQuantidade(rs.getByte("QtdeMes"));
				orcamento.getTipoDeContrato().getMensal().setPrecoFinal(rs.getDouble("ValorFinal"));
				
				listaCliente.forEach(cliente -> {
					if(cliente.getIdCliente() == orcamento.getCliente().getIdCliente()) {
						orcamento.setCliente(cliente);
					}
				});
				
				listaFuncionario.forEach(funcionario -> {
					if(funcionario.getIdade() == orcamento.getFuncionario().getIdFuncionario()) {
						orcamento.setFuncionario(funcionario);
					}
				});
				
				listaProduto.forEach(produto -> {
					if(produto.getIdProduto() == orcamento.getProduto().getIdProduto()) {
						orcamento.setProduto(produto);
					}
				});
				
				listaLoja.forEach(loja -> {
					if(loja.getIdLoja() == orcamento.getLoja().getIdLoja()) {
						orcamento.setLoja(loja);
					}
				});
				
				lista.add(orcamento);
				
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
		
	}

	public Orcamento getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
}
