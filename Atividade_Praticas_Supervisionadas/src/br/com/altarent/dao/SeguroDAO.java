package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Seguro;

public class SeguroDAO extends DAO {
	private Seguro seguro;
	
	public SeguroDAO() {}
	
	public SeguroDAO(Seguro seguro) {
		this.seguro = seguro;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Seguro;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.execute();
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				
				Seguro seguro = new Seguro();
				
				seguro.setIdSeguro(rs.getInt("idSeguro"));
				seguro.setValor(rs.getFloat("Valor"));
				seguro.setPessoaJuridica(new PessoaJuridicaDAO().selectPessoaJuridica(rs.getInt("PessoaJuridica_idPessoaJuridica")));
				
				lista.add(seguro);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Seguro (Valor, PessoaJuridica_idPessoaJuridica) values (?,?);";
		String [] id = {"idSeguro"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setFloat(1, seguro.getValor());
			stmt.setInt(2, new PessoaJuridicaDAO(seguro.getPessoaJuridica()).selectPessoaJuridica(seguro.getPessoaJuridica().getIdPessoaJuridica())
					.getIdPessoaJuridica());
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.seguro.setIdSeguro(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Seguro", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException();
		}
		
		return this.seguro.getIdSeguro();
	}

	@Override
	public int update(int idFuncionario) {
		String sql ="update Seguro set Valor = ?, PessoaJuridica_idPessoaJuridica = ? where idSeguro = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setFloat(1, seguro.getValor());
			stmt.setInt(2, new PessoaJuridicaDAO(seguro.getPessoaJuridica()).update(idFuncionario));
			stmt.setInt(3, seguro.getIdSeguro());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Seguro", "UPDATE", seguro.getIdSeguro());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.seguro.getIdSeguro();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Seguro where idSeguro = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, seguro.getIdSeguro());
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Seguro", "DELETE", seguro.getIdSeguro());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}

	}
	
	public Seguro selectSeguro(int id) {
		String sql = "select * from Seguro where idSeguro = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Seguro seguro = new Seguro();
			
			while(rs.next()) {
				seguro.setIdSeguro(rs.getInt("idSeguro"));
				seguro.setValor(rs.getFloat("Valor"));
				seguro.setPessoaJuridica(new PessoaJuridicaDAO().selectPessoaJuridica(rs.getInt("PessoaJuridica_idPessoaJuridica")));
				
				this.setSeguro(seguro);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		return this.seguro;
	}

	public Seguro getSeguro() {
		return seguro;
	}

	public void setSeguro(Seguro seguro) {
		this.seguro = seguro;
	}
}
