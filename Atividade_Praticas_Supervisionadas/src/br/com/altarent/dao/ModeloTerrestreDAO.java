package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.ModeloTerrestre;

public class ModeloTerrestreDAO extends DAO {
	private ModeloTerrestre modelo;

	public ModeloTerrestreDAO() {
	}

	public ModeloTerrestreDAO(ModeloTerrestre modelo) {
		this.modelo = modelo;
	}

	@Override
	public List<Object> select() {

		List<Object> lista = new ArrayList<>();
		String sql = "select * from ModeloTerrestre;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				ModeloTerrestre modelo = new ModeloTerrestre();
				modelo.setIdModeloTerrestre(rs.getInt("idModeloTerrestre"));
				modelo.setNomeModelo(rs.getString("NomeModelo"));
				modelo.setCambio(rs.getString("Cambio"));
				modelo.setArCondicionado(rs.getBoolean("ArCondicionado"));
				modelo.setDirecao(rs.getString("Direcao"));
				modelo.setVidrosEletricos(rs.getBoolean("VidrosEletricos"));
				modelo.setPortas(rs.getByte("Portas"));
				modelo.setTravaEletrica(rs.getBoolean("TravaEletrica"));
				modelo.setAirBag(rs.getBoolean("AirBag"));
				modelo.setAbs(rs.getBoolean("ABS"));
				modelo.setCapacidadePessoas(rs.getByte("CapacidadePessoas"));
				modelo.setLitragemPortaMala(rs.getShort("LitragemPortaMalas"));
				modelo.setMarca(new MarcaDAO().selectMarca(rs.getInt("Marca_idMarca")));

				lista.add(modelo);
			}
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into ModeloTerrestre (NomeModelo, Cambio, ArCondicionado, Direcao, VidrosEletricos, Portas, TravaEletrica, "
				+ "AirBag, ABS, CapacidadePessoas, LitragemPortaMalas, Marca_idMarca) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		String[] id = { "idModeloTerrestre" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, modelo.getNomeModelo());
			stmt.setString(2, modelo.getCambio());
			stmt.setBoolean(3, modelo.isArCondicionado());
			stmt.setString(4, modelo.getDirecao());
			stmt.setBoolean(5, modelo.isVidrosEletricos());
			stmt.setByte(6, modelo.getPortas());
			stmt.setBoolean(7, modelo.isTravaEletrica());
			stmt.setBoolean(8, modelo.isAirBag());
			stmt.setBoolean(9, modelo.isAbs());
			stmt.setByte(10, modelo.getCapacidadePessoas());
			stmt.setShort(11, modelo.getLitragemPortaMala());
			stmt.setInt(12, new MarcaDAO(modelo.getMarca()).insert(idFuncionario));
			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.modelo.setIdModeloTerrestre(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "ModeloTerrestre", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.modelo.getIdModeloTerrestre();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update ModeloTerrestre set NomeModelo = ?, Cambio = ?, ArCondicionado = ?, Direcao = ?, VidrosEletricos = ?, "
				+ "Portas = ?, TravaEletrica = ?, AirBag = ?, ABS = ?, CapacidadePessoas = ?, LitragemPortaMalas = ? where idModeloTerreste = ?;";
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, modelo.getNomeModelo());
			stmt.setString(2, modelo.getCambio());
			stmt.setBoolean(3, modelo.isArCondicionado());
			stmt.setString(4, modelo.getDirecao());
			stmt.setBoolean(5, modelo.isVidrosEletricos());
			stmt.setByte(6, modelo.getPortas());
			stmt.setBoolean(7, modelo.isTravaEletrica());
			stmt.setBoolean(8, modelo.isAirBag());
			stmt.setBoolean(9, modelo.isAbs());
			stmt.setByte(10, modelo.getCapacidadePessoas());
			stmt.setShort(11, modelo.getLitragemPortaMala());
			stmt.setInt(12, modelo.getIdModeloTerrestre());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "ModeloTerrestre", "UPDATE", modelo.getIdModeloTerrestre());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.modelo.getIdModeloTerrestre();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from ModeloTerrestre where idModeloTerrestre = ?;";
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, modelo.getIdModeloTerrestre());
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "ModeloTerrestre", "DELETE", modelo.getIdModeloTerrestre());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public ModeloTerrestre selectModeloTerrestre(int id) {
		String sql = "select * from ModeloTerrestre where idModeloTerrestre = ?;";
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				
				ModeloTerrestre modelo = new ModeloTerrestre();
				modelo.setIdModeloTerrestre(rs.getInt("idModeloTerrestre"));
				modelo.setNomeModelo(rs.getString("NomeModelo"));
				modelo.setCambio(rs.getString("Cambio"));
				modelo.setArCondicionado(rs.getBoolean("ArCondicionado"));
				modelo.setDirecao(rs.getString("Direcao"));
				modelo.setVidrosEletricos(rs.getBoolean("VidrosEletricos"));
				modelo.setPortas(rs.getByte("Portas"));
				modelo.setTravaEletrica(rs.getBoolean("TravaEletrica"));
				modelo.setAirBag(rs.getBoolean("AirBag"));
				modelo.setAbs(rs.getBoolean("ABS"));
				modelo.setCapacidadePessoas(rs.getByte("CapacidadePessoas"));
				modelo.setLitragemPortaMala(rs.getShort("LitragemPortaMalas"));
				modelo.setMarca(new MarcaDAO().selectMarca(rs.getInt("Marca_idMarca")));

				this.setModelo(modelo);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.modelo;
	}

	public ModeloTerrestre getModelo() {
		return modelo;
	}

	public void setModelo(ModeloTerrestre modelo) {
		this.modelo = modelo;
	}
}