package br.com.altarent.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Contato;
import br.com.altarent.modelo.Endereco;
import br.com.altarent.modelo.PessoaFisica;

public class PessoaFisicaDAO extends DAO {
	private PessoaFisica pessoa;

	public PessoaFisicaDAO() {
	}

	public PessoaFisicaDAO(PessoaFisica pessoa) {
		this.pessoa = pessoa;
	}
	
	//M�todo para o Exerc�cio 17 da Lista de Banco de Dados
	public List<PessoaFisica> selectPessoaNomeCpfPeloSexo(String sexo) {
		
		List<PessoaFisica> lista = new ArrayList<>();
		
		String sql = "select * from PessoaFisica where Sexo = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, sexo);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				lista.add(new PessoaFisicaDAO().selectPessoaFisica(rs.getInt("idPessoaFisica")));
			}

		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from PessoaFisica;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				PessoaFisica pessoa = new PessoaFisica();

				pessoa.setIdPessoaFisica(rs.getInt("idPessoaFisica"));
				pessoa.setNome(rs.getString("Nome"));
				pessoa.setDataNascimento(rs.getDate("DataNascimento"));
				pessoa.setCpf(rs.getString("CPF"));
				pessoa.setSexo(rs.getString("Sexo"));
				pessoa.setCnh(rs.getString("CNH"));
				pessoa.setCha(rs.getString("CHA"));
				pessoa.setCategoriaHabilitacao(rs.getString("CategoriaHab"));
				pessoa.setContato(new ContatoDAO().selectContato(rs.getInt("Contato_idContato")));
				pessoa.setEndereco(new EnderecoDAO().selectEndereco(rs.getInt("Endereco_idEndereco")));

				lista.add(pessoa);
			}

		} catch (SQLException ex) {
			throw new RuntimeException();
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into PessoaFisica (Nome, DataNascimento, CPF, Sexo, CNH, CHA, CategoriaHab, Contato_idContato, Endereco_idEndereco) "
				+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?);";
		String[] id = { "idPessoaFisica" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, pessoa.getNome());
			stmt.setDate(2, Date.valueOf(pessoa.getDataNascimento()));
			stmt.setString(3, pessoa.getCpf());
			stmt.setString(4, pessoa.getSexo());
			stmt.setString(5, pessoa.getCnh());
			stmt.setString(6, pessoa.getCha());
			stmt.setString(7, pessoa.getCategoriaHabilitacao());
			stmt.setInt(8, new ContatoDAO(pessoa.getContato()).insert(idFuncionario));
			stmt.setInt(9, new EnderecoDAO(pessoa.getEndereco()).insert(idFuncionario));

			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.pessoa.setIdPessoaFisica(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "PessoaFisica", "INSERT", rs.getInt(1));
			}

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.pessoa.getIdPessoaFisica();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update PessoaFisica set Nome = ?, DataNascimento = ?, CPF = ?, Sexo = ?, CNH = ?, CHA = ?, "
				+ "CategoriaHab = ?, Contato_idContato = ?, Endereco_idEndereco = ? where idPessoaFisica = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, pessoa.getNome());
			stmt.setDate(2, Date.valueOf(pessoa.getDataNascimento()));
			stmt.setString(3, pessoa.getCpf());
			stmt.setString(4, pessoa.getSexo());
			stmt.setString(5, pessoa.getCnh());
			stmt.setString(6, pessoa.getCha());
			stmt.setString(7, pessoa.getCategoriaHabilitacao());
			stmt.setInt(8, new ContatoDAO(pessoa.getContato()).update(idFuncionario));
			stmt.setInt(9, new EnderecoDAO(pessoa.getEndereco()).update(idFuncionario));
			stmt.setInt(10, pessoa.getIdPessoaFisica());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "PessoaFisica", "UPDATE", pessoa.getIdPessoaFisica());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.pessoa.getIdPessoaFisica();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from PessoaFisica where idPessoaFisica = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, pessoa.getIdPessoaFisica());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "PessoaFisica", "DELETE", pessoa.getIdPessoaFisica());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public PessoaFisica selectPessoaFisica(int id) {
		String sql = "select * from PessoaFisica where idPessoaFisica = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			PessoaFisica pessoa = new PessoaFisica();
			
			while (rs.next()) {
				
				pessoa.setIdPessoaFisica(rs.getInt("idPessoaFisica"));
				pessoa.setNome(rs.getString("Nome"));
				pessoa.setDataNascimento(rs.getDate("DataNascimento"));
				pessoa.setCpf(rs.getString("CPF"));
				pessoa.setSexo(rs.getString("Sexo"));
				pessoa.setCnh(rs.getString("CNH"));
				pessoa.setCha(rs.getString("CHA"));
				pessoa.setCategoriaHabilitacao(rs.getString("CategoriaHab"));
				pessoa.setContato(new ContatoDAO().selectContato(rs.getInt("Contato_idContato")));
				pessoa.setEndereco(new EnderecoDAO().selectEndereco(rs.getInt("Endereco_idEndereco")));

				this.setPessoa(pessoa);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.pessoa;
	}
	
	public List<PessoaFisica> selectPessoaFisica() {
		List<PessoaFisica> lista = new ArrayList<>();
		String sql = "select * from PessoaFisica join endereco on endereco.idEndereco = pessoafisica.Endereco_idEndereco\r\n" + 
				"join contato on contato.idContato = pessoafisica.Contato_idContato\r\n" + 
				"order by idPessoaFisica;;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);	
			
			while(rs.next()) {
				
				PessoaFisica pessoa = new PessoaFisica();
				pessoa.setContato(new Contato());
				pessoa.setEndereco(new Endereco());
				
				pessoa.setIdPessoaFisica(rs.getInt("idPessoaFisica"));
				pessoa.setNome(rs.getString("Nome"));
				pessoa.setDataNascimento(rs.getDate("DataNascimento"));
				pessoa.setCpf(rs.getString("CPF"));
				pessoa.setSexo(rs.getString("Sexo"));
				pessoa.setCnh(rs.getString("CNH"));
				pessoa.setCha(rs.getString("CHA"));
				pessoa.setCategoriaHabilitacao(rs.getString("CategoriaHab"));
				pessoa.getEndereco().setIdEndereco(rs.getInt("Endereco_idEndereco"));
				pessoa.getContato().setIdContato(rs.getInt("Contato_idContato"));
				pessoa.getEndereco().setNomeRua(rs.getString("NomeRua"));
				pessoa.getEndereco().setCEP(rs.getString("CEP"));
				pessoa.getEndereco().setBairro(rs.getString("Bairro"));
				pessoa.getEndereco().setCidade(rs.getString("Cidade"));
				pessoa.getEndereco().setComplemento(rs.getString("Complemento"));
				pessoa.getEndereco().setNumero(rs.getInt("Numero"));
				pessoa.getContato().setTelefonePrincipal(rs.getString("TelefonePrincipal"));
				pessoa.getContato().setTelefoneSecundario(rs.getString("TelefoneSecundario"));
				pessoa.getContato().setEmail(rs.getString("E-mail"));
				
				lista.add(pessoa);
				
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	public PessoaFisica getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaFisica pessoa) {
		this.pessoa = pessoa;
	}
}
