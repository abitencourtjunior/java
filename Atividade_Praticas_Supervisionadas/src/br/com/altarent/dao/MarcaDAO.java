package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Marca;

public class MarcaDAO extends DAO {
	private Marca marca;

	public MarcaDAO() {
	}

	public MarcaDAO(Marca marca) {
		this.marca = marca;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Marca;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.execute();
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				Marca marca = new Marca();
				marca.setIdMarca(rs.getInt("idMarca"));
				marca.setNome(rs.getString("Nome"));
			
				
				lista.add(marca);
			}
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Marca (Nome) value (?);";
		String[] id = {"idMarca"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, marca.getNome());
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.marca.setIdMarca(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Marca", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.marca.getIdMarca();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Marca set Nome = ? where idMarca = ?;";
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, marca.getNome());
			stmt.setInt(2, marca.getIdMarca());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Marca", "UPDATE", marca.getIdMarca());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		return this.marca.getIdMarca();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Marca where idMarca = ?";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, marca.getIdMarca());
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Marca", "DELETE", marca.getIdMarca());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Marca selectMarca(int id) {
		String sql = "select * from Marca where idMarca = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			Marca marca = new Marca();

			while (rs.next()) {

				marca.setIdMarca(rs.getInt("idMarca"));
				marca.setNome(rs.getString("Nome"));
				
				this.setMarca(marca); 

			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.marca;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}
}
