package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Semanal;

public class SemanalDAO extends DAO {
	private Semanal semanal;

	public SemanalDAO() {
	}

	public SemanalDAO(Semanal semanal) {
		this.semanal = semanal;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Semanal;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Semanal semanal = new Semanal();

				semanal.setId(rs.getInt("idSemanal"));
				semanal.setQuantidade(rs.getByte("QtdeSemanas"));
				semanal.setPrecoFinal(rs.getDouble("ValorFinal"));

				lista.add(semanal);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Semanal (QtdeSemanas, ValorFinal) values (?,?);";
		String[] id = { "idSemanal" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setByte(1, semanal.getQuantidade());
			stmt.setDouble(2, semanal.getPrecoFinal());

			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.semanal.setId(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Semanal", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.semanal.getId();
	}

	@Override
	public int update(int idFuncionario) {
		// M�TODO N�O UTILIZADO NESTA DAO
		return this.semanal.getId();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Semanal where idSemanal = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, semanal.getId());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Semanal", "DELETE", semanal.getId());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Semanal selectSemanal(int id) {
		String sql = "select * from Semanal where idSemanal = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			Semanal semanal = new Semanal();

			while (rs.next()) {
				semanal.setId(rs.getInt("idSemanal"));
				semanal.setQuantidade(rs.getByte("QtdeSemanas"));
				semanal.setPrecoFinal(rs.getDouble("ValorFinal"));

				this.setSemanal(semanal);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.semanal;
	}

	public Semanal getSemanal() {
		return semanal;
	}

	public void setSemanal(Semanal semanal) {
		this.semanal = semanal;
	}
}
