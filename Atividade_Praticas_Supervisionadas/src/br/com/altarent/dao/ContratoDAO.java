package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Cliente;
import br.com.altarent.modelo.Contrato;

public class ContratoDAO extends DAO {
	private Contrato contrato;
	
	public ContratoDAO() {}
	
	public ContratoDAO(Contrato contrato) {
		this.contrato = contrato;
	}
	
	//M�todo do exerc�cio 10 da Lista de Banco De Dados
	public int selectContratosVindosDeOrca() {
		String sql = "select count(Contrato.idContrato) from Contrato where Orcamento_idOrcamento > 1;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			int resultado = 0;
			
			while(rs.next()) {
				resultado = rs.getInt(1);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
			return resultado;
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
	}
	
	//M�todo para o Exerc�cio 2 da Lista de Banco De Dados!
		public Cliente selectClienteComMaisAlugueis() {
			String sql = "select Cliente.* from Contrato join Orcamento on Contrato.Orcamento_idOrcamento = Orcamento.idOrcamento " + 
					"join Cliente on Orcamento.Cliente_idCliente = Cliente.idCliente " + 
					"join PessoaFisica on Cliente.PessoaFisica_idPessoaFisica = PessoaFisica_idPessoaFisica " + 
					"group by Orcamento.idOrcamento order by count(idCliente) desc limit 1;";
			
			try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
				ResultSet rs = stmt.executeQuery();
				
				Cliente cliente = new Cliente();
				
				while(rs.next()) {
					
					cliente = new ClienteDAO().selectCliente(rs.getInt("idCliente"));		
				}
				
				stmt.closeOnCompletion();
				rs.close();
				conexao.close();
				
				return cliente;
				
			} catch(SQLException ex) {
				throw new RuntimeException(ex);
			}
		}

	
	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		
		String sql = "select * from Contrato;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				
				Contrato contrato = new Contrato();
				
				contrato.setIdContrato(rs.getInt("idContrato"));
				contrato.setOrcamento(new OrcamentoDAO().selectOrcamento(rs.getInt("Orcamento_idOrcamento")));
				contrato.setVenda(new VendaDAO().selectVenda(rs.getInt("Venda_idVenda")));
				
				lista.add(contrato);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
		
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "inset into Contrato (Orcamento_idOrcamento, Venda_idVenda) values (?,?);";
		String[] id = {"idContrato"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, new OrcamentoDAO(contrato.getOrcamento()).insert(idFuncionario));
			stmt.setInt(2, new VendaDAO(contrato.getVenda()).insert(idFuncionario));
			
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.contrato.setIdContrato(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Contrato", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.contrato.getIdContrato();
	}

	@Override
	public int update(int idFuncionario) {
		//M�todo n�o utilizado nesta DAO
		return this.contrato.getIdContrato();
	}

	@Override
	public void delete(int idFuncionario) {
		//M�todo n�o utilizado nesta DAO
	}
	
	public Contrato selectContrato(int id) {
		String sql = "select * from Contrato where idContrato = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Contrato contrato = new Contrato();
			
			while(rs.next()) {
				contrato.setIdContrato(rs.getInt("idContrato"));
				contrato.setOrcamento(new OrcamentoDAO().selectOrcamento(rs.getInt("Orcamento_idOrcamento")));
				contrato.setVenda(new VendaDAO().selectVenda(rs.getInt("Venda_idVenda")));
				
				this.setContrato(contrato);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.contrato;
	}
	
	public void insertContratoOrcamentoCarro(int idFuncionario, int idOrcamento, String placa, LocalDate dataRetorno, double valorDoContrato) {
		String sql = "insert into Contrato (Orcamento_idOrcamento, Venda_idVenda) values (?,?);";
		String[] id = {"idContrato"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, idOrcamento);
			stmt.setInt(2, 1);
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				ProceduresDAO.registraLog(idFuncionario, "Contrato", "INSERT", rs.getInt(1));
				
				new ProdutoDAO().updateProdutoCarro(idFuncionario, dataRetorno, placa);
				new CarroDAO().updateTotalGerado(idFuncionario, valorDoContrato, placa);
				
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public void insertContratoOrcamentoJetski(int idFuncionario, int idOrcamento, int documento, LocalDate dataRetorno, double valorDoContrato) {
		String sql = "insert into Contrato (Orcamento_idOrcamento, Venda_idVenda) values (?,?);";
		String[] id = {"idContrato"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, idOrcamento);
			stmt.setInt(2, 1);
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				ProceduresDAO.registraLog(idFuncionario, "Contrato", "INSERT", rs.getInt(1));
				
				new ProdutoDAO().updateProdutoJetski(idFuncionario, dataRetorno, documento);
				new JetskiDAO().updateTotalGerado(idFuncionario, valorDoContrato, documento);
				
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public int insertContratoVenda(int idFuncionario, int idVenda) {
		String sql = "inset into Contrato (Orcamento_idOrcamento, Venda_idVenda) values (?,?);";
		String[] id = {"idContrato"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, 1);
			stmt.setInt(2, idVenda);
			
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.contrato.setIdContrato(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Contrato", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.contrato.getIdContrato();
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
}
