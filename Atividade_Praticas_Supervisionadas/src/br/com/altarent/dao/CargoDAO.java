package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Cargo;

public class CargoDAO extends DAO {
	private Cargo cargo;

	public CargoDAO() {
	}

	public CargoDAO(Cargo cargo) {
		this.cargo = cargo;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Cargo;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Cargo cargo = new Cargo();
				cargo.setIdCargo(rs.getInt("idCargo"));
				cargo.setCargo(rs.getString("Cargo"));
				cargo.setSalario(rs.getDouble("Salario"));

				lista.add(cargo);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Cargo (Cargo, Salario) values (?, ?);";
		String[] id = { "idCargo" };
		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, cargo.getCargo());
			stmt.setDouble(2, cargo.getSalario());

			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();
			
			
			
			while (rs.next()) {
				this.cargo.setIdCargo(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Cargo", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.cargo.getIdCargo();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Cargo set Cargo = ?, Salario = ? where idCargo = ?;";
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, cargo.getCargo());
			stmt.setDouble(2, cargo.getSalario());
			stmt.setInt(3, cargo.getIdCargo());

			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Cargo", "UPDATE", cargo.getIdCargo());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.cargo.getIdCargo();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Cargo where idCargo = ?;";
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, cargo.getIdCargo());
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Cargo", "DELETE", cargo.getIdCargo());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Cargo selectCargo(int id) {
		String sql = "select * from Cargo where idCargo = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();
			
			Cargo cargoDao = new Cargo();
			
			while (rs.next()) {
				
				cargoDao.setIdCargo(rs.getInt("idCargo"));
				cargoDao.setCargo(rs.getString("Cargo"));
				cargoDao.setSalario(rs.getDouble("Salario"));
				
				this.setCargo(cargoDao);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.cargo;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}
