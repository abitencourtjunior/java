package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Disponivel;

public class DisponivelDAO extends DAO {
	private Disponivel disponivel;

	public DisponivelDAO() {
	}

	public DisponivelDAO(Disponivel disponivel) {
		this.disponivel = disponivel;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Disponivel;";
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Disponivel disponivel = new Disponivel(rs.getString("Disponivel"));
				disponivel.setId(rs.getInt("idDisponivel"));

				lista.add(disponivel);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Disponivel (disponivel) value (?);";
		String[] id = { "idDisponivel" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, disponivel.getDisponivel());
			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.disponivel.setId(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Disponivel", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.disponivel.getId();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Disponivel set disponivel = ? where idDisponivel = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, disponivel.getDisponivel());
			stmt.setInt(2, disponivel.getId());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Disponivel", "UPDATE", disponivel.getId());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.disponivel.getId();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Disponivel where id = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, disponivel.getId());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Disponivel", "DELETE", disponivel.getId());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Disponivel selectDisponivel(int id) {
		String sql = "select * from Disponivel where idDisponivel = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				
				Disponivel disponivel = new Disponivel(rs.getString("Disponivel"));
				disponivel.setId(rs.getInt("idDisponivel"));
				this.setDisponivel(disponivel);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.disponivel;
	}

	public Disponivel getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(Disponivel disponivel) {
		this.disponivel = disponivel;
	}
}
