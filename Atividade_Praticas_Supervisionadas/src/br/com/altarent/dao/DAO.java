package br.com.altarent.dao;

import java.sql.Connection;
import java.util.List;

import br.com.altarent.conexao.ConnectionFactory;

public abstract class DAO {

    public Connection conexao = ConnectionFactory.conecta();
	
	public abstract List<Object> select();
	
	public abstract int insert(int idFuncionario);
	
	public abstract int update(int idFuncionario);
	
	public abstract void delete(int idFuncionario);
	
}
