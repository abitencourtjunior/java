package br.com.altarent.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Autenticacao;
import br.com.altarent.modelo.Cargo;
import br.com.altarent.modelo.Contato;
import br.com.altarent.modelo.Endereco;
import br.com.altarent.modelo.Funcionario;

public class FuncionarioDAO extends DAO {
	private Funcionario funcionario;
	
	public FuncionarioDAO() {}
	
	public FuncionarioDAO(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
	@Override
	public List<Object> select() {
		
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Funcionario;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				Funcionario funcionario = new Funcionario();

				funcionario.setIdFuncionario(rs.getInt("idFuncionario"));
				funcionario.setNome(rs.getString("Nome"));
				funcionario.setCpf(rs.getString("CPF"));
				funcionario.setDataNascimento(rs.getDate("DataNascimento"));
				funcionario.setSexo(rs.getString("Sexo"));
				funcionario.setEndereco(new EnderecoDAO().selectEndereco(rs.getInt("Endereco_idEndereco")));
				funcionario.setContato(new ContatoDAO().selectContato(rs.getInt("Contato_idContato")));
				funcionario.setCargo(new CargoDAO().selectCargo(rs.getInt("Cargo_idCargo")));
				funcionario.setAutenticacao(new AutenticacaoDAO().selectAutentica(rs.getInt("Autenticacao_idAutenticacao")));

				lista.add(funcionario);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException();
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Funcionario (Nome, CPF, DataNascimento, Sexo, Endereco_idEndereco, Contato_idContato, Cargo_idCargo, "
				+ "Autenticacao_idAutenticacao) values (?,?,?,?,?,?,?,?);";
		String[] id = { "idFuncionario" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			
			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getCpf());
			stmt.setDate(3, Date.valueOf(funcionario.getDataNascimento()));
			stmt.setString(4, funcionario.getSexo());
			stmt.setInt(5, new EnderecoDAO(funcionario.getEndereco()).insert(idFuncionario));
			stmt.setInt(6, new ContatoDAO(funcionario.getContato()).insert(idFuncionario));
			stmt.setInt(7, new CargoDAO().selectCargo(funcionario.getCargo().getIdCargo()).getIdCargo());  
			stmt.setInt(8, new AutenticacaoDAO(funcionario.getAutenticacao()).insert(idFuncionario));
			
			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.funcionario.setIdFuncionario(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Funcionario", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.funcionario.getIdFuncionario();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Funcionario set Nome = ?, CPF = ?, DataNascimento = ?, Sexo = ?, Endereco_idEndereco = ?, Contato_idContato = ?, "
				+ "Cargo_idCargo = ?, Autenticacao_idAutenticacao = ? where idFuncionario = ?";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getCpf());
			stmt.setDate(3, Date.valueOf(funcionario.getDataNascimento()));
			stmt.setString(4, funcionario.getSexo());
			stmt.setInt(5, new EnderecoDAO(funcionario.getEndereco()).insert(idFuncionario));
			stmt.setInt(6, new ContatoDAO(funcionario.getContato()).insert(idFuncionario));
			stmt.setInt(7, new CargoDAO().selectCargo(funcionario.getCargo().getIdCargo()).getIdCargo());  
			stmt.setInt(8, new AutenticacaoDAO(funcionario.getAutenticacao()).insert(idFuncionario));
			stmt.setInt(9, funcionario.getIdFuncionario());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Funcionario", "UPDATE", funcionario.getIdFuncionario());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException();
		}
		
		return this.funcionario.getIdFuncionario();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Funcionario where idFuncionario = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, funcionario.getIdFuncionario());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Funcionario", "DELETE", funcionario.getIdFuncionario());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

	}
	
	public Funcionario selectFuncionario(int id) {
		String sql = "select * from Funcionario where idFuncionario = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Funcionario funcionario = new Funcionario();
			
			while(rs.next()) {
				
				funcionario.setIdFuncionario(rs.getInt("idFuncionario"));
				funcionario.setNome(rs.getString("Nome"));
				funcionario.setCpf(rs.getString("CPF"));
				funcionario.setDataNascimento(rs.getDate("DataNascimento"));
				funcionario.setSexo(rs.getString("Sexo"));
				funcionario.setEndereco(new EnderecoDAO().selectEndereco(rs.getInt("Endereco_idEndereco")));
				funcionario.setContato(new ContatoDAO().selectContato(rs.getInt("Contato_idContato")));
				funcionario.setCargo(new CargoDAO().selectCargo(rs.getInt("Cargo_idCargo")));
				funcionario.setAutenticacao(new AutenticacaoDAO().selectAutentica(rs.getInt("Autenticacao_idAutenticacao")));
				
				this.setFuncionario(funcionario);
				
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.funcionario;
	}
	
	public Funcionario getFuncionarioAutenticado(String senha) {
		String sql = "select * from funcionario\r\n" + 
				"join endereco on endereco.idEndereco = funcionario.Endereco_idEndereco\r\n" + 
				"join contato on contato.idContato = funcionario.Contato_idContato \r\n" + 
				"join cargo on cargo.idCargo = funcionario.Cargo_idCargo \r\n" + 
				"join autenticacao on autenticacao.idAutenticacao = funcionario.Autenticacao_idAutenticacao\r\n" + 
				"where senha = ?";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, senha);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				
				Funcionario funcionario = new Funcionario();
				funcionario.setAutenticacao(new Autenticacao());
				funcionario.setCargo(new Cargo());
				funcionario.setContato(new Contato());
				funcionario.setEndereco(new Endereco());
				
				funcionario.setIdFuncionario(rs.getInt("idFuncionario"));
				funcionario.setNome(rs.getString("Nome"));
				funcionario.setCpf(rs.getString("CPF"));
				funcionario.setDataNascimento(rs.getDate("DataNascimento"));
				funcionario.getEndereco().setIdEndereco(rs.getInt("Endereco_idEndereco"));
				funcionario.getContato().setIdContato(rs.getInt("Contato_idContato"));
				funcionario.getCargo().setIdCargo(rs.getInt("Cargo_idCargo"));
				funcionario.getAutenticacao().setIdAutenticacao(rs.getInt("Autenticacao_idAutenticacao"));
				funcionario.getEndereco().setNomeRua(rs.getString("NomeRua"));
				funcionario.getEndereco().setCEP(rs.getString("CEP"));
				funcionario.getEndereco().setBairro(rs.getString("Bairro"));
				funcionario.getEndereco().setCidade(rs.getString("Cidade"));
				funcionario.getEndereco().setComplemento(rs.getString("Complemento"));
				funcionario.getEndereco().setNumero(rs.getInt("Numero"));
				funcionario.getContato().setTelefonePrincipal(rs.getString("TelefonePrincipal"));
				funcionario.getContato().setTelefoneSecundario(rs.getString("TelefoneSecundario"));
				funcionario.getContato().setEmail(rs.getString("E-mail"));
				funcionario.getCargo().setCargo(rs.getString("Cargo"));
				funcionario.getCargo().setSalario(rs.getDouble("Salario"));
				funcionario.getAutenticacao().setUsuario(rs.getString("Usuario"));
				funcionario.getAutenticacao().setSenha(rs.getString("Senha"));
				funcionario.getAutenticacao().setDataCriacao(rs.getDate("DataCriacao"));
				funcionario.getAutenticacao().setTipoUsuario(rs.getString("TipoUsuario"));
				
				this.setFuncionario(funcionario);
				
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.funcionario;
	}
	
	public List<Funcionario> selectFuncionario() {
		List<Funcionario> lista = new ArrayList<>();
		
		String sql = "select * from funcionario\r\n" + 
				"join endereco on endereco.idEndereco = funcionario.Endereco_idEndereco\r\n" + 
				"join contato on contato.idContato = funcionario.Contato_idContato\r\n" + 
				"join cargo on cargo.idCargo = funcionario.Cargo_idCargo\r\n" + 
				"join autenticacao on autenticacao.idAutenticacao = funcionario.Autenticacao_idAutenticacao\r\n" + 
				"group by idFuncionario;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Funcionario funcionario = new Funcionario();
				funcionario.setAutenticacao(new Autenticacao());
				funcionario.setCargo(new Cargo());
				funcionario.setContato(new Contato());
				funcionario.setEndereco(new Endereco());
				
				funcionario.setIdFuncionario(rs.getInt("idFuncionario"));
				funcionario.setNome(rs.getString("Nome"));
				funcionario.setCpf(rs.getString("CPF"));
				funcionario.setDataNascimento(rs.getDate("DataNascimento"));
				funcionario.getEndereco().setIdEndereco(rs.getInt("Endereco_idEndereco"));
				funcionario.getContato().setIdContato(rs.getInt("Contato_idContato"));
				funcionario.getCargo().setIdCargo(rs.getInt("Cargo_idCargo"));
				funcionario.getAutenticacao().setIdAutenticacao(rs.getInt("Autenticacao_idAutenticacao"));
				funcionario.getEndereco().setNomeRua(rs.getString("NomeRua"));
				funcionario.getEndereco().setCEP(rs.getString("CEP"));
				funcionario.getEndereco().setBairro(rs.getString("Bairro"));
				funcionario.getEndereco().setCidade(rs.getString("Cidade"));
				funcionario.getEndereco().setComplemento(rs.getString("Complemento"));
				funcionario.getEndereco().setNumero(rs.getInt("Numero"));
				funcionario.getContato().setTelefonePrincipal(rs.getString("TelefonePrincipal"));
				funcionario.getContato().setTelefoneSecundario(rs.getString("TelefoneSecundario"));
				funcionario.getContato().setEmail(rs.getString("E-mail"));
				funcionario.getCargo().setCargo(rs.getString("Cargo"));
				funcionario.getCargo().setSalario(rs.getDouble("Salario"));
				funcionario.getAutenticacao().setUsuario(rs.getString("Usuario"));
				funcionario.getAutenticacao().setSenha(rs.getString("Senha"));
				funcionario.getAutenticacao().setDataCriacao(rs.getDate("DataCriacao"));
				funcionario.getAutenticacao().setTipoUsuario(rs.getString("TipoUsuario"));
				
				lista.add(funcionario);
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
}
