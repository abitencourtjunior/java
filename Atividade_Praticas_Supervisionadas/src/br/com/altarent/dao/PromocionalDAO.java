package br.com.altarent.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Promocional;

public class PromocionalDAO extends DAO {
	private Promocional promocional;

	public PromocionalDAO() {
	}

	public PromocionalDAO(Promocional promocional) {
		this.promocional = promocional;
	}

	@Override
	public List<Object> select() {
		String sql = "select * from Promocional;";

		List<Object> lista = new ArrayList<>();

		try (Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Promocional promocional = new Promocional();

				promocional.setIdPromocional(rs.getInt("idPromocional"));
				promocional.setHash(rs.getString("Hash"));
				promocional.setUsado(rs.getBoolean("Usado"));
				promocional.setValidade(rs.getDate("Validade"));

				lista.add(promocional);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Promocional (Hash, Usado, Validade) values (?, ?, ?);";
		String[] id = { "idPromocional" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, promocional.getHash());
			stmt.setBoolean(2, promocional.isUsado());
			stmt.setDate(3, Date.valueOf(promocional.getValidade()));
			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.promocional.setIdPromocional(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Promocional", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.promocional.getIdPromocional();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Promocional set Hash = ?, Usado = ?, Validade = ? where idPromocional = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, promocional.getHash());
			stmt.setBoolean(2, promocional.isUsado());
			stmt.setDate(3, Date.valueOf(promocional.getValidade()));
			stmt.setInt(4, promocional.getIdPromocional());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Seguro", "UPDATE", promocional.getIdPromocional());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.promocional.getIdPromocional();

	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Promocional where idPromocional = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, promocional.getIdPromocional());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Seguro", "DELETE", promocional.getIdPromocional());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Promocional selectPromocional(int id) {
		String sql = "select * from Promocional where idPromocional = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();
			
			Promocional promocional = new Promocional();
			
			while (rs.next()) {

				promocional.setIdPromocional(rs.getInt("idPromocional"));
				promocional.setHash(rs.getString("Hash"));
				promocional.setUsado(rs.getBoolean("Usado"));
				promocional.setValidade(rs.getDate("Validade"));

				this.setPromocional(promocional);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.promocional;
	}
	

	public Promocional getPromocional() {
		return promocional;
	}

	public void setPromocional(Promocional promocional) {
		this.promocional = promocional;
	}
}
