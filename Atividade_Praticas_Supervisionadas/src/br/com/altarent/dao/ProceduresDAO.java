package br.com.altarent.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import br.com.altarent.conexao.ConnectionFactory;

public class ProceduresDAO {
	
	public static void registraLog(int funcionario, String tabela, String comando, int id) {
		String sql = "call guardaLog(?,?,?,?);";
		Connection conexao = ConnectionFactory.conecta();
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, funcionario);
			stmt.setString(2, tabela);
			stmt.setString(3, comando);
			stmt.setInt(4, id);
			
			stmt.execute();
			stmt.closeOnCompletion();
			conexao.close();
		
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
}
