package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.EstadoDeUso;
import br.com.altarent.modelo.Jetski;
import br.com.altarent.modelo.Manutencao;
import br.com.altarent.modelo.Marca;
import br.com.altarent.modelo.ModeloAquatico;
import br.com.altarent.modelo.PessoaJuridica;
import br.com.altarent.modelo.Seguro;

public class JetskiDAO extends DAO {
	private Jetski jetski;
	
	public JetskiDAO() {}
	
	public JetskiDAO(Jetski jetski) {
		this.jetski = jetski;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Jetski;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				
				Jetski jetski = new Jetski();
				
				jetski.setModeloAquatico(new ModeloAquaticoDAO().selectModeloAquatico(rs.getInt("ModeloAquatico_idModeloAquatico")));
				jetski.setDocumentoMarinha(rs.getInt("DocumentoMarinha"));
				jetski.setCor(rs.getString("Cor"));
				jetski.setSeguro(new SeguroDAO().selectSeguro(rs.getInt("Seguro_idSeguro")));
				jetski.setManutencao(new ManutencaoDAO().selectManutencao(rs.getInt("Manutencao_idManutencao")));
				jetski.setValorAluguel(rs.getDouble("ValorAluguel"));
				jetski.setDataFabricacao(rs.getInt("DataFabricacao"));
				jetski.setValorDeCompra(rs.getDouble("ValorDeCompra"));
				jetski.setValorAtual(rs.getDouble("ValorAtual"));
				jetski.setTotalGerado(rs.getDouble("TotalGerado"));
				jetski.setEstadoDeUso(new EstadoDeUsoDAO().selectEstadoDeUso(rs.getInt("EstadoDeUso_idEstadoDeUso")));
				
				lista.add(jetski);
				
			}
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}	
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Jetski (ModeloAquatico_idModeloAquatico, DocumentoMarinha, Cor, Seguro_idSeguro, Manutencao_idManutencao, "
				+ "ValorAluguel, DataFabricacao, ValorDeCompra, ValorAtual, TotalGerado, EstadoDeUso_idEstadoDeUso) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?);";
		String[] id = {"idJetski"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, new ModeloAquaticoDAO(jetski.getModeloAquatico()).insert(idFuncionario));
			stmt.setInt(2, jetski.getDocumentoMarinha());
			stmt.setString(3, jetski.getCor());
			stmt.setInt(4, new SeguroDAO(jetski.getSeguro()).insert(idFuncionario));
			stmt.setInt(5, new ManutencaoDAO().selectManutencao(jetski.getManutencao().getIdManutencao()).getIdManutencao());
			stmt.setDouble(6, jetski.getValorAluguel());
			stmt.setInt(7, jetski.getDataFabricacao());
			stmt.setDouble(8, jetski.getValorDeCompra());
			stmt.setDouble(9, jetski.getValorAtual());
			stmt.setDouble(10, jetski.getTotalGerado());
			stmt.setInt(11, new EstadoDeUsoDAO().selectEstadoDeUso(jetski.getEstadoDeUso().getIdEstadoDeUso()).getIdEstadoDeUso());
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.jetski.setIdJetski(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Jetski", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.jetski.getIdJetski();
	}	

	@Override
	public int update(int idFuncionario) {
		String sql = "update Jetski set Seguro_idSeguro = ?, Manutencao_idManutencao = ?, ValorAluguel = ?, ValorAtual = ?, "
				+ "TotalGerado = ?, EstadoDeUso_idEstadoDeUso = ? where idJetski = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new SeguroDAO().selectSeguro(jetski.getSeguro().getIdSeguro()).getIdSeguro());
			stmt.setInt(2, new ManutencaoDAO(jetski.getManutencao()).insert(idFuncionario));
			stmt.setDouble(3, jetski.getValorAluguel());
			stmt.setDouble(4, jetski.getValorAtual());
			stmt.setDouble(5, jetski.getTotalGerado());
			stmt.setInt(6, new EstadoDeUsoDAO().selectEstadoDeUso(jetski.getEstadoDeUso().getIdEstadoDeUso()).getIdEstadoDeUso());
			stmt.setInt(7, jetski.getIdJetski());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Jetski", "UPDATE", jetski.getIdJetski());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.jetski.getIdJetski();
	}
	
	public void updateManutencao(int idFuncionario, Integer documento) {
		String sql = "update Jetski set Manutencao_idManutencao = ?, TotalGerado = ? where DocumentoMarinha = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			int idManutencao = new ManutencaoDAO(jetski.getManutencao()).insert(idFuncionario);
			stmt.setInt(1, idManutencao);
			stmt.setDouble(2, ((new JetskiDAO().selectJetskiPeloDocumento(documento).getTotalGerado()) - 
					(new ManutencaoDAO().selectManutencao(idManutencao).getValorManutencao())));
			stmt.setInt(3, documento);
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Jetski", "UPDATE", new JetskiDAO().selectJetskiPeloDocumento(documento).getIdJetski());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		
	}
	
	public void updateSeguro(int idFuncionario, Integer documento) {
		String sql = "update Jetski set Seguro_idSeguro = ?, TotalGerado = ? where DocumentoMarinha = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			int idSeguro = new SeguroDAO(jetski.getSeguro()).insert(idFuncionario);
			stmt.setInt(1, idSeguro);
			stmt.setDouble(2, ((new JetskiDAO().selectJetskiPeloDocumento(documento).getTotalGerado()) - 
					(new SeguroDAO().selectSeguro(idSeguro).getValor())));
			stmt.setInt(3, documento);
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Jetski", "UPDATE", new JetskiDAO().selectJetskiPeloDocumento(documento).getIdJetski());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public void updateTotalGerado(int idFuncionario, double novoValor, Integer documento) {
		String sql = "update Jetski set TotalGerado = ? where DocumentoMarinha = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setDouble(1, novoValor);
			stmt.setInt(2, documento);
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Carro", "UPDATE", new JetskiDAO().selectJetskiPeloDocumento(documento).getIdJetski());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Jetski where idJetski = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, jetski.getIdJetski());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Jetski", "DELETE", jetski.getIdJetski());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}

	}
	
	public Jetski selectJetski(int id) {
		String sql = "select * from Jetski where idJetski = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Jetski jetski = new Jetski();
			
			while(rs.next()) {
				
				jetski.setIdJetski(rs.getInt("idJetski"));
				jetski.setModeloAquatico(new ModeloAquaticoDAO().selectModeloAquatico(rs.getInt("ModeloAquatico_idModeloAquatico")));
				jetski.setDocumentoMarinha(rs.getInt("DocumentoMarinha"));
				jetski.setCor(rs.getString("Cor"));
				jetski.setSeguro(new SeguroDAO().selectSeguro(rs.getInt("Seguro_idSeguro")));
				jetski.setManutencao(new ManutencaoDAO().selectManutencao(rs.getInt("Manutencao_idManutencao")));
				jetski.setValorAluguel(rs.getDouble("ValorAluguel"));
				jetski.setDataFabricacao(rs.getInt("DataFabricacao"));
				jetski.setValorDeCompra(rs.getDouble("ValorDeCompra"));
				jetski.setValorAtual(rs.getDouble("ValorAtual"));
				jetski.setTotalGerado(rs.getDouble("TotalGerado"));
				jetski.setEstadoDeUso(new EstadoDeUsoDAO().selectEstadoDeUso(rs.getInt("EstadoDeUso_idEstadoDeUso")));
				
				this.setJetski(jetski);
				
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return jetski;
	}
	
	public Jetski selectJetskiPeloDocumento(int documento) {
		String sql = "select * from Jetski where DocumentoMarinha = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, documento);
			
			ResultSet rs = stmt.executeQuery();
			
			Jetski jetski = new Jetski();
			
			while(rs.next()) {
				
				jetski.setIdJetski(rs.getInt("idJetski"));
				jetski.setModeloAquatico(new ModeloAquaticoDAO().selectModeloAquatico(rs.getInt("ModeloAquatico_idModeloAquatico")));
				jetski.setDocumentoMarinha(rs.getInt("DocumentoMarinha"));
				jetski.setCor(rs.getString("Cor"));
				jetski.setSeguro(new SeguroDAO().selectSeguro(rs.getInt("Seguro_idSeguro")));
				jetski.setManutencao(new ManutencaoDAO().selectManutencao(rs.getInt("Manutencao_idManutencao")));
				jetski.setValorAluguel(rs.getDouble("ValorAluguel"));
				jetski.setDataFabricacao(rs.getInt("DataFabricacao"));
				jetski.setValorDeCompra(rs.getDouble("ValorDeCompra"));
				jetski.setValorAtual(rs.getDouble("ValorAtual"));
				jetski.setTotalGerado(rs.getDouble("TotalGerado"));
				jetski.setEstadoDeUso(new EstadoDeUsoDAO().selectEstadoDeUso(rs.getInt("EstadoDeUso_idEstadoDeUso")));
				
				this.setJetski(jetski);
				
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return jetski;
	}
	
	public double selectValorAluguel(int documento) {
		String sql = "select ValorAluguel from Jetski where DocumentoMarinha = ?;";
		double valor = 0;
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, documento);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				valor = rs.getDouble("ValorAluguel");
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return valor;
	}
	
	public List<Jetski> selectJetski() {
		List<Jetski> lista = new ArrayList<>();
		String sql = "select * from jetski\r\n" + 
				"join modeloaquatico on modeloaquatico.idModeloAquatico = jetski.ModeloAquatico_idModeloAquatico\r\n" + 
				"join marca on marca.idMarca = modeloaquatico.Marca_idMarca\r\n" + 
				"join manutencao on manutencao.idManutencao = jetski.Manutencao_idManutencao\r\n" + 
				"join seguro on seguro.idSeguro = jetski.Seguro_idSeguro\r\n" + 
				"join estadodeuso on estadodeuso.idEstadoDeUso  = jetski.EstadoDeUso_idEstadoDeUso\r\n" + 
				"order by idJetski;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Jetski jet = new Jetski();
				jet.setEstadoDeUso(new EstadoDeUso());
				jet.setManutencao(new Manutencao());
				jet.setSeguro(new Seguro());
				jet.getSeguro().setPessoaJuridica(new PessoaJuridica());
				jet.setModeloAquatico(new ModeloAquatico());
				jet.getModeloAquatico().setMarca(new Marca());
				
				jet.setIdJetski(rs.getInt("idJetski"));
				jet.setDocumentoMarinha(rs.getInt("DocumentoMarinha"));
				jet.setDataFabricacao(rs.getInt("DataFabricacao"));
				jet.setCor(rs.getString("Cor"));
				jet.getSeguro().setIdSeguro(rs.getInt("Seguro_idSeguro"));
				jet.getManutencao().setIdManutencao(rs.getInt("Manutencao_idManutencao"));
				jet.setValorAluguel(rs.getDouble("ValorAluguel"));
				jet.setValorDeCompra(rs.getDouble("ValorDeCompra"));
				jet.setValorAtual(rs.getDouble("ValorAtual"));
				jet.setTotalGerado(rs.getDouble("TotalGerado"));
				jet.getModeloAquatico().setIdModeloAquatico(rs.getInt("ModeloAquatico_idModeloAquatico"));
				jet.getEstadoDeUso().setIdEstadoDeUso(rs.getInt("EstadoDeUso_idEstadoDeUso"));
				jet.getModeloAquatico().setNomeModelo(rs.getString("NomeModelo"));
				jet.getModeloAquatico().setPotenciaMotor(rs.getShort("PotenciaMotor"));
				jet.getModeloAquatico().setTipoMotor(rs.getByte("TipoMotor"));
				jet.getModeloAquatico().setLugares(rs.getByte("Lugares"));
				jet.getModeloAquatico().setTipoDeConducao(rs.getString("TipoDeConducao"));
				jet.getModeloAquatico().setTipoDeIgnicao(rs.getString("TipoDeIgnicao"));
				jet.getModeloAquatico().setTipoDeAgua(rs.getString("TipoDeAgua"));
				jet.getModeloAquatico().setVolumeTanque(rs.getShort("VolumeTanque"));
				jet.getModeloAquatico().getMarca().setIdMarca(rs.getInt("Marca_idMarca"));
				jet.getModeloAquatico().getMarca().setNome(rs.getString("Nome"));
				jet.getManutencao().setTipoManutencao(rs.getString("TipoManutencao"));
				jet.getManutencao().setDataManutencao(rs.getDate("DataManutencao"));
				jet.getManutencao().setKilometragem(rs.getShort(28));
				jet.getManutencao().setValorManutencao(rs.getFloat("ValorManutencao"));
				jet.getManutencao().setDescricao(rs.getString("Descricao"));
				jet.getSeguro().setValor(rs.getFloat("Valor"));
				jet.getSeguro().getPessoaJuridica().setIdPessoaJuridica(rs.getInt("PessoaJuridica_idPessoaJuridica"));
				jet.getEstadoDeUso().setEstadoDeUso(rs.getString("EstadoDeUso"));
				
				lista.add(jet);
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex ) {
			throw new RuntimeException(ex);
		}
		
		
		return lista;
	}

	public Jetski getJetski() {
		return jetski;
	}

	public void setJetski(Jetski jetski) {
		this.jetski = jetski;
	}
}
