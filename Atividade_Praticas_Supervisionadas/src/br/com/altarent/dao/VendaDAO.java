package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Venda;

public class VendaDAO extends DAO {
	private Venda venda;
	
	public VendaDAO() {}
	
	public VendaDAO(Venda venda) {
		this.venda = venda;
	}
	
	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		
		String sql = "select * from Venda;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				
				Venda venda = new Venda();
				
				venda.setIdVenda(rs.getInt("idVenda"));
				venda.setProduto(new ProdutoDAO().selectProduto(rs.getInt("Produto_idProduto")));
				venda.setLoja(new LojaDAO().selectLoja(rs.getInt("Loja_idLoja")));
				venda.setFuncionario(new FuncionarioDAO().selectFuncionario(rs.getInt("Funcionario_idFuncionario")));
				venda.setCliente(new ClienteDAO().selectCliente(rs.getInt("Cliente_idCliente")));
				
				lista.add(venda);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
		
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Venda (Produto_idProduto, Funcionario_idFuncionario, Cliente_idCliente, Loja_idLoja) values (?,?,?,?);";
		String[] id = {"idVenda"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, new ProdutoDAO().selectProduto(venda.getProduto().getIdProduto()).getIdProduto());
			stmt.setInt(2, new FuncionarioDAO().selectFuncionario(venda.getFuncionario().getIdFuncionario()).getIdFuncionario());
			stmt.setInt(3, new ClienteDAO().selectCliente(venda.getCliente().getIdCliente()).getIdCliente());
			stmt.setInt(4, new LojaDAO().selectLoja(venda.getLoja().getIdLoja()).getIdLoja());
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.venda.setIdVenda(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Venda", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.venda.getIdVenda();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Venda set Produto_idProduto = ? where idVenda = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new ProdutoDAO().selectProduto(venda.getProduto().getIdProduto()).getIdProduto());
			stmt.setInt(2, venda.getIdVenda());
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Venda", "UPDATE", venda.getIdVenda());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.venda.getIdVenda();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Venda where idVenda = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, venda.getIdVenda());
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Venda", "DELETE", venda.getIdVenda());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);		
		}

	}
	
	public Venda selectVenda(int id) {
		String sql = "select * from Venda where idVenda = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, venda.getIdVenda());
			
			ResultSet rs = stmt.executeQuery();
			
			Venda venda = new Venda();
			
			while(rs.next()) {
				
				venda.setIdVenda(rs.getInt("idVenda"));
				venda.setProduto(new ProdutoDAO().selectProduto(rs.getInt("Produto_idProduto")));
				venda.setFuncionario(new FuncionarioDAO().selectFuncionario(rs.getInt("Funcionario_idFuncionario")));
				venda.setCliente(new ClienteDAO().selectCliente(rs.getInt("Cliente_idCliente")));
				venda.setLoja(new LojaDAO().selectLoja(rs.getInt("Loja_idLoja")));
				
				this.setVenda(venda);
				
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.venda;
	}

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}
}
