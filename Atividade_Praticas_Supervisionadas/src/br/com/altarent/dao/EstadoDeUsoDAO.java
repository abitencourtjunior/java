package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.EstadoDeUso;

public class EstadoDeUsoDAO extends DAO {
	private EstadoDeUso estado;

	public EstadoDeUsoDAO() {
	}

	public EstadoDeUsoDAO(EstadoDeUso estado) {
		this.estado = estado;
	}

	@Override
	public List<Object> select() {

		List<Object> lista = new ArrayList<>();
		String sql = "select * from EstadoDeUso;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				EstadoDeUso estado = new EstadoDeUso(rs.getString("EstadoDeUso"));
				estado.setIdEstadoDeUso(rs.getInt("idEstadoDeUso"));

				lista.add(estado);

			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into EstadoDeUso (EstadoDeUso) value (?);";
		String[] id = { "idEstadoDeUso" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, estado.getEstadoDeUso());
			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.estado.setIdEstadoDeUso(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "EstadoDeUso", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.estado.getIdEstadoDeUso();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update EstadoDeUso set EstadoDeUso = ? where idEstadoDeUso = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, estado.getEstadoDeUso());
			stmt.setInt(2, estado.getIdEstadoDeUso());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "EstadoDeUso", "UPDATE", estado.getIdEstadoDeUso());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.estado.getIdEstadoDeUso();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from EstadoDeUso where idEstadoDeUso = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, estado.getIdEstadoDeUso());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "EstadoDeUso", "DELETE", estado.getIdEstadoDeUso());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public EstadoDeUso selectEstadoDeUso(int id) {
		String sql = "select * from EstadoDeUso where idEstadoDeUso = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				EstadoDeUso estado = new EstadoDeUso(rs.getString("EstadoDeUso"));
				estado.setIdEstadoDeUso(rs.getInt("idEstadoDeUso"));
				this.setEstado(estado);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.estado;
	}

	public EstadoDeUso getEstado() {
		return estado;
	}

	public void setEstado(EstadoDeUso estado) {
		this.estado = estado;
	}
}
