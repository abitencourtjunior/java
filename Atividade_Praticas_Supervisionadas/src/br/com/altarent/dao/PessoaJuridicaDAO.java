package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import br.com.altarent.modelo.Contato;
import br.com.altarent.modelo.Endereco;
import br.com.altarent.modelo.PessoaFisica;
import br.com.altarent.modelo.PessoaJuridica;

public class PessoaJuridicaDAO extends DAO {
	private PessoaJuridica pessoaJuridica;
	
	public PessoaJuridicaDAO() {}
	
	public PessoaJuridicaDAO(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}
	
	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from PessoaJuridica;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				PessoaJuridica pessoaJuridica = new PessoaJuridica();
				
				pessoaJuridica.setIdPessoaJuridica(rs.getInt("idPessoaJuridica"));
				pessoaJuridica.setNome(rs.getString("Nome"));
				pessoaJuridica.setRazaoSocial(rs.getString("NomeFantasia"));
				pessoaJuridica.setCnpj(rs.getString("CNPJ"));
				pessoaJuridica.setResponsavel(new PessoaFisicaDAO().selectPessoaFisica(rs.getShort("PessoaFisica_idPessoaFisica")));
				pessoaJuridica.setContato(new ContatoDAO().selectContato(rs.getInt("Contato_idContato")));
				pessoaJuridica.setEndereco(new EnderecoDAO().selectEndereco(rs.getInt("Endereco_idEndereco")));
				
				lista.add(pessoaJuridica);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into PessoaJuridica (Nome, NomeFantasia, CNPJ, PessoaFisica_idPessoaFisica, "
				+ "Endereco_idEndereco, Contato_idContato, Seguradora) values (?,?,?,?,?,?,?);";
		String[] id = { "idPessoaJuridica" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, pessoaJuridica.getNome());
			stmt.setString(2, pessoaJuridica.getRazaoSocial());
			stmt.setString(3, pessoaJuridica.getCnpj());
			stmt.setInt(4, new PessoaFisicaDAO(pessoaJuridica.getResponsavel()).insert(idFuncionario));
			stmt.setInt(5, new EnderecoDAO(pessoaJuridica.getEndereco()).insert(idFuncionario));
			stmt.setInt(6, new ContatoDAO(pessoaJuridica.getContato()).insert(idFuncionario));
			stmt.setBoolean(7, pessoaJuridica.isSeguradora());
			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.pessoaJuridica.setIdPessoaJuridica(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "PessoaJuridica", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.pessoaJuridica.getIdPessoaJuridica();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update PessoaJuridica set Nome = ?, NomeFantasia = ?, CNPJ = ?, PessoaFisica_idPessoaFisica = ?, "
				+ "Endereco_idEndereco = ?, Contato_idContato = ?, Seguradora = ? where idPessoaJuridica = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, pessoaJuridica.getNome());
			stmt.setString(2, pessoaJuridica.getRazaoSocial());
			stmt.setString(3, pessoaJuridica.getCnpj());
			stmt.setInt(4, new PessoaFisicaDAO(pessoaJuridica.getResponsavel()).update(idFuncionario));
			stmt.setInt(5, new EnderecoDAO(pessoaJuridica.getEndereco()).update(idFuncionario));
			stmt.setInt(6, new ContatoDAO(pessoaJuridica.getContato()).update(idFuncionario));
			stmt.setBoolean(7, pessoaJuridica.isSeguradora());
			stmt.setInt(8, pessoaJuridica.getIdPessoaJuridica());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "PessoaJuridica", "UPDATE", pessoaJuridica.getIdPessoaJuridica());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.pessoaJuridica.getIdPessoaJuridica();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from PessoaJuridica where idPessoaJuridica = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, pessoaJuridica.getIdPessoaJuridica());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "PessoaJuridica", "DELETE", pessoaJuridica.getIdPessoaJuridica());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public PessoaJuridica selectPessoaJuridica(int id) {
		String sql = "select * from PessoaJuridica where idPessoaJuridica = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			PessoaJuridica pessoa = new PessoaJuridica();
			
			while (rs.next()) {
				
				pessoa.setIdPessoaJuridica(rs.getInt("idPessoaJuridica"));
				pessoa.setNome(rs.getString("Nome"));
				pessoa.setRazaoSocial(rs.getString("NomeFantasia"));
				pessoa.setCnpj(rs.getString("CNPJ"));
				pessoa.setSeguradora(rs.getBoolean("Seguradora"));
				pessoa.setResponsavel(new PessoaFisicaDAO().selectPessoaFisica(rs.getShort("PessoaFisica_idPessoaFisica")));
				pessoa.setContato(new ContatoDAO().selectContato(rs.getInt("Contato_idContato")));
				pessoa.setEndereco(new EnderecoDAO().selectEndereco(rs.getInt("Endereco_idEndereco")));

				this.setPessoaJuridica(pessoa);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return this.pessoaJuridica;
	}
	
	public HashSet<PessoaJuridica> selectSeguradoras() {
		HashSet<PessoaJuridica> lista = new HashSet<>();
		List<PessoaFisica> listaPf = new PessoaFisicaDAO().selectPessoaFisica();
		
		String sql = "select * from pessoajuridica\r\n" + 
				"join pessoafisica on pessoafisica.idPessoaFisica = pessoajuridica.PessoaFisica_idPessoaFisica\r\n" + 
				"join endereco on endereco.idEndereco = pessoajuridica.Endereco_idEndereco\r\n" + 
				"join contato on contato.idContato = pessoajuridica.Contato_idContato\r\n" + 
				"where Seguradora is true\r\n" + 
				"order by idPessoaJuridica;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				PessoaJuridica pj = new PessoaJuridica();
				pj.setContato(new Contato());
				pj.setEndereco(new Endereco());
				pj.setResponsavel(new PessoaFisica());
				
				pj.setIdPessoaJuridica(rs.getInt("idPessoaJuridica"));
				pj.setNome(rs.getString("Nome"));
				pj.setRazaoSocial(rs.getString("NomeFantasia"));
				pj.setCnpj(rs.getString("CNPJ"));
				pj.setSeguradora(rs.getBoolean("Seguradora"));
				pj.getEndereco().setIdEndereco(rs.getInt(6));
				pj.getContato().setIdContato(rs.getInt(7));
				pj.getEndereco().setNomeRua(rs.getString("NomeRua"));
				pj.getEndereco().setCEP(rs.getString("CEP"));
				pj.getEndereco().setBairro(rs.getString("Bairro"));
				pj.getEndereco().setCidade(rs.getString("Cidade"));
				pj.getEndereco().setComplemento(rs.getString("Complemento"));
				pj.getEndereco().setNumero(rs.getInt("Numero"));
				pj.getContato().setTelefonePrincipal(rs.getString("TelefonePrincipal"));
				pj.getContato().setTelefoneSecundario(rs.getString("TelefoneSecundario"));
				pj.getContato().setEmail(rs.getString("E-mail"));
				pj.getResponsavel().setIdPessoaFisica(rs.getInt("PessoaFisica_idPessoaFisica"));
				
				listaPf.forEach(pessoaFisica -> {
					if(pessoaFisica.getIdPessoaFisica() == pj.getResponsavel().getIdPessoaFisica()) {
						pj.setResponsavel(pessoaFisica);
					}
				});
				
				lista.add(pj);	
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	
		return lista;
	}
	
	public List<PessoaJuridica> selectPessoaJuridica() {
		List<PessoaJuridica> lista = new ArrayList<>();
		List<PessoaFisica> listaPf = new PessoaFisicaDAO().selectPessoaFisica();
		
		String sql = "select * from pessoajuridica\r\n" + 
				"join pessoafisica on pessoafisica.idPessoaFisica = pessoajuridica.PessoaFisica_idPessoaFisica\r\n" + 
				"join endereco on endereco.idEndereco = pessoajuridica.Endereco_idEndereco\r\n" + 
				"join contato on contato.idContato = pessoajuridica.Contato_idContato\r\n" + 
				"order by idPessoaJuridica;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);	
			
			while(rs.next()) {
				
				PessoaJuridica pj = new PessoaJuridica();
				pj.setContato(new Contato());
				pj.setEndereco(new Endereco());
				pj.setResponsavel(new PessoaFisica());
				
				pj.setIdPessoaJuridica(rs.getInt("idPessoaJuridica"));
				pj.setNome(rs.getString("Nome"));
				pj.setRazaoSocial(rs.getString("NomeFantasia"));
				pj.setCnpj(rs.getString("CNPJ"));
				pj.setSeguradora(rs.getBoolean("Seguradora"));
				pj.getEndereco().setIdEndereco(rs.getInt(6));
				pj.getContato().setIdContato(rs.getInt(7));
				pj.getEndereco().setNomeRua(rs.getString("NomeRua"));
				pj.getEndereco().setCEP(rs.getString("CEP"));
				pj.getEndereco().setBairro(rs.getString("Bairro"));
				pj.getEndereco().setCidade(rs.getString("Cidade"));
				pj.getEndereco().setComplemento(rs.getString("Complemento"));
				pj.getEndereco().setNumero(rs.getInt("Numero"));
				pj.getContato().setTelefonePrincipal(rs.getString("TelefonePrincipal"));
				pj.getContato().setTelefoneSecundario(rs.getString("TelefoneSecundario"));
				pj.getContato().setEmail(rs.getString("E-mail"));
				pj.getResponsavel().setIdPessoaFisica(rs.getInt("PessoaFisica_idPessoaFisica"));
				
				listaPf.forEach(pessoaFisica -> {
					if(pessoaFisica.getIdPessoaFisica() == pj.getResponsavel().getIdPessoaFisica()) {
						pj.setResponsavel(pessoaFisica);
					}
				});
				
				lista.add(pj);	
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

}
