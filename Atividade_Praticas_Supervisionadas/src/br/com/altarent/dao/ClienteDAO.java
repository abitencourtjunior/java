package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Autenticacao;
import br.com.altarent.modelo.Cliente;
import br.com.altarent.modelo.PessoaFisica;
import br.com.altarent.modelo.PessoaJuridica;
import br.com.altarent.modelo.Promocional;

public class ClienteDAO extends DAO {
	private Cliente cliente;

	public ClienteDAO() {
	}

	public ClienteDAO(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Cliente;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				Cliente cliente = new Cliente();
				
				cliente.setIdCliente(rs.getInt("idCliente"));
				cliente.setPessoaFisica(new PessoaFisicaDAO().selectPessoaFisica(rs.getInt("PessoaFisica_idPessoaFisica")));
				cliente.setPessoaJuridica(new PessoaJuridicaDAO().selectPessoaJuridica(rs.getInt("PessoaJuridica_idPessoaJuridica")));
				cliente.setCodigo(new PromocionalDAO().selectPromocional(rs.getInt("Promocional_idPromocional")));
				cliente.setAutenticacao(new AutenticacaoDAO().selectAutentica(rs.getInt("Autenticacao_idAutenticacao")));
				
				lista.add(cliente);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Cliente (PessoaFisica_idPessoaFisica, PessoaJuridica_idPessoaJuridica, Promocional_idPromocional, "
				+ "Autenticacao_idAutenticacao) values (?,?,?,?);";
		String[] id = {"idCliente"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, new PessoaFisicaDAO(cliente.getPessoaFisica()).insert(idFuncionario));
			stmt.setInt(2, new PessoaJuridicaDAO(cliente.getPessoaJuridica()).insert(idFuncionario));
			stmt.setInt(3, new PromocionalDAO(cliente.getCodigo()).insert(idFuncionario));
			stmt.setInt(4, new AutenticacaoDAO(cliente.getAutenticacao()).insert(idFuncionario));
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			
			
			while(rs.next()) {
				this.cliente.setIdCliente(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Cliente", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.cliente.getIdCliente();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Cliente set PessoaFisica_idPessoa = ?, PessoaJuridica_idPessoaJuridica = ?, Promocional_idPromocional = ?, "
				+ "Autenticacao_idAutenticacao = ? where idCliente = ?";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new PessoaFisicaDAO(cliente.getPessoaFisica()).update(idFuncionario));
			stmt.setInt(2, new PessoaJuridicaDAO(cliente.getPessoaJuridica()).update(idFuncionario));
			stmt.setInt(3, new PromocionalDAO(cliente.getCodigo()).update(idFuncionario));
			stmt.setInt(4, new AutenticacaoDAO(cliente.getAutenticacao()).update(idFuncionario));
			stmt.setInt(5, cliente.getIdCliente());
			
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Cliente", "UPDATE", cliente.getIdCliente());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.cliente.getIdCliente();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Cliente where idCliente = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, cliente.getIdCliente());
			
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Cliente", "DELETE", cliente.getIdCliente());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}

	}

	public Cliente selectCliente(int id) {
		String sql = "select * from Cliente where idCliente = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Cliente cliente = new Cliente();
			
			while(rs.next()) {
				cliente.setIdCliente(rs.getInt("idCliente"));
				cliente.setPessoaFisica(new PessoaFisicaDAO().selectPessoaFisica(rs.getInt("PessoaFisica_idPessoaFisica")));
				cliente.setPessoaJuridica(new PessoaJuridicaDAO().selectPessoaJuridica(rs.getInt("PessoaJuridica_idPessoaJuridica")));
				cliente.setCodigo(new PromocionalDAO().selectPromocional(rs.getInt("Promocional_idPromocional")));
				cliente.setAutenticacao(new AutenticacaoDAO().selectAutentica(rs.getInt("Autenticacao_idAutenticacao")));
				
				this.setCliente(cliente);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.cliente;
	}
	
	public int insertPessoaFisica(int idFuncionario) {
		String sql = "insert into Cliente (PessoaFisica_idPessoaFisica, PessoaJuridica_idPessoaJuridica, Promocional_idPromocional, "
				+ "Autenticacao_idAutenticacao) values (?,?,?,?);";
		String[] id = {"idCliente"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, new PessoaFisicaDAO(cliente.getPessoaFisica()).insert(idFuncionario));
			stmt.setInt(2, 1);
			stmt.setInt(3, new PromocionalDAO(cliente.getCodigo()).insert(idFuncionario));
			stmt.setInt(4, new AutenticacaoDAO(cliente.getAutenticacao()).insert(idFuncionario));
			
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.cliente.setIdCliente(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Cliente", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.cliente.getIdCliente();
	}
	
	public int insertPessoaJuridica(int idFuncionario) {
		String sql = "insert into Cliente (PessoaFisica_idPessoaFisica, PessoaJuridica_idPessoaJuridica, Promocional_idPromocional, "
				+ "Autenticacao_idAutenticacao) values (?,?,?,?);";
		String[] id = {"idCliente"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, 1);
			stmt.setInt(2, new PessoaJuridicaDAO(cliente.getPessoaJuridica()).insert(idFuncionario));
			stmt.setInt(3, new PromocionalDAO(cliente.getCodigo()).insert(idFuncionario));
			stmt.setInt(4, new AutenticacaoDAO(cliente.getAutenticacao()).insert(idFuncionario));
			
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.cliente.setIdCliente(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Cliente", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.cliente.getIdCliente();
	}
	
	public int selectIdClienteCpf(String cpf) {
		int id = 1;
		String sql = "select idCliente from Cliente join PessoaFisica on PessoaFisica.idPessoaFisica = Cliente.PessoaFisica_idPessoaFisica"
				+ " where CPF = ?;";
		
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, cpf);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				id = rs.getInt("idCliente");
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return id;	
	}
	
	public int selectIdClienteCnpj(String cnpj) {
		int id = 1;
		String sql = "select idCliente from Cliente join PessoaJuridica on PessoaJuridica.idPessoaJuridica = "
				+ "Cliente.PessoaJuridica_idPessoaJuridica where CNPJ = ?;";
		
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, cnpj);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				id = rs.getInt("idCliente");
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return id;	
	}
	
	public List<Cliente> selectCliente() {
		List<Cliente> listaCliente = new ArrayList<>();
		List<PessoaJuridica> listaPj = new PessoaJuridicaDAO().selectPessoaJuridica();
		List<PessoaFisica> listaPf = new PessoaFisicaDAO().selectPessoaFisica();
		
		String sql = "select * from cliente \r\n" + 
				"join promocional on promocional.idPromocional = cliente.Promocional_idPromocional\r\n" + 
				"join autenticacao on autenticacao.idAutenticacao = cliente.Autenticacao_idAutenticacao\r\n" + 
				"order by idCliente;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				
				Cliente cliente = new Cliente();
				cliente.setAutenticacao(new Autenticacao());
				cliente.setCodigo(new Promocional());
				cliente.setPessoaFisica(new PessoaFisica());
				cliente.setPessoaJuridica(new PessoaJuridica());
				
				cliente.setIdCliente(rs.getInt("idCliente"));
				cliente.getPessoaFisica().setIdPessoaFisica(rs.getInt("PessoaFisica_idPessoaFisica"));
				cliente.getPessoaJuridica().setIdPessoaJuridica(rs.getInt("PessoaJuridica_idPessoaJuridica"));
				cliente.getCodigo().setIdPromocional(rs.getInt("Promocional_idPromocional"));
				cliente.getAutenticacao().setIdAutenticacao(rs.getInt("Autenticacao_idAutenticacao"));
				cliente.getCodigo().setHash(rs.getString("Hash"));
				cliente.getCodigo().setUsado(rs.getBoolean("Usado"));
				cliente.getCodigo().setValidade(rs.getDate("Validade"));
				cliente.getAutenticacao().setUsuario(rs.getString("Usuario"));
				cliente.getAutenticacao().setSenha(rs.getString("Senha"));
				cliente.getAutenticacao().setDataCriacao(rs.getDate("DataCriacao"));
				cliente.getAutenticacao().setTipoUsuario(rs.getString("TipoUsuario"));
				
				if(cliente.getPessoaFisica().getIdPessoaFisica() > 1) {
					listaPf.forEach(pessoa -> {
						if(pessoa.getIdPessoaFisica() == cliente.getPessoaFisica().getIdPessoaFisica()) {
							cliente.setPessoaFisica(pessoa);
							
						}
					});
				} else if(cliente.getPessoaJuridica().getIdPessoaJuridica() > 1){
					listaPj.forEach(pessoa -> {
						if(pessoa.getIdPessoaJuridica() == cliente.getPessoaJuridica().getIdPessoaJuridica()) {
							cliente.setPessoaJuridica(pessoa);
						}
					});
				}
				
				listaCliente.add(cliente);
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return listaCliente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
