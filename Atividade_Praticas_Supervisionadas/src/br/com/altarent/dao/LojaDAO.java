package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Contato;
import br.com.altarent.modelo.Endereco;
import br.com.altarent.modelo.Loja;

public class LojaDAO extends DAO {
	private Loja loja;

	public LojaDAO() {
	}

	public LojaDAO(Loja loja) {
		this.loja = loja;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Loja;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				int idContatoDAO = rs.getInt("Contato_idContato");
				int idEnderecoDAO = rs.getInt("Endereco_idEndereco");

				Loja loja = new Loja();
				loja.setIdLoja(rs.getInt("idLoja"));
				loja.setEndereco(new EnderecoDAO().selectEndereco(idEnderecoDAO));
				loja.setContato(new ContatoDAO().selectContato(idContatoDAO));

				lista.add(loja);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Loja (Endereco_idEndereco, Contato_idContato) values (?,?);";
		String[] id = { "idLoja" };
		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {

			stmt.setInt(1, new EnderecoDAO(loja.getEndereco()).insert(idFuncionario));
			stmt.setInt(2, new ContatoDAO(loja.getContato()).insert(idFuncionario));
			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.loja.setIdLoja(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Loja", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.loja.getIdLoja();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update loja set Endereco_idEndereco = ?, Contato_idContato = ? where idLoja = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new EnderecoDAO(loja.getEndereco()).update(idFuncionario));
			stmt.setInt(2, new ContatoDAO(loja.getContato()).update(idFuncionario));
			stmt.setInt(3, loja.getIdLoja());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Loja", "UPDATE", loja.getIdLoja());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.loja.getIdLoja();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Loja where idLoja = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, loja.getIdLoja());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Loja", "DELETE", loja.getIdLoja());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

	}

	public Loja selectLoja(int id) {
		String sql = "Select * from Loja where idLoja = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();
			
			Loja loja = new Loja();
			
			while (rs.next()) {
				
				loja.setIdLoja(rs.getInt("idLoja"));
				loja.setEndereco(new EnderecoDAO().selectEndereco(rs.getInt("Endereco_idEndereco")));
				loja.setContato(new ContatoDAO().selectContato(rs.getInt("Contato_idContato")));
				
				this.setLoja(loja);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.loja;
	}
	
	public List<Loja> selectLoja() {
		List<Loja> lista = new ArrayList<>();
		String sql = "select * from loja\r\n" + 
				"join endereco on endereco.idEndereco = loja.Endereco_idEndereco\r\n" + 
				"join contato on contato.idContato = loja.Contato_idContato;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Loja loja = new Loja(new Contato(), new Endereco());
				
				loja.getEndereco().setIdEndereco(rs.getInt("Endereco_idEndereco"));
				loja.getContato().setIdContato(rs.getInt("Contato_idContato"));
				loja.getEndereco().setNomeRua(rs.getString("NomeRua"));
				loja.getEndereco().setCEP(rs.getString("CEP"));
				loja.getEndereco().setBairro(rs.getString("Bairro"));
				loja.getEndereco().setCidade(rs.getString("Cidade"));
				loja.getEndereco().setComplemento(rs.getString("Complemento"));
				loja.getEndereco().setNumero(rs.getInt("Numero"));
				loja.getContato().setTelefonePrincipal(rs.getString("TelefonePrincipal"));
				loja.getContato().setTelefoneSecundario(rs.getString("TelefoneSecundario"));
				loja.getContato().setEmail(rs.getString("E-mail"));
				
				lista.add(loja);
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	public Loja getLoja() {
		return loja;
	}

	public void setLoja(Loja loja) {
		this.loja = loja;
	}
}
