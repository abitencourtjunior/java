package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Carro;
import br.com.altarent.modelo.EstadoDeUso;
import br.com.altarent.modelo.Manutencao;
import br.com.altarent.modelo.Marca;
import br.com.altarent.modelo.ModeloTerrestre;
import br.com.altarent.modelo.PessoaJuridica;
import br.com.altarent.modelo.Seguro;

public class CarroDAO extends DAO {
	private Carro carro;
	
	public CarroDAO() {}
	
	public CarroDAO(Carro carro) {
		this.carro = carro;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Carro;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				
				Carro carro = new Carro();
				
				carro.setIdCarro(rs.getInt("idCarro"));
				carro.setModeloTerrestre(new ModeloTerrestreDAO().selectModeloTerrestre(rs.getInt("ModeloTerrestre_idModeloTerrestre")));
				carro.setChassi(rs.getString("Chassi"));
				carro.setDataFabricacao(rs.getInt("DataFabricacao"));
				carro.setPlaca(rs.getString("Placa"));
				carro.setCor(rs.getString("Cor"));
				carro.setSeguro(new SeguroDAO().selectSeguro(rs.getInt("Seguro_idSeguro")));
				carro.setKilometragem(rs.getShort("Kilometragem"));
				carro.setManutencao(new ManutencaoDAO().selectManutencao(rs.getInt("Manutencao_idManutencao")));
				carro.setValorAluguel(rs.getDouble("ValorAluguel"));
				carro.setRenavam(rs.getString("Renavam"));
				carro.setValorDeCompra(rs.getDouble("ValorDeCompra"));
				carro.setValorAtual(rs.getDouble("ValorAtual"));
				carro.setValorGerado(rs.getDouble("TotalGerado"));
				carro.setEstadoDeUso(new EstadoDeUsoDAO().selectEstadoDeUso(rs.getInt("EstadoDeUso_idEstadoDeUso")));
				
				lista.add(carro);
			}
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Carro (ModeloTerrestre_idModeloTerrestre, Chassi, DataFabricacao, Placa, Cor, Seguro_idSeguro, Kilometragem, "
				+ "Manutencao_idManutencao, "
				+ "ValorAluguel, Renavam, ValorDeCompra, ValorAtual, TotalGerado, EstadoDeUso_idEstadoDeUso) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		String [] id = {"idCarro"};
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			//PRECISA DE TESTE AS CHAVES ESTRANGEIRAS!!!!!
			stmt.setInt(1, new ModeloTerrestreDAO(carro.getModeloTerrestre()).insert(idFuncionario));
			stmt.setString(2, carro.getChassi());
			stmt.setInt(3, carro.getDataFabricacao());
			stmt.setString(4, carro.getPlaca());
			stmt.setString(5, carro.getCor());
			stmt.setInt(6, new SeguroDAO(carro.getSeguro()).insert(idFuncionario));
			stmt.setShort(7, carro.getKilometragem());
			stmt.setInt(8, new ManutencaoDAO().selectManutencao(carro.getManutencao().getIdManutencao()).getIdManutencao());
			stmt.setDouble(9, carro.getValorAluguel());
			stmt.setString(10, carro.getRenavam());
			stmt.setDouble(11, carro.getValorDeCompra());
			stmt.setDouble(12, carro.getValorAtual());
			stmt.setDouble(13, carro.getValorGerado());
			stmt.setInt(14, new EstadoDeUsoDAO().selectEstadoDeUso(carro.getEstadoDeUso().getIdEstadoDeUso()).getIdEstadoDeUso());
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.carro.setIdCarro(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Carro", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.carro.getIdCarro();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Carro set Seguro_idSeguro = ?, Kilometragem = ?, Manutencao_idManutencao = ?, ValorAluguel = ?, ValorAtual = ?, "
				+ "TotalGerado = ?, EstadoDeUso_idEstadoDeUso = ? where idCarro = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new SeguroDAO().selectSeguro(carro.getSeguro().getIdSeguro()).getIdSeguro());
			stmt.setShort(2, carro.getKilometragem());
			stmt.setInt(3, new ManutencaoDAO(carro.getManutencao()).insert(idFuncionario));
			stmt.setDouble(4, carro.getValorAluguel());
			stmt.setDouble(5, carro.getValorAtual());
			stmt.setDouble(6, carro.getValorGerado());
			stmt.setInt(7, new EstadoDeUsoDAO().selectEstadoDeUso(carro.getEstadoDeUso().getIdEstadoDeUso()).getIdEstadoDeUso());
			stmt.setInt(8, carro.getIdCarro());
			
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Carro", "UPDATE", carro.getIdCarro());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.carro.getIdCarro();
	}
	
	public void updateManutencao(int idFuncionario, String placa) {
		String sql = "update Carro set Manutencao_idManutencao = ?, TotalGerado = ? where Placa = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			int idManutencao = new ManutencaoDAO(carro.getManutencao()).insert(idFuncionario);
			stmt.setInt(1, idManutencao);
			stmt.setDouble(2, ((new CarroDAO().selectCarroPlaca(placa).getValorGerado()) - 
					(new ManutencaoDAO().selectManutencao(idManutencao).getValorManutencao())));
			stmt.setString(3, placa);
			
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Carro", "UPDATE", new CarroDAO().selectCarroPlaca(placa).getIdCarro());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		
	}
	
	public void updateSeguro(int idFuncionario, String placa) {
		String sql = "update Carro set Seguro_idSeguro = ?, TotalGerado = ? where Placa = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			int idSeguro = new SeguroDAO(carro.getSeguro()).insert(idFuncionario);
			stmt.setInt(1, idSeguro);
			stmt.setDouble(2, ((new CarroDAO().selectCarroPlaca(placa).getValorGerado()) - 
					(new SeguroDAO().selectSeguro(idSeguro).getValor())));
			stmt.setString(3, placa);
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Carro", "UPDATE", new CarroDAO().selectCarroPlaca(placa).getIdCarro());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public void updateTotalGerado(int idFuncionario, double novoValor, String placa) {
		String sql = "update Carro set TotalGerado = ? where Placa = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setDouble(1, novoValor);
			stmt.setString(2, placa);
			
			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Carro", "UPDATE", new CarroDAO().selectCarroPlaca(placa).getIdCarro());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Carro where idCarro = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, carro.getIdCarro());
			
			stmt.execute();
			
			ProceduresDAO.registraLog(idFuncionario, "Carro", "DELETE", carro.getIdCarro());
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}

	}
	
	public Carro selectCarro(int id) {
		String sql = "select * from Carro where idCarro = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			Carro carro = new Carro();
			
			while(rs.next()) {
				
				carro.setIdCarro(rs.getInt("idCarro"));
				carro.setModeloTerrestre(new ModeloTerrestreDAO().selectModeloTerrestre(rs.getInt("ModeloTerrestre_idModeloTerrestre")));
				carro.setChassi(rs.getString("Chassi"));
				carro.setDataFabricacao(rs.getInt("DataFabricacao"));
				carro.setPlaca(rs.getString("Placa"));
				carro.setCor(rs.getString("Cor"));
				carro.setSeguro(new SeguroDAO().selectSeguro(rs.getInt("Seguro_idSeguro")));
				carro.setKilometragem(rs.getShort("Kilometragem"));
				carro.setManutencao(new ManutencaoDAO().selectManutencao(rs.getInt("Manutencao_idManutencao")));
				carro.setValorAluguel(rs.getDouble("ValorAluguel"));
				carro.setRenavam(rs.getString("Renavam"));
				carro.setValorDeCompra(rs.getDouble("ValorDeCompra"));
				carro.setValorAtual(rs.getDouble("ValorAtual"));
				carro.setValorGerado(rs.getDouble("TotalGerado"));
				carro.setEstadoDeUso(new EstadoDeUsoDAO().selectEstadoDeUso(rs.getInt("EstadoDeUso_idEstadoDeUso")));
				
				
				this.setCarro(carro);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.carro;
	}
	
	public Carro selectCarroPlaca(String placa) {
		String sql = "select * from Carro where Placa = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, placa);
			
			ResultSet rs = stmt.executeQuery();
			
			Carro carro = new Carro();
			
			while(rs.next()) {
				
				carro.setIdCarro(rs.getInt("idCarro"));
				carro.setModeloTerrestre(new ModeloTerrestreDAO().selectModeloTerrestre(rs.getInt("ModeloTerrestre_idModeloTerrestre")));
				carro.setChassi(rs.getString("Chassi"));
				carro.setDataFabricacao(rs.getInt("DataFabricacao"));
				carro.setPlaca(rs.getString("Placa"));
				carro.setCor(rs.getString("Cor"));
				carro.setSeguro(new SeguroDAO().selectSeguro(rs.getInt("Seguro_idSeguro")));
				carro.setKilometragem(rs.getShort("Kilometragem"));
				carro.setManutencao(new ManutencaoDAO().selectManutencao(rs.getInt("Manutencao_idManutencao")));
				carro.setValorAluguel(rs.getDouble("ValorAluguel"));
				carro.setRenavam(rs.getString("Renavam"));
				carro.setValorDeCompra(rs.getDouble("ValorDeCompra"));
				carro.setValorAtual(rs.getDouble("ValorAtual"));
				carro.setValorGerado(rs.getDouble("TotalGerado"));
				carro.setEstadoDeUso(new EstadoDeUsoDAO().selectEstadoDeUso(rs.getInt("EstadoDeUso_idEstadoDeUso")));
				
				
				this.setCarro(carro);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.carro;
	}
	
	public double selectValorAluguel(String placa) {
		String sql = "select ValorAluguel from Carro where Placa = ?;";
		double valor = 0;
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, placa);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				valor = rs.getDouble("ValorAluguel");
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return valor;
		
	}
	
	public List<Carro> selectCarro() {
		List<Carro> listaCarros = new ArrayList<>();
		
		String sql = "select * from carro\r\n" + 
				"join modeloterrestre on modeloterrestre.idModeloTerrestre = carro.ModeloTerrestre_idModeloTerrestre\r\n" + 
				"join marca on marca.idMarca = modeloterrestre.Marca_idMarca\r\n" + 
				"join manutencao on manutencao.idManutencao = carro.Manutencao_idManutencao\r\n" + 
				"join seguro on seguro.idSeguro = carro.Seguro_idSeguro\r\n" + 
				"join estadodeuso on estadodeuso.idEstadoDeUso  = carro.EstadoDeUso_idEstadoDeUso\r\n" + 
				"order by idCarro;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				
				Carro carro = new Carro();
				carro.setEstadoDeUso(new EstadoDeUso());
				carro.setManutencao(new Manutencao());
				carro.setSeguro(new Seguro());
				carro.getSeguro().setPessoaJuridica(new PessoaJuridica());
				carro.setModeloTerrestre(new ModeloTerrestre());
				carro.getModeloTerrestre().setMarca(new Marca());
				
				carro.setIdCarro(rs.getInt("idCarro"));
				carro.setChassi(rs.getString("Chassi"));
				carro.setDataFabricacao(rs.getInt("DataFabricacao"));
				carro.setPlaca(rs.getString("Placa"));
				carro.setCor(rs.getString("Cor"));
				carro.getSeguro().setIdSeguro(rs.getInt("Seguro_idSeguro"));
				carro.setKilometragem(rs.getShort("Kilometragem"));
				carro.getManutencao().setIdManutencao(rs.getInt("Manutencao_idManutencao"));
				carro.setValorAluguel(rs.getDouble("ValorAluguel"));
				carro.setRenavam(rs.getString("Renavam"));
				carro.setValorDeCompra(rs.getDouble("ValorDeCompra"));
				carro.setValorAtual(rs.getDouble("ValorAtual"));
				carro.setValorGerado(rs.getDouble("TotalGerado"));
				carro.getModeloTerrestre().setIdModeloTerrestre(rs.getInt("ModeloTerrestre_idModeloTerrestre"));
				carro.getEstadoDeUso().setIdEstadoDeUso(rs.getInt("EstadoDeUso_idEstadoDeUso"));
				carro.getModeloTerrestre().setNomeModelo(rs.getString("NomeModelo"));
				carro.getModeloTerrestre().setCambio(rs.getString("Cambio"));
				carro.getModeloTerrestre().setArCondicionado(rs.getBoolean("ArCondicionado"));
				carro.getModeloTerrestre().setDirecao(rs.getString("Direcao"));
				carro.getModeloTerrestre().setVidrosEletricos(rs.getBoolean("VidrosEletricos"));
				carro.getModeloTerrestre().setPortas(rs.getByte("Portas"));
				carro.getModeloTerrestre().setTravaEletrica(rs.getBoolean("TravaEletrica"));
				carro.getModeloTerrestre().setAirBag(rs.getBoolean("AirBag"));
				carro.getModeloTerrestre().setAbs(rs.getBoolean("ABS"));
				carro.getModeloTerrestre().setCapacidadePessoas(rs.getByte("CapacidadePessoas"));
				carro.getModeloTerrestre().setLitragemPortaMala(rs.getShort("LitragemPortaMalas"));
				carro.getModeloTerrestre().getMarca().setIdMarca(rs.getInt("Marca_idMarca"));
				carro.getModeloTerrestre().getMarca().setNome(rs.getString("Nome"));
				carro.getManutencao().setTipoManutencao(rs.getString("TipoManutencao"));
				carro.getManutencao().setDataManutencao(rs.getDate("DataManutencao"));
				carro.getManutencao().setKilometragem(rs.getShort(34));
				carro.getManutencao().setValorManutencao(rs.getFloat("ValorManutencao"));
				carro.getManutencao().setDescricao(rs.getString("Descricao"));
				carro.getSeguro().setValor(rs.getFloat("Valor"));
				carro.getSeguro().getPessoaJuridica().setIdPessoaJuridica(rs.getInt("PessoaJuridica_idPessoaJuridica"));
				carro.getEstadoDeUso().setEstadoDeUso(rs.getString("EstadoDeUso"));
				
				listaCarros.add(carro);
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return listaCarros;
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}
}
