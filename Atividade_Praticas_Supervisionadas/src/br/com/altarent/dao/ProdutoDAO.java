package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Carro;
import br.com.altarent.modelo.Disponibilidade;
import br.com.altarent.modelo.Disponivel;
import br.com.altarent.modelo.Jetski;
import br.com.altarent.modelo.Produto;

public class ProdutoDAO extends DAO {
	private Produto produto;

	public ProdutoDAO() {
	}

	public ProdutoDAO(Produto produto) {
		this.produto = produto;
	}
	
	//M�todo para o Exerc�cio 1 da Lista de Banco De Dados
	public List<Produto> selectCarrosFabMaiorSeisAnos() {
		
		List<Produto> lista = new ArrayList<>();
		List<Carro> listaCarros = new CarroDAO().selectCarro();
		
		String sql = "select * from Produto \r\n" + 
				"join Carro on Produto.Carro_idCarro1 = Carro.idCarro\r\n" + 
				"join disponibilidade on disponibilidade.idDisponibilidade = produto.Disponibilidade_idDisponibilidade\r\n" +
				"join disponivel on disponivel.iddisponivel = disponibilidade.disponivel_iddisponivel\r\n" +
				"where year(now()) - DataFabricacao > 6 and Jetski_idJetski = 1; ";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Produto produto = new Produto();
				produto.setCarro(new Carro());
				produto.setJetski(new Jetski());
				produto.setDisponibilidade(new Disponibilidade());
				produto.getDisponibilidade().setDisponivel(new Disponivel());
				
				produto.setIdProduto(rs.getInt("idProduto"));
				produto.getCarro().setIdCarro(rs.getInt("Carro_idCarro1"));
				produto.getJetski().setIdJetski(rs.getInt("Jetski_idJetski"));
				produto.getDisponibilidade().setIdDisponibilidade(rs.getInt("Disponibilidade_idDisponibilidade"));
				produto.getDisponibilidade().setData(rs.getDate("Data"));
				produto.getDisponibilidade().setHora(rs.getTime("Hora"));
				produto.getDisponibilidade().getDisponivel().setId(rs.getInt("Disponivel_idDisponivel"));
				produto.getDisponibilidade().getDisponivel().setDisponivel(rs.getString("Disponivel"));
				
				listaCarros.forEach(carro -> {
					if(carro.getIdCarro() == produto.getCarro().getIdCarro()) {
						produto.setCarro(carro);
					}
				});
				
				lista.add(produto);
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}
	
public List<Produto> selectJetskisFabMaiorSeisAnos() {
		
		List<Produto> lista = new ArrayList<>();
		List<Jetski> listaJetskis = new JetskiDAO().selectJetski();
		
		String sql = "select * from Produto\r\n" + 
				"join Jetski on Produto.Jetski_idJetski = jetski.idJetski\r\n" + 
				"join disponibilidade on disponibilidade.idDisponibilidade = produto.Disponibilidade_idDisponibilidade\r\n" + 
				"join disponivel on disponivel.iddisponivel = disponibilidade.disponivel_iddisponivel\r\n" + 
				"where year(now()) - DataFabricacao > 6 and Carro_idCarro1 = 1 and jetski.idJetski > 1;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Produto produto = new Produto();
				produto.setCarro(new Carro());
				produto.setJetski(new Jetski());
				produto.setDisponibilidade(new Disponibilidade());
				produto.getDisponibilidade().setDisponivel(new Disponivel());
				
				produto.setIdProduto(rs.getInt("idProduto"));
				produto.getCarro().setIdCarro(rs.getInt("Carro_idCarro1"));
				produto.getJetski().setIdJetski(rs.getInt("Jetski_idJetski"));
				produto.getDisponibilidade().setIdDisponibilidade(rs.getInt("Disponibilidade_idDisponibilidade"));
				produto.getDisponibilidade().setData(rs.getDate("Data"));
				produto.getDisponibilidade().setHora(rs.getTime("Hora"));
				produto.getDisponibilidade().getDisponivel().setId(rs.getInt("Disponivel_idDisponivel"));
				produto.getDisponibilidade().getDisponivel().setDisponivel(rs.getString("Disponivel"));
				
				listaJetskis.forEach(jet -> {
					if(jet.getIdJetski() == produto.getJetski().getIdJetski()) {
						produto.setJetski(jet);
					}
				});
				
				lista.add(produto);
			}
			
			stmt.closeOnCompletion();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}


	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Produto;";

		try (Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Produto produto = new Produto();

				produto.setIdProduto(rs.getInt("idProduto"));
				produto.setCarro(new CarroDAO().selectCarro(rs.getInt("Carro_idCarro1")));
				produto.setJetski(new JetskiDAO().selectJetski(rs.getInt("Jetski_idJetski")));
				produto.setDisponibilidade(
						new DisponibilidadeDAO().selectDisponibilidade(rs.getInt("Disponibilidade_idDisponibilidade")));

				lista.add(produto);

			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Produto (Carro_idCarro1, Jetski_idJetski, Disponibilidade_idDisponibilidade) values (?,?,?);";
		String[] id = { "idProduto" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, new CarroDAO(produto.getCarro()).insert(idFuncionario));
			stmt.setInt(2, new JetskiDAO(produto.getJetski()).insert(idFuncionario));
			stmt.setInt(3, new DisponibilidadeDAO(produto.getDisponibilidade()).insert(idFuncionario));
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.produto.setIdProduto(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Produto", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.produto.getIdProduto();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Produto set Disponibilidade_idDisponibilidade = ? where idProduto = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new DisponibilidadeDAO()
					.selectDisponibilidade(produto.getDisponibilidade().getIdDisponibilidade()).getIdDisponibilidade());
			stmt.setInt(2, produto.getIdProduto());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Produto", "UPDATE", produto.getIdProduto());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.produto.getIdProduto();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Produto where idProduto = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, produto.getIdProduto());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Produto", "DELETE", produto.getIdProduto());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Produto selectProduto(int id) {
		String sql = "select * from Produto where idProduto = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			Produto produto = new Produto();

			while (rs.next()) {

				produto.setIdProduto(rs.getInt("idProduto"));
				produto.setCarro(new CarroDAO().selectCarro(rs.getInt("Carro_idCarro1")));
				produto.setJetski(new JetskiDAO().selectJetski(rs.getInt("Jetski_idJetski")));
				produto.setDisponibilidade(
						new DisponibilidadeDAO().selectDisponibilidade(rs.getInt("Disponibilidade_idDisponibilidade")));

				this.setProduto(produto);

			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return this.produto;
	}
	
	public int insertJetski(int idFuncionario) {
		String sql = "insert into Produto (Carro_idCarro1, Jetski_idJetski, Disponibilidade_idDisponibilidade) values (?,?,?);";
		String[] id = { "idProduto" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, 1);
			stmt.setInt(2, new JetskiDAO(produto.getJetski()).insert(idFuncionario));
			stmt.setInt(3, new DisponibilidadeDAO(produto.getDisponibilidade()).insert(idFuncionario));
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.produto.setIdProduto(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Produto", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.produto.getIdProduto();
	}
	
	public int insertCarro(int idFuncionario) {
		String sql = "insert into Produto (Carro_idCarro1, Jetski_idJetski, Disponibilidade_idDisponibilidade) values (?,?,?);";
		String[] id = { "idProduto" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setInt(1, new CarroDAO(produto.getCarro()).insert(idFuncionario));
			stmt.setInt(2, 1);
			stmt.setInt(3, new DisponibilidadeDAO(produto.getDisponibilidade()).insert(idFuncionario));
			
			stmt.execute();
			
			ResultSet rs = stmt.getGeneratedKeys();
			
			while(rs.next()) {
				this.produto.setIdProduto(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Produto", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.produto.getIdProduto();
	}
	
	public Produto selectProdutoCarroPlaca(String placa) {
		List<Produto> lista = selectProduto();
		
		lista.forEach(produtoLista -> {
			if(produtoLista.getCarro().getPlaca().equals(placa)) {
				this.produto = produtoLista;
			}
		});
		
		return this.produto;
	}
	
	public int selectIdProdutoPlaca(String placa) {
		int id = 1;
		String sql = "select idProduto from Produto join Carro on Carro.idCarro = Produto.Carro_idCarro1 where Placa = ?;";
		
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, placa);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				id = rs.getInt("idProduto");
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return id;	
	}
	
	public Produto selectProdutoJetDocumento(int documento) {
		List<Produto> lista = selectProduto();
		
		lista.forEach(produtoLista -> {
			if(produtoLista.getJetski().getDocumentoMarinha() == documento) {
				this.produto = produtoLista;
			}
		});
		
		return this.produto;
	}
	
	public int selectIdProdutoDocumento(int documento) {
		int id = 1;
		String sql = "select idProduto from Produto join Jetski on Jetski.idJetski = Produto.Jetski_idJetski where DocumentoMarinha = ?;";
		
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, documento);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				id = rs.getInt("idProduto");
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return id;
		
	}
	
	public int selectIdDisponibilidadeDoProdutoCarro(String placa) {
		int idDisponibilidade = 0;
		String sql = "select idDisponibilidade from produto "
				+ "join disponibilidade on Disponibilidade.idDisponibilidade = Produto.Disponibilidade_idDisponibilidade "
				+ "join Carro on Carro.idCarro = Produto.Carro_idCarro1 where Placa = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, placa);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				idDisponibilidade = rs.getInt("idDisponibilidade");
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return idDisponibilidade;
	}
	
	public int selectIdDisponibilidadeDoProdutoJetski(int documento) {
		int idDisponibilidade = 0;
		String sql = "select idDisponibilidade from produto "
				+ "join disponibilidade on Disponibilidade.idDisponibilidade = Produto.Disponibilidade_idDisponibilidade "
				+ "join Jetski on Jetski.idJetski = Produto.Jetski_idJetski where DocumentoMarinha = ?;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, documento);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				idDisponibilidade = rs.getInt("idDisponibilidade");
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return idDisponibilidade;
	}
	
	public int updateProdutoCarro(int idFuncionario, LocalDate data, String placa) {
		String sql = "update Produto set Disponibilidade_idDisponibilidade = ? where idProduto = ?;";
		int idDisponibilidade = new ProdutoDAO().selectIdDisponibilidadeDoProdutoCarro(placa);
		int idProdutoCarro = new ProdutoDAO().selectIdProdutoPlaca(placa);
		
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new DisponibilidadeDAO().updateDisponibilidade(idFuncionario, idDisponibilidade, data));
			stmt.setInt(2, idProdutoCarro);

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Produto", "UPDATE", idProdutoCarro);
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return idProdutoCarro;
	}
	
	public int updateProdutoJetski(int idFuncionario, LocalDate data, int documento) {
		String sql = "update Produto set Disponibilidade_idDisponibilidade = ? where idProduto = ?;";
		int idDisponibilidade = new ProdutoDAO().selectIdDisponibilidadeDoProdutoJetski(documento);
		int idProdutoJetski = new ProdutoDAO().selectIdProdutoDocumento(documento);
		
		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, new DisponibilidadeDAO().updateDisponibilidade(idFuncionario, idDisponibilidade, data));
			stmt.setInt(2, idProdutoJetski);

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Produto", "UPDATE", idProdutoJetski);
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return idProdutoJetski;
	}
	
	public List<Produto> selectProduto() {
		List<Produto> lista = new ArrayList<>();
		List<Carro> listaCarros = new CarroDAO().selectCarro();
		List<Jetski> listaJetski = new JetskiDAO().selectJetski();
		
		String sql = "select * from Produto\r\n" + 
				"join Disponibilidade on disponibilidade.idDisponibilidade = produto.Disponibilidade_idDisponibilidade\r\n" + 
				"join disponivel on disponivel.idDisponivel = disponibilidade.Disponivel_idDisponivel;";
		
		try(Statement stmt = conexao.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Produto produto = new Produto();
				produto.setCarro(new Carro());
				produto.setJetski(new Jetski());
				produto.setDisponibilidade(new Disponibilidade());
				produto.getDisponibilidade().setDisponivel(new Disponivel());
				
				produto.setIdProduto(rs.getInt("idProduto"));
				produto.getCarro().setIdCarro(rs.getInt("Carro_idCarro1"));
				produto.getJetski().setIdJetski(rs.getInt("Jetski_idJetski"));
				produto.getDisponibilidade().setIdDisponibilidade(rs.getInt("Disponibilidade_idDisponibilidade"));
				produto.getDisponibilidade().setData(rs.getDate("Data"));
				produto.getDisponibilidade().setHora(rs.getTime("Hora"));
				produto.getDisponibilidade().getDisponivel().setId(rs.getInt("Disponivel_idDisponivel"));
				produto.getDisponibilidade().getDisponivel().setDisponivel(rs.getString("Disponivel"));
				
				listaCarros.forEach(carro -> {
					if(carro.getIdCarro() == produto.getCarro().getIdCarro()) {
						produto.setCarro(carro);
					}
				});
				
				listaJetski.forEach(jet -> {
					if(jet.getIdJetski() == produto.getJetski().getIdJetski()) {
						produto.setJetski(jet);
					}
				});
				
				lista.add(produto);
			}
			
			stmt.closeOnCompletion();
			conexao.close();
					
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}
