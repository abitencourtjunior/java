package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Mensal;

public class MensalDAO extends DAO {
	private Mensal mensal;

	public MensalDAO() {
	}
	
	public MensalDAO(Mensal mensal) {
		this.mensal = mensal;
	}

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Mensal;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Mensal mensal = new Mensal();

				mensal.setId(rs.getInt("idMensal"));
				mensal.setQuantidade(rs.getByte("QtdeMes"));
				mensal.setPrecoFinal(rs.getDouble("ValorFinal"));

				lista.add(mensal);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Mensal (QtdeMes, ValorFinal) values (?,?);";
		String[] id = { "idMensal" };

		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setByte(1, mensal.getQuantidade());
			stmt.setDouble(2, mensal.getPrecoFinal());

			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.mensal.setId(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Mensal", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.mensal.getId();
	}

	@Override
	public int update(int idFuncionario) {
		//M�TODO N�O UTILIZADO NESTA DAO
		return this.mensal.getId();
	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Mensal where idMensal = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, mensal.getId());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Mensal", "DELETE", mensal.getId());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public Mensal selectMensal(int id) {
		String sql = "select * from Mensal where idMensal = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			Mensal mensal = new Mensal();

			while (rs.next()) {
				mensal.setId(rs.getInt("idMensal"));
				mensal.setQuantidade(rs.getByte("QtdeMes"));
				mensal.setPrecoFinal(rs.getDouble("ValorFinal"));

				this.setMensal(mensal);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return this.mensal;
	}

	public Mensal getMensal() {
		return mensal;
	}

	public void setMensal(Mensal mensal) {
		this.mensal = mensal;
	}
}
