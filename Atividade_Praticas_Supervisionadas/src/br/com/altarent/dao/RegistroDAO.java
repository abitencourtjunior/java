package br.com.altarent.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Registro;

public class RegistroDAO extends DAO {
	private Registro registro;

	@Override
	public List<Object> select() {
		List<Object> lista = new ArrayList<>();
		String sql = "select * from Registro;";
		
		try(PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				Registro registro = new Registro();
				
				registro.setIdRegistro(rs.getInt("idRegistro"));
				registro.setDataModificacao(rs.getDate("DataModificacao"));
				registro.setHora(rs.getTime("Hora"));
				registro.setIdFuncionario(rs.getInt("idFuncionario"));
				registro.setNomeTabela(rs.getString("NomeTabela"));
				registro.setTipoDeRegistro(rs.getString("TipoDeRegistro"));
				registro.setIdDaTupla(rs.getInt("idDaTupla"));
				
				lista.add(registro);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch(SQLException ex) {
			throw new RuntimeException(ex);
		}
		
		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		//M�todo n�o utilizado nesta DAO
		return 0;
	}

	@Override
	public int update(int idFuncionario) {
		//M�todo n�o utilizado nesta DAO
		return 0;
	}

	@Override
	public void delete(int idFuncionario) {
		//M�todo n�o utilizado nesta DAO
	}

	public Registro getRegistro() {
		return registro;
	}

	public void setRegistro(Registro registro) {
		this.registro = registro;
	}
}
