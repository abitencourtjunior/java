package br.com.altarent.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.altarent.modelo.Manutencao;

public class ManutencaoDAO extends DAO {
	private Manutencao manutencao;

	public ManutencaoDAO() {
	}

	public ManutencaoDAO(Manutencao manutencao) {
		this.manutencao = manutencao;
	}

	@Override
	public List<Object> select() {

		List<Object> lista = new ArrayList<>();

		String sql = "select * from Manutencao;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {

				Manutencao manutencao = new Manutencao();

				manutencao.setIdManutencao(rs.getInt("idManutencao"));
				manutencao.setTipoManutencao(rs.getString("TipoManutencao"));
				manutencao.setDataManutencao(rs.getDate("DataManutencao"));
				manutencao.setKilometragem(rs.getShort("Kilometragem"));
				manutencao.setValorManutencao(rs.getFloat("ValorManutencao"));
				manutencao.setDescricao(rs.getString("Descricao"));

				lista.add(manutencao);

			}
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return lista;
	}

	@Override
	public int insert(int idFuncionario) {
		String sql = "insert into Manutencao (TipoManutencao, DataManutencao, Kilometragem, ValorManutencao, Descricao) values (?, ?, ?, ?, ?);";
		String[] id = { "idManutencao" };
		try (PreparedStatement stmt = conexao.prepareStatement(sql, id)) {
			stmt.setString(1, manutencao.getTipoManutencao());
			stmt.setDate(2, Date.valueOf(manutencao.getDataManutencao()));
			stmt.setShort(3, manutencao.getKilometragem());
			stmt.setFloat(4, manutencao.getValorManutencao());
			stmt.setString(5, manutencao.getDescricao());

			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();

			while (rs.next()) {
				this.manutencao.setIdManutencao(rs.getInt(1));
				ProceduresDAO.registraLog(idFuncionario, "Manutencao", "INSERT", rs.getInt(1));
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.manutencao.getIdManutencao();
	}

	@Override
	public int update(int idFuncionario) {
		String sql = "update Manutencao set TipoManutencao = ?, DataManutencao = ?, Kilometragem = ?, "
				+ "ValorManutencao = ?, Descricao = ? where idManutencao = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setString(1, manutencao.getTipoManutencao());
			stmt.setDate(2, Date.valueOf(manutencao.getDataManutencao()));
			stmt.setShort(3, manutencao.getKilometragem());
			stmt.setFloat(4, manutencao.getValorManutencao());
			stmt.setString(5, manutencao.getDescricao());
			stmt.setInt(6, manutencao.getIdManutencao());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Manutencao", "UPDATE", manutencao.getIdManutencao());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.manutencao.getIdManutencao();

	}

	@Override
	public void delete(int idFuncionario) {
		String sql = "delete from Manutencao where idManutencao = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, manutencao.getIdManutencao());

			stmt.execute();
			ProceduresDAO.registraLog(idFuncionario, "Manutencao", "DELETE", manutencao.getIdManutencao());
			
			stmt.closeOnCompletion();
			conexao.close();

		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

	}

	public Manutencao selectManutencao(int id) {
		String sql = "select * from Manutencao where idManutencao = ?;";

		try (PreparedStatement stmt = conexao.prepareStatement(sql)) {
			stmt.setInt(1, id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				
				Manutencao manutencao = new Manutencao();

				manutencao.setIdManutencao(rs.getInt("idManutencao"));
				manutencao.setTipoManutencao(rs.getString("TipoManutencao"));
				manutencao.setDataManutencao(rs.getDate("DataManutencao"));
				manutencao.setKilometragem(rs.getShort("Kilometragem"));
				manutencao.setValorManutencao(rs.getFloat("ValorManutencao"));
				manutencao.setDescricao(rs.getString("Descricao"));

				this.setManutencao(manutencao);
			}
			
			stmt.closeOnCompletion();
			rs.close();
			conexao.close();
			
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}

		return this.manutencao;
	}

	public Manutencao getManutencao() {
		return manutencao;
	}

	public void setManutencao(Manutencao manutencao) {
		this.manutencao = manutencao;
	}
}
