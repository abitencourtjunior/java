package br.com.altarent.modelo;

public class Marca {
	private int idMarca;
	private String nome;
	
	
	public Marca() {}
	
	public Marca(String nome){
		this.nome = nome;
	}

	public int getIdMarca() {
		return this.idMarca;
	}

	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
