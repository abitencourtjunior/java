package br.com.altarent.modelo;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

public class Registro {
	private int idRegistro;
	private LocalDate dataModificacao = LocalDate.now();
	private LocalTime hora = LocalTime.now();
	private int idFuncionario;
	private String nomeTabela;
	private String tipoDeRegistro;
	private int idDaTupla;
	
	public Registro() {}
	
	public Registro(int idFuncionario, String nomeTabela, String TipoDeRegistro, int idDaTupla) {
		this.idFuncionario = idFuncionario;
		this.nomeTabela = nomeTabela;
		this.tipoDeRegistro = TipoDeRegistro;
		this.idDaTupla = idDaTupla;
	}

	public int getIdRegistro() {
		return idRegistro;
	}

	public void setIdRegistro(int idRegistro) {
		this.idRegistro = idRegistro;
	}

	public LocalDate getDataModificacao() {
		return dataModificacao;
	}

	public void setDataModificacao(LocalDate dataModificacao) {
		this.dataModificacao = dataModificacao;
	}
	
	public void setDataModificacao(Date dataModificacao) {
		this.dataModificacao = dataModificacao.toLocalDate();
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}
	
	public void setHora(Time hora) {
		this.hora = hora.toLocalTime();
	}

	public int getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(int idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getNomeTabela() {
		return nomeTabela;
	}

	public void setNomeTabela(String nomeTabela) {
		this.nomeTabela = nomeTabela;
	}

	public String getTipoDeRegistro() {
		return tipoDeRegistro;
	}

	public void setTipoDeRegistro(String tipoDeRegistro) {
		this.tipoDeRegistro = tipoDeRegistro;
	}

	public int getIdDaTupla() {
		return idDaTupla;
	}

	public void setIdDaTupla(int idDaTupla) {
		this.idDaTupla = idDaTupla;
	}
}
