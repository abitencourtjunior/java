package br.com.altarent.modelo;

public class Venda {
	private int idVenda;
	private Produto produto;
	private Funcionario funcionario;
	private Cliente cliente;
	private Loja loja;
	
	public Venda() {}
	
	public Venda(Produto produto, Funcionario funcionario, Cliente cliente, Loja loja) {
		this.produto = produto;
		this.funcionario = funcionario;
		this.cliente = cliente;
		this.loja = loja;
	}

	public int getIdVenda() {
		return idVenda;
	}

	public void setIdVenda(int idVenda) {
		this.idVenda = idVenda;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Loja getLoja() {
		return loja;
	}

	public void setLoja(Loja loja) {
		this.loja = loja;
	}	
}