package br.com.altarent.modelo;
import java.sql.Date;
import java.time.LocalDate;

public class Manutencao {
	private int idManutencao;
	private String tipoManutencao;
	private LocalDate dataManutencao;
	private short kilometragem;
	private float valorManutencao;
	private String descricao;
	
	public Manutencao() {}
	
	public Manutencao(String tipoManutencao, LocalDate data, short kilometragem, float valorManutencao, String descricao){
		this.tipoManutencao = tipoManutencao;
		this.dataManutencao = data;
		this.kilometragem = kilometragem;
		this.valorManutencao = valorManutencao;
		this.descricao = descricao;
	}

	public int getIdManutencao() {
		return this.idManutencao;
	}

	public void setIdManutencao(int idManutencao) {
		this.idManutencao = idManutencao;
	}

	public String getTipoManutencao() {
		return this.tipoManutencao;
	}

	public void setTipoManutencao(String tipoManutencao) {
		this.tipoManutencao = tipoManutencao;
	}

	public LocalDate getDataManutencao() {
		return this.dataManutencao;
	}

	public void setDataManutencao(LocalDate data) {
		this.dataManutencao = data;
	}
	
	public void setDataManutencao(Date data) {
		this.dataManutencao = data.toLocalDate();
	}

	public short getKilometragem() {
		return this.kilometragem;
	}

	public void setKilometragem(short kilometragem) {
		this.kilometragem = kilometragem;
	}

	public float getValorManutencao() {
		return this.valorManutencao;
	}

	public void setValorManutencao(float valorManutencao) {
		this.valorManutencao = valorManutencao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
