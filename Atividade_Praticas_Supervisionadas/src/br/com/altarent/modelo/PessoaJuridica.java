package br.com.altarent.modelo;

public class PessoaJuridica extends Pessoa {

	private int idPessoaJuridica;
	private String razaoSocial;
	private String cnpj;
	private PessoaFisica responsavel;
	private boolean seguradora;

	public PessoaJuridica() {
	}

	public PessoaJuridica(String nome, Contato contato, Endereco endereco, String razaoSocial, String cnpj,
			PessoaFisica responsavel, boolean seguradora) {
		super(nome, contato, endereco);
		this.razaoSocial = razaoSocial;
		this.cnpj = cnpj;
		this.responsavel = responsavel;
		this.seguradora = seguradora;
	}

	public int getIdPessoaJuridica() {
		return idPessoaJuridica;
	}

	public void setIdPessoaJuridica(int idPessoaJuridica) {
		this.idPessoaJuridica = idPessoaJuridica;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public PessoaFisica getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(PessoaFisica responsavel) {
		this.responsavel = responsavel;
	}

	public boolean isSeguradora() {
		return seguradora;
	}

	public void setSeguradora(boolean seguradora) {
		this.seguradora = seguradora;
	}
}
