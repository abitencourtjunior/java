package br.com.altarent.modelo;

public class Carro {
	private int idCarro;
	private ModeloTerrestre modeloTerrestre;
	private String chassi;
	private int dataFabricacao;
	private String placa;
	private String cor;
	private EstadoDeUso estadoDeUso;
	private Seguro seguro;
	private short kilometragem;
	private Manutencao manutencao;
	private double valorAluguel;
	private String renavam;
	private double valorDeCompra;
	private double valorAtual;
	private double valorGerado;

	public Carro() {

	}

	public Carro(ModeloTerrestre modeloTerrestre, String chassi, int data, String placa, String cor, EstadoDeUso estadoDeUso, Seguro seguro,
			short kilometragem, Manutencao manutencao, double valorAluguel, String renavam, double valorCompra,
			double valorAtual, double valorGerado) {
		this.modeloTerrestre = modeloTerrestre;
		this.chassi = chassi;
		this.dataFabricacao = data;
		this.placa = placa;
		this.cor = cor;
		this.estadoDeUso = estadoDeUso;
		this.seguro = seguro;
		this.kilometragem = kilometragem;
		this.manutencao = manutencao;
		this.valorAluguel = valorAluguel;
		this.renavam = renavam;
		this.valorDeCompra = valorCompra;
		this.valorAtual = valorAtual;
		this.valorGerado = valorGerado;
	}

	public int getIdCarro() {
		return this.idCarro;
	}

	public void setIdCarro(int idCarro) {
		this.idCarro = idCarro;
	}

	public String getChassi() {
		return this.chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public int getDataFabricacao() {
		return this.dataFabricacao;
	}

	public void setDataFabricacao(int dataFabricacao) {
		this.dataFabricacao = dataFabricacao;
	}

	public EstadoDeUso getEstadoDeUso() {
		return estadoDeUso;
	}

	public void setEstadoDeUso(EstadoDeUso estadoDeUso) {
		this.estadoDeUso = estadoDeUso;
	}

	public Seguro getSeguro() {
		return seguro;
	}

	public void setSeguro(Seguro seguro) {
		this.seguro = seguro;
		setValorGerado(-seguro.getValor());
	}

	public Manutencao getManutencao() {
		return manutencao;
	}

	public void setManutencao(Manutencao manutencao) {
		this.manutencao = manutencao;
		setValorGerado(-manutencao.getValorManutencao());
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCor() {
		return this.cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public short getKilometragem() {
		return this.kilometragem;
	}

	public void setKilometragem(short kilometragem) {
		this.kilometragem = kilometragem;
	}

	public double getValorAluguel() {
		return this.valorAluguel;
	}

	public void setValorAluguel(double valorAluguel) {
		this.valorAluguel = valorAluguel;
	}

	public String getRenavam() {
		return this.renavam;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public double getValorDeCompra() {
		return this.valorDeCompra;
	}

	public void setValorDeCompra(double valorDeCompra) {
		this.valorDeCompra = valorDeCompra;
	}

	public double getValorAtual() {
		return this.valorAtual;
	}

	public void setValorAtual(double valorAtual) {
		this.valorAtual = valorAtual;
	}

	public double getValorGerado() {
		return this.valorGerado;
	}

	public void setValorGerado(double valorGerado) {
		this.valorGerado += valorGerado;
	}

	public ModeloTerrestre getModeloTerrestre() {
		return modeloTerrestre;
	}

	public void setModeloTerrestre(ModeloTerrestre modeloTerrestre) {
		this.modeloTerrestre = modeloTerrestre;
	}
}