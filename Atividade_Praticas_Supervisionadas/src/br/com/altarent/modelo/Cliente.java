package br.com.altarent.modelo;

public class Cliente {

	private int idCliente;
	private PessoaFisica pessoaFisica;
	private PessoaJuridica pessoaJuridica;
	private Autenticacao autenticacao;
	private Promocional codigo;

	public Cliente() {
	}

	public Cliente(PessoaFisica pessoaFisica, PessoaJuridica pessoaJuridica, Autenticacao autenticacao,
			Promocional codigo) {
		this.pessoaFisica = pessoaFisica;
		this.pessoaJuridica = pessoaJuridica;
		this.autenticacao = autenticacao;
		this.codigo = codigo;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public Autenticacao getAutenticacao() {
		return autenticacao;
	}

	public void setAutenticacao(Autenticacao autenticacao) {
		this.autenticacao = autenticacao;
	}

	public Promocional getCodigo() {
		return codigo;
	}

	public void setCodigo(Promocional codigo) {
		this.codigo = codigo;
	}
}
