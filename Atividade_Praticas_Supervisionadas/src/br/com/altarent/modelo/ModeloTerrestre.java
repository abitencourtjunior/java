package br.com.altarent.modelo;

public class ModeloTerrestre {
	private int idModeloTerrestre;
	private String nomeModelo;
	private String cambio;
	private boolean arCondicionado;
	private String direcao;
	private boolean vidrosEletricos;
	private byte portas;
	private boolean travaEletrica;
	private boolean airBag;
	private boolean abs;
	private byte capacidadePessoas;
	private short litragemPortaMala;
	private Marca marca;

	public ModeloTerrestre() {}

	public ModeloTerrestre(String nomeModelo, String cambio, boolean ar, String direcao, boolean vidros, byte portas,
			boolean trava, boolean airBag, boolean abs, byte capacidadePessoas, short litragem, Marca marca) {
		this.nomeModelo = nomeModelo;
		this.cambio = cambio;
		this.arCondicionado = ar;
		this.direcao = direcao;
		this.vidrosEletricos = vidros;
		this.portas = portas;
		this.travaEletrica = trava;
		this.airBag = airBag;
		this.abs = abs;
		this.capacidadePessoas = capacidadePessoas;
		this.litragemPortaMala = litragem;
		this.marca = marca;
	}

	public int getIdModeloTerrestre() {
		return this.idModeloTerrestre;
	}

	public void setIdModeloTerrestre(int idModeloTerrestre) {
		this.idModeloTerrestre = idModeloTerrestre;
	}

	public String getNomeModelo() {
		return this.nomeModelo;
	}

	public void setNomeModelo(String nomeModelo) {
		this.nomeModelo = nomeModelo;
	}

	public String getCambio() {
		return this.cambio;
	}

	public void setCambio(String cambio) {
		this.cambio = cambio;
	}

	public boolean isArCondicionado() {
		return this.arCondicionado;
	}

	public void setArCondicionado(boolean arCondicionado) {
		this.arCondicionado = arCondicionado;
	}

	public String getDirecao() {
		return this.direcao;
	}

	public void setDirecao(String direcao) {
		this.direcao = direcao;
	}

	public boolean isVidrosEletricos() {
		return this.vidrosEletricos;
	}

	public void setVidrosEletricos(boolean vidrosEletricos) {
		this.vidrosEletricos = vidrosEletricos;
	}

	public byte getPortas() {
		return this.portas;
	}

	public void setPortas(byte portas) {
		this.portas = portas;
	}

	public boolean isTravaEletrica() {
		return this.travaEletrica;
	}

	public void setTravaEletrica(boolean travaEletrica) {
		this.travaEletrica = travaEletrica;
	}

	public boolean isAirBag() {
		return this.airBag;
	}

	public void setAirBag(boolean airBag) {
		this.airBag = airBag;
	}

	public boolean isAbs() {
		return this.abs;
	}

	public void setAbs(boolean abs) {
		this.abs = abs;
	}

	public byte getCapacidadePessoas() {
		return this.capacidadePessoas;
	}

	public void setCapacidadePessoas(byte capacidadePessoas) {
		this.capacidadePessoas = capacidadePessoas;
	}

	public short getLitragemPortaMala() {
		return this.litragemPortaMala;
	}

	public void setLitragemPortaMala(short litragemPortaMala) {
		this.litragemPortaMala = litragemPortaMala;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}
}