package br.com.altarent.modelo;


public abstract class Pessoa {
    
    private String nome;
    private Contato contato;
    private Endereco endereco;
    
    public Pessoa(){}
    
    public Pessoa(String nome, Contato contato, Endereco endereco) {
	   this.nome = nome;
	   this.contato = contato;
	   this.endereco = endereco;
    }

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}
