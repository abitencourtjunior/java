package br.com.altarent.modelo;

import java.sql.Date;
import java.time.LocalDate;

public class Autenticacao {
    
    private int idAutenticacao;
    private String usuario;
    private String senha;
    private LocalDate dataCriacao;
    private String tipoUsuario;
    
    public Autenticacao(){
        
    }
    
    public Autenticacao(String usuario, String senha, LocalDate dataCriacao, String tipoUsuario){
        this.usuario = usuario;
        this.senha = senha;
        this.dataCriacao = dataCriacao;
        this.tipoUsuario = tipoUsuario;
    }

   
    public int getIdAutenticacao() {
        return idAutenticacao;
    }

    
    public void setIdAutenticacao(int idAutenticacao) {
        this.idAutenticacao = idAutenticacao;
    }

    
    public String getUsuario() {
        return usuario;
    }

    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

   
    public String getSenha() {
        return senha;
    }

    
    public void setSenha(String senha) {
        this.senha = senha;
    }

  
    public LocalDate getDataCriacao() {
        return dataCriacao;
    }

    
    public void setDataCriacao(LocalDate dataCriacao) {
        this.dataCriacao = dataCriacao;
    }
    
    public void setDataCriacao(Date dataCriacao) {
    	this.dataCriacao = dataCriacao.toLocalDate();
    }

    
    public String getTipoUsuario() {
        return tipoUsuario;
    }

    
    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }   
}
