package br.com.altarent.modelo;

public class TipoDeContrato {
	private int idTipoDeContrato;
	private Diaria diaria;
	private Semanal semanal;
	private Mensal mensal;
	private double valor;
	
	public TipoDeContrato(){}
	
	public TipoDeContrato(Diaria diaria, Semanal semanal, Mensal mensal){
		this.diaria = diaria;
		this.semanal = semanal;
		this.mensal = mensal;
	}
	
	public int getIdTipoDeContrato() {
		return this.idTipoDeContrato;
	}
	
	public void setIdTipoDeContrato(int idTipoDeContrato) {
		this.idTipoDeContrato = idTipoDeContrato;
	}

	public Diaria getDiaria() {
		return diaria;
	}

	public void setDiaria(Diaria diaria) {
		this.diaria = diaria;
	}

	public Semanal getSemanal() {
		return semanal;
	}

	public void setSemanal(Semanal semanal) {
		this.semanal = semanal;
	}

	public Mensal getMensal() {
		return mensal;
	}

	public void setMensal(Mensal mensal) {
		this.mensal = mensal;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public void setValor(Diaria diaria) {
		this.valor = diaria.getPrecoFinal();
	}
	
	public void setValor(Semanal semanal) {
		this.valor = semanal.getPrecoFinal();
	}
	
	public void setValor(Mensal mensal) {
		this.valor = mensal.getPrecoFinal();
	}	
}