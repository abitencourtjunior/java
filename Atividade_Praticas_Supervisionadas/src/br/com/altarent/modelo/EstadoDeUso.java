package br.com.altarent.modelo;

public class EstadoDeUso {
	private int idEstadoDeUso;
	private String estadoDeUso;
	
	public EstadoDeUso(){}
	
	public EstadoDeUso(String estado){
		this.estadoDeUso = estado;
	}

	public int getIdEstadoDeUso() {
		return this.idEstadoDeUso;
	}

	public void setIdEstadoDeUso(int idEstadoDeUso) {
		this.idEstadoDeUso = idEstadoDeUso;
	}

	public String getEstadoDeUso() {
		return this.estadoDeUso;
	}

	public void setEstadoDeUso(String estadoDeUso) {
		this.estadoDeUso = estadoDeUso;
	}
	
	
}