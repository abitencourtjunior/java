package br.com.altarent.modelo;

public class Jetski {
	private int idJetski;
	private ModeloAquatico modeloAquatico;
	private int documentoMarinha;
	private String cor;
	private EstadoDeUso estadoDeUso;
	private Seguro seguro;
	private Manutencao manutencao;
	private double valorAluguel;
	private int dataFabricacao;
	private double valorDeCompra;
	private double valorAtual;
	private double totalGerado;

	public Jetski() {

	}

	public Jetski(ModeloAquatico modeloAquatico, int documento, String cor, EstadoDeUso estado, Seguro seguro, Manutencao manutencao,
			double valorAluguel, int data, double valorCompra, double valorAtual, double totalGerado) {
		this.modeloAquatico = modeloAquatico;
		this.documentoMarinha = documento;
		this.cor = cor;
		this.estadoDeUso = estado;
		this.seguro = seguro;
		this.manutencao = manutencao;
		this.valorAluguel = valorAluguel;
		this.dataFabricacao = data;
		this.valorDeCompra = valorCompra;
		this.valorAtual = valorAtual;
		this.totalGerado = totalGerado;
	}

	public int getIdJetski() {
		return this.idJetski;
	}

	public void setIdJetski(int idJetski) {
		this.idJetski = idJetski;
	}

	public int getDocumentoMarinha() {
		return this.documentoMarinha;
	}

	public void setDocumentoMarinha(int documentoMarinha) {
		this.documentoMarinha = documentoMarinha;
	}

	public String getCor() {
		return this.cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public double getValorAluguel() {
		return this.valorAluguel;
	}

	public void setValorAluguel(double valorAluguel) {
		this.valorAluguel = valorAluguel;
	}

	public int getDataFabricacao() {
		return this.dataFabricacao;
	}

	public void setDataFabricacao(int dataFabricacao) {
		this.dataFabricacao = dataFabricacao;
	}

	public double getValorDeCompra() {
		return this.valorDeCompra;
	}

	public void setValorDeCompra(double valorDeCompra) {
		this.valorDeCompra = valorDeCompra;
	}

	public double getValorAtual() {
		return this.valorAtual;
	}

	public void setValorAtual(double valorAtual) {
		this.valorAtual = valorAtual;
	}

	public double getTotalGerado() {
		return this.totalGerado;
	}

	public void setTotalGerado(double totalGerado) {
		this.totalGerado += totalGerado;
	}

	public EstadoDeUso getEstadoDeUso() {
		return estadoDeUso;
	}

	public void setEstadoDeUso(EstadoDeUso estadoDeUso) {
		this.estadoDeUso = estadoDeUso;
	}

	public Seguro getSeguro() {
		return seguro;
	}

	public void setSeguro(Seguro seguro) {
		this.seguro = seguro;
		setTotalGerado(-seguro.getValor());
	}

	public Manutencao getManutencao() {
		return manutencao;
	}

	public void setManutencao(Manutencao manutencao) {
		this.manutencao = manutencao;
		setTotalGerado(-manutencao.getValorManutencao());
	}

	public ModeloAquatico getModeloAquatico() {
		return modeloAquatico;
	}

	public void setModeloAquatico(ModeloAquatico modeloAquatico) {
		this.modeloAquatico = modeloAquatico;
	}
}