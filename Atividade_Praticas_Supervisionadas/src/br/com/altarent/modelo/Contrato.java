package br.com.altarent.modelo;

public class Contrato {
	private int idContrato;
	private Orcamento orcamento;
	private Venda venda;
	
	public Contrato() {}
	
	public Contrato(Orcamento orcamento, Venda venda) {
		this.orcamento = orcamento;
		this.venda = venda;
	}

	public int getIdContrato() {
		return idContrato;
	}

	public void setIdContrato(int idContrato) {
		this.idContrato = idContrato;
	}

	public Orcamento getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	
}
