package br.com.altarent.modelo;

public class Seguro {
	private int idSeguro;
	private float valor;
	private PessoaJuridica pessoaJuridica;
	
	public Seguro(){}
	
	public Seguro(float valor, PessoaJuridica pessoaJuridica){
		this.valor = valor;
		this.pessoaJuridica = pessoaJuridica;
	}

	public int getIdSeguro() {
		return this.idSeguro;
	}

	public void setIdSeguro(int idSeguro) {
		this.idSeguro = idSeguro;
	}

	public float getValor() {
		return this.valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}
}