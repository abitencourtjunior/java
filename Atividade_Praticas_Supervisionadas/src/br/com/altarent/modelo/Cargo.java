package br.com.altarent.modelo;

public class Cargo {
    
    private int idCargo;
    private String cargo;
    private double salario;
    
    public Cargo() {}
    
    public Cargo(String cargo, float salario){
        this.cargo = cargo;
        this.salario = salario;
    }

    public int getIdCargo() {
        return idCargo;
    }

    
    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    
    public String getCargo() {
        return cargo;
    }

    
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    
    public double getSalario() {
        return salario;
    }

    
    public void setSalario(double salario) {
        this.salario = salario;
    }

}
