package br.com.altarent.modelo;

public class Disponivel {
	private int idDisponivel;
	private String disponivel;
	
	public Disponivel() {}
	
	public Disponivel(String disponivel) {
		this.disponivel = disponivel;
	}

	public String getDisponivel() {
		return this.disponivel;
	}

	public void setDisponivel(String disponivel) {
		this.disponivel = disponivel;
	}

	public int getId() {
		return idDisponivel;
	}

	public void setId(int id) {
		this.idDisponivel = id;
	}
}
