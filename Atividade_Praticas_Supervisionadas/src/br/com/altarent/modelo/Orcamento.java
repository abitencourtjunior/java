package br.com.altarent.modelo;
import java.sql.Date;
import java.time.LocalDate;

public class Orcamento {
	private int idOrcamento;
	private LocalDate dataEntrega;
	private LocalDate dataRetorno;
	private Produto produto;
	private TipoDeContrato tipoDeContrato;
	private Loja loja;
	private Funcionario funcionario;
	private Cliente cliente;
	
	public Orcamento() {}
	
	public Orcamento(LocalDate dataEntrega, LocalDate dataRetorno, Produto produto, TipoDeContrato tipoDeContrato,
			Loja loja, Funcionario funcionario, Cliente cliente) {
		this.dataEntrega = dataEntrega;
		this.dataRetorno = dataRetorno;
		this.produto = produto;
		this.tipoDeContrato = tipoDeContrato;
		this.loja = loja;
		this.funcionario = funcionario;
		this.cliente = cliente;
	}

	
	public int getIdOrcamento() {
		return idOrcamento;
	}

	public void setIdOrcamento(int idOrcamento) {
		this.idOrcamento = idOrcamento;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public TipoDeContrato getTipoDeContrato() {
		return tipoDeContrato;
	}

	public void setTipoDeContrato(TipoDeContrato tipoDeContrato) {
		this.tipoDeContrato = tipoDeContrato;
	}

	public Loja getLoja() {
		return loja;
	}

	public void setLoja(Loja loja) {
		this.loja = loja;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public LocalDate getDataEntrega() {
		return dataEntrega;
	}


	public void setDataEntrega(LocalDate dataEntrega) {
		this.dataEntrega = dataEntrega;
	}
	
	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega.toLocalDate();
	}

	public LocalDate getDataRetorno() {
		return dataRetorno;
	}
	
	public void setDataRetorno(Date dataRetorno) {
		this.dataRetorno = dataRetorno.toLocalDate();
	}

	public void setDataRetorno(LocalDate dataRetorno) {
		this.dataRetorno = dataRetorno;
	}	
}