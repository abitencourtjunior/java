package br.com.altarent.modelo;

import java.sql.Date;
import java.time.LocalDate;

public class Promocional {
	private int idPromocional;
	private String hash;
	private boolean usado;
	private LocalDate validade;
	
	public Promocional() {}
	
	public Promocional(String hash, boolean usado, LocalDate validade) {
		this.hash = hash;
		this.usado = usado;
		this.validade = validade;
	}

	public int getIdPromocional() {
		return idPromocional;
	}

	public void setIdPromocional(int idPromocional) {
		this.idPromocional = idPromocional;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public boolean isUsado() {
		return usado;
	}

	public void setUsado(boolean usado) {
		this.usado = usado;
	}

	public LocalDate getValidade() {
		return validade;
	}

	public void setValidade(LocalDate validade) {
		this.validade = validade;
	}
	
	public void setValidade(Date validade) {
		this.validade = validade.toLocalDate();
	}
}
