package br.com.altarent.modelo;

import java.sql.Date;
import java.time.LocalDate;

public class PessoaFisica extends Pessoa {
	private int idPessoaFisica;
	private LocalDate dataNascimento;
	private String cpf;
	private String cnh;
	private String cha;
	private String categoriaHabilitacao;
	private String sexo;

	public PessoaFisica() {
		super();
	}

	public PessoaFisica(String nome, Contato contato, Endereco endereco, String sexo, LocalDate dataNascimento, String cpf,
			String cnh, String cha, String categoria) {
		super(nome, contato, endereco);
		this.sexo = sexo;
		this.dataNascimento = dataNascimento;
		this.cpf = cpf;
		this.cnh = cnh;
		this.cha = cha;
		this.categoriaHabilitacao = categoria;
	}

	public int getIdPessoaFisica() {
		return idPessoaFisica;
	}

	public void setIdPessoaFisica(int idPessoaFisica) {
		this.idPessoaFisica = idPessoaFisica;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento.toLocalDate();
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	public String getCha() {
		return cha;
	}

	public void setCha(String cha) {
		this.cha = cha;
	}

	public String getCategoriaHabilitacao() {
		return categoriaHabilitacao;
	}

	public void setCategoriaHabilitacao(String categoriaHabilitacao) {
		this.categoriaHabilitacao = categoriaHabilitacao;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public int getIdade() {
		LocalDate sistema = LocalDate.now();
		int idade = sistema.getYear() - this.dataNascimento.getYear();
		if(this.dataNascimento.getDayOfYear() > sistema.getDayOfYear()) {
			idade--;
		}
		return idade;
	}
}