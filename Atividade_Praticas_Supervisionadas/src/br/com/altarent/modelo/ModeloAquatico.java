package br.com.altarent.modelo;

public class ModeloAquatico {
	private int idModeloAquatico;
	private String nomeModelo;
	private short potenciaMotor;
	private byte tipoMotor;
	private byte lugares;
	private String tipoDeConducao;
	private String tipoDeIgnicao;
	private String tipoDeAgua;
	private short volumeTanque;
	private Marca marca;

	public ModeloAquatico() {}

	public ModeloAquatico(String nome, short potencia, byte tipoMotor, byte lugares, String tipoConducao,
			String tipoIgnicao, String tipoAgua, short volumeTanque, Marca marca) {
		this.nomeModelo = nome;
		this.potenciaMotor = potencia;
		this.tipoMotor = tipoMotor;
		this.lugares = lugares;
		this.tipoDeConducao = tipoConducao;
		this.tipoDeIgnicao = tipoIgnicao;
		this.tipoDeAgua = tipoAgua;
		this.volumeTanque = volumeTanque;
		this.marca = marca;
	}

	public int getIdModeloAquatico() {
		return this.idModeloAquatico;
	}

	public void setIdModeloAquatico(int idModeloAquatico) {
		this.idModeloAquatico = idModeloAquatico;
	}

	public String getNomeModelo() {
		return this.nomeModelo;
	}

	public void setNomeModelo(String nomeModelo) {
		this.nomeModelo = nomeModelo;
	}

	public short getPotenciaMotor() {
		return this.potenciaMotor;
	}

	public void setPotenciaMotor(short potenciaMotor) {
		this.potenciaMotor = potenciaMotor;
	}

	public byte getTipoMotor() {
		return this.tipoMotor;
	}

	public void setTipoMotor(byte tipoMotor) {
		this.tipoMotor = tipoMotor;
	}

	public byte getLugares() {
		return this.lugares;
	}

	public void setLugares(byte lugares) {
		this.lugares = lugares;
	}

	public String getTipoDeConducao() {
		return this.tipoDeConducao;
	}

	public void setTipoDeConducao(String tipoDeConducao) {
		this.tipoDeConducao = tipoDeConducao;
	}

	public String getTipoDeIgnicao() {
		return this.tipoDeIgnicao;
	}

	public void setTipoDeIgnicao(String tipoDeIgnicao) {
		this.tipoDeIgnicao = tipoDeIgnicao;
	}

	public String getTipoDeAgua() {
		return this.tipoDeAgua;
	}

	public void setTipoDeAgua(String tipoDeAgua) {
		this.tipoDeAgua = tipoDeAgua;
	}

	public short getVolumeTanque() {
		return this.volumeTanque;
	}

	public void setVolumeTanque(short volumeTanque) {
		this.volumeTanque = volumeTanque;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}
}