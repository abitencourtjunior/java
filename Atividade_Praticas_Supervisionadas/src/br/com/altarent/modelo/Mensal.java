package br.com.altarent.modelo;

public class Mensal extends TempoDeContrato{
	private static final double DESCONTO = 0.2;
	private double precoFinal = 0;
	
	public Mensal(){}
	
	public Mensal(byte quantidade, double valorProduto){
		super(quantidade, valorProduto);
	}

	@Override
	public void calculaValorCarro() {
		double precoAluguel = Mensal.this.getValorProduto() * Mensal.this.getQuantidade();
		double calculo = precoAluguel - (precoAluguel * DESCONTO);
		this.precoFinal = calculo;
	}

	@Override
	public void calculaValorJetski() {
		double precoAluguel = Mensal.this.getValorProduto() * Mensal.this.getQuantidade();
		double calculo = precoAluguel - (precoAluguel * DESCONTO);
		this.precoFinal = calculo;	
	}

	public double getPrecoFinal(String string) {
		if(string.equals("Carro")) {
			this.calculaValorCarro();
		} 
		else {
			this.calculaValorJetski();
		}
		
		return this.precoFinal;
	}

	public void setPrecoFinal(double precoFinal) {
		this.precoFinal = precoFinal;
	}

	public double getPrecoFinal() {
		return precoFinal;
	}

	public double getDESCONTO() {
		return DESCONTO;
	}
		
}