package br.com.altarent.modelo;

import java.sql.Date;
import java.time.LocalDate;

public class Funcionario extends Pessoa {

	private int idFuncionario;
	private LocalDate dataNascimento;
	private String cpf;
	private Cargo cargo;
	private Autenticacao autenticacao;
	private String sexo;

	public Funcionario() {}

	public Funcionario(String nome, Contato contato, Endereco endereco, String sexo, LocalDate dataNascimento, String cpf,
			Cargo cargo, Autenticacao autenticacao) {
		super(nome, contato, endereco);
		this.sexo = sexo;
		this.dataNascimento = dataNascimento;
		this.cpf = cpf;
		this.cargo = cargo;
		this.autenticacao = autenticacao;
	}

	public int getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(int idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento.toLocalDate();
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Autenticacao getAutenticacao() {
		return autenticacao;
	}

	public void setAutenticacao(Autenticacao autenticacao) {
		this.autenticacao = autenticacao;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public int getIdade() {
		LocalDate sistema = LocalDate.now();
		int idade = sistema.getYear() - this.dataNascimento.getYear();
		if(this.dataNascimento.getDayOfYear() > sistema.getDayOfYear()) {
			idade--;
		}
		return idade;
	}
}