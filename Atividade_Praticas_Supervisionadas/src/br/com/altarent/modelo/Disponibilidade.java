package br.com.altarent.modelo;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

public class Disponibilidade {
	private int idDisponibilidade;
	private Disponivel disponivel;
	private LocalDate data;
	private LocalTime hora;
	
	public Disponibilidade(){
		
	}
	
	public Disponibilidade(Disponivel disponibilidade, LocalDate data, LocalTime hora){
		this.disponivel = disponibilidade;
		this.data = data;
		this.hora = hora;
	}

	public int getIdDisponibilidade() {
		return this.idDisponibilidade;
	}

	public void setIdDisponibilidade(int idDisponibilidade) {
		this.idDisponibilidade = idDisponibilidade;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}
	
	public void setData(Date data) {
		this.data = data.toLocalDate();
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}
	
	public void setHora(Time hora) {
		this.hora = hora.toLocalTime();
	}

	public Disponivel getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(Disponivel disponibilidade) {
		this.disponivel = disponibilidade;
	}
}
