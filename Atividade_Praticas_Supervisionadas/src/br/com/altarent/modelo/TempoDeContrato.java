package br.com.altarent.modelo;

public abstract class TempoDeContrato {
	private int id;
	private byte quantidade;
	private double valorProduto;

	public TempoDeContrato(){}
	
	public TempoDeContrato(byte quantidade, double valorProduto){
		this.quantidade = quantidade;
		this.valorProduto = valorProduto;
	}

	public double getValorProduto() {
		return valorProduto;
	}

	public void setValorProduto(double valorProduto) {
		this.valorProduto = valorProduto;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(byte quantidade) {
		this.quantidade = quantidade;
	}
	
	public abstract void calculaValorCarro();
	
	public abstract void calculaValorJetski();
}