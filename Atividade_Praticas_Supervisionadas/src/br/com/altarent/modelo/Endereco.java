package br.com.altarent.modelo;

public class Endereco {
    
    private int idEndereco;
    private String nomeRua;
    private String cep;
    private String bairro;
    private String cidade;
    private String complemento;
    private int numero;
    
    public Endereco(){}
    
    public Endereco(String nomeRua, String cep, String bairro, String cidade, String complemento, int numero){
        this.nomeRua = nomeRua;
        this.cep = cep;
        this.bairro = bairro;
        this.cidade = cidade;
        this.complemento = complemento;
        this.numero = numero;
    }
    
    public void setIdEndereco(int idEndereco){
        this.idEndereco = idEndereco;
    }
    
    public int getIdEndereco(){
        return idEndereco;
    }
      
    public void setNomeRua(String nomeRua){
        this.nomeRua = nomeRua;
    }
    
    public String getNomeRua(){
        return nomeRua;
    }

    public void setCEP(String cep){
        this.cep = cep;
    }
    
    public String getCep(){
        return cep;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getBairro() {
        return bairro;
    }
    
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCidade() {
        return cidade;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }
      
    public String getComplemento() {
        return complemento;
    }

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
}