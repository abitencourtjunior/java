package br.com.altarent.modelo;

public class Loja {

    private int idLoja;
    private Contato contato;
    private Endereco endereco;
    
    public Loja() {}
    
    public Loja(Contato contato, Endereco endereco) {
        this.contato = contato;
        this.endereco = endereco;
    }

    public int getIdLoja() {
        return idLoja;
    }

    public void setIdLoja(int idLoja) {
        this.idLoja = idLoja;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
