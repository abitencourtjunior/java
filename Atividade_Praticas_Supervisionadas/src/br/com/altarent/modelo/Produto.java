package br.com.altarent.modelo;

public class Produto {
	private int idProduto;
	private Carro carro;
	private Jetski jetski;
	private Disponibilidade disponibilidade;
	
	public Produto(){}
	
	public Produto(Carro carro, Jetski jetski, Disponibilidade disponivel){
		this.carro = carro;
		this.jetski = jetski;
		this.disponibilidade = disponivel;
	}

	public int getIdProduto() {
		return this.idProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public Jetski getJetski() {
		return jetski;
	}

	public void setJetski(Jetski jetski) {
		this.jetski = jetski;
	}

	public Disponibilidade getDisponibilidade() {
		return disponibilidade;
	}

	public void setDisponibilidade(Disponibilidade disponivel) {
		this.disponibilidade = disponivel;
	}
}