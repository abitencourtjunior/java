/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.altarent.grafica;

import br.com.altarent.dao.CarroDAO;
import br.com.altarent.dao.ContratoDAO;
import br.com.altarent.dao.JetskiDAO;
import br.com.altarent.dao.OrcamentoDAO;
import br.com.altarent.dao.PessoaFisicaDAO;
import br.com.altarent.dao.PessoaJuridicaDAO;
import br.com.altarent.dao.RegistroDAO;
import br.com.altarent.modelo.Carro;
import br.com.altarent.modelo.Contrato;
import br.com.altarent.modelo.Jetski;
import br.com.altarent.modelo.Orcamento;
import br.com.altarent.modelo.PessoaFisica;
import br.com.altarent.modelo.PessoaJuridica;
import br.com.altarent.modelo.Registro;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class BuscarDados extends javax.swing.JFrame {

    /**
     * Creates new form BuscarDados
     */
    public BuscarDados() {
        initComponents();
    }
    
    private void ConsultaCarro(){
        
        DefaultTableModel modelo = (DefaultTableModel) jTableBuscar.getModel();
        modelo.setNumRows(0);
        
        List<Carro> carro = new CarroDAO().selectCarro();
        
            carro.forEach(objeto ->{
                
                if(objeto.getIdCarro() > 1){
                    
                modelo.addRow(new Object[]{
                   objeto.getIdCarro(),
                   objeto.getModeloTerrestre().getNomeModelo(),
                   objeto.getChassi(),
                   objeto.getRenavam(),
                   objeto.getDataFabricacao(),
                   objeto.getValorAluguel()
               });  
            
            }       
        });
    }
    private void ConsultaJetski(){
        
        DefaultTableModel modelo = (DefaultTableModel) jTableBuscar.getModel();
        modelo.setNumRows(0);
        List<Jetski> jetski = new JetskiDAO().selectJetski();
                    
            jetski.forEach(objeto ->{
                
                if(objeto.getIdJetski()> 1){
                    
                modelo.addRow(new Object[]{
                   objeto.getIdJetski(),
                   objeto.getModeloAquatico().getNomeModelo(),
                   objeto.getDocumentoMarinha(),
                   objeto.getModeloAquatico().getTipoDeConducao(),
                   objeto.getDataFabricacao(),
                   objeto.getValorAluguel()
               });  
            
            }       
        });
    }
    private void ConsultaPessoaFisica(){
        
        DefaultTableModel modelo = (DefaultTableModel) jTableBuscar.getModel();
        modelo.setNumRows(0);
        
        List<PessoaFisica> pf = new PessoaFisicaDAO().selectPessoaFisica();
                    
            pf.forEach(objeto ->{
                
                if(objeto.getIdPessoaFisica() > 1){
                    
                modelo.addRow(new Object[]{
                   objeto.getIdPessoaFisica(),
                   objeto.getNome(),
                   objeto.getCpf(),
                   objeto.getEndereco().getIdEndereco(),
                   objeto.getContato().getTelefonePrincipal(),
                   objeto.getContato().getEmail()
               });  
            
            }       
        });
    }
    private void ConsultaPessoaJuridica(){
        
        DefaultTableModel modelo = (DefaultTableModel) jTableBuscar.getModel();
        modelo.setNumRows(0);
        
        List<PessoaJuridica> pj = new PessoaJuridicaDAO().selectPessoaJuridica();
                    
            pj.forEach(objeto ->{
                
                if(objeto.getIdPessoaJuridica()> 1){
                    
                modelo.addRow(new Object[]{
                   objeto.getIdPessoaJuridica(),
                   objeto.getNome(),
                   objeto.getCnpj(),
                   objeto.getEndereco().getIdEndereco(),
                   objeto.getContato().getTelefonePrincipal(),
                   objeto.getContato().getEmail()
               });  
            
            }       
        });
    }
    private void ConsultaOrcamentos(){
        
        DefaultTableModel modelo = (DefaultTableModel) jTableBuscar.getModel();
        modelo.setNumRows(0);
        
        List<Orcamento> orcamentos = new OrcamentoDAO().selectOrcamento();
                    
            orcamentos.forEach(objeto ->{
                
                if(objeto.getIdOrcamento()> 1){
                    
                modelo.addRow(new Object[]{
                   objeto.getIdOrcamento(),
                   objeto.getCliente().getPessoaFisica().getCpf(),
                   objeto.getDataEntrega(),
                   objeto.getDataRetorno(),
                   objeto.getTipoDeContrato(),
                   objeto.getTipoDeContrato().getValor()
                });  
            }       
        });
    }
    private void ConsultaContratos(){
        
        
        DefaultTableModel modelo = (DefaultTableModel) jTableBuscar.getModel();
        modelo.setNumRows(0);
        
        List<Object> contrato = new ContratoDAO().select();
                    
            contrato.forEach(objeto ->{ Contrato contratoConsulta = (Contrato) objeto;
                
                if(contratoConsulta.getIdContrato() > 1 && contratoConsulta.getVenda().getIdVenda() > 1){
                    
                modelo.addRow(new Object[]{
                   contratoConsulta.getIdContrato(),
                   contratoConsulta.getOrcamento().getCliente().getIdCliente(),
                   contratoConsulta.getVenda().getIdVenda(),
                   contratoConsulta.getOrcamento().getTipoDeContrato().getValor(),
                   contratoConsulta.getVenda().getProduto(),
                   contratoConsulta.getOrcamento().getLoja().getIdLoja(),
                   contratoConsulta.getOrcamento().getDataRetorno()
               });  
            
            }       
        });
    }
    private void ConsultaRegistros(){
        
        
        DefaultTableModel modelo = (DefaultTableModel) jTableBuscar.getModel();
        modelo.setNumRows(0);
        
        List<Object> registros = new RegistroDAO().select();
                    
            registros.forEach(objeto ->{ Registro registro = (Registro) objeto;
                                  
                modelo.addRow(new Object[]{
                   registro.getIdRegistro(),
                   registro.getIdDaTupla(),
                   registro.getIdFuncionario(),
                   registro.getNomeTabela(),
                   registro.getTipoDeRegistro(),
                   registro.getDataModificacao()
               });  
            
                   
        });
    }
     
   @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableBuscar = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jComboBoxConsulta = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Buscar");

        jTableBuscar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nome", "CPF", "Endereço", "Contato", "E-mail"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableBuscar.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTableBuscar);

        jButton1.setText("Atualizar");

        jComboBoxConsulta.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Clientes Fisicos", "Clientes Juridicos", "Orçamentos", "Contratos", "Carros", "Jetskis", "Registros" }));
        jComboBoxConsulta.setSelectedIndex(-1);
        jComboBoxConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxConsultaActionPerformed(evt);
            }
        });

        jButton2.setText("Voltar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 871, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBoxConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxConsultaActionPerformed
        if(jComboBoxConsulta.getSelectedItem().toString().equals("Clientes Fisicos")){
            ConsultaPessoaFisica();
        } else if(jComboBoxConsulta.getSelectedItem().toString().equals("Carros")){
            ConsultaCarro();
        } else if (jComboBoxConsulta.getSelectedItem().toString().equals("Jetskis")){
            ConsultaJetski();
        } else if (jComboBoxConsulta.getSelectedItem().toString().equals("Clientes Juridicos")){
            ConsultaPessoaJuridica();
        } else if (jComboBoxConsulta.getSelectedItem().toString().equals("Orçamentos")){
            ConsultaOrcamentos();
        } else if (jComboBoxConsulta.getSelectedItem().toString().equals("Contratos")){
            ConsultaContratos();
        } else if (jComboBoxConsulta.getSelectedItem().toString().equals("Registros")){
            ConsultaRegistros();
        }
    }//GEN-LAST:event_jComboBoxConsultaActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
        new TelaInicial().setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BuscarDados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BuscarDados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BuscarDados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BuscarDados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BuscarDados().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBoxConsulta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableBuscar;
    // End of variables declaration//GEN-END:variables
}
