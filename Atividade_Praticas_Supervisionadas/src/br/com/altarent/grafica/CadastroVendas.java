/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.altarent.grafica;

import br.com.altarent.dao.CarroDAO;
import br.com.altarent.dao.ClienteDAO;
import br.com.altarent.dao.JetskiDAO;
import br.com.altarent.dao.LojaDAO;
import br.com.altarent.dao.ProdutoDAO;
import br.com.altarent.dao.VendaDAO;
import br.com.altarent.modelo.Carro;
import br.com.altarent.modelo.Cliente;
import br.com.altarent.modelo.Jetski;
import br.com.altarent.modelo.Loja;
import br.com.altarent.modelo.Produto;
import br.com.altarent.modelo.Venda;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author ALTAMIRBITENCOURTJUN
 */
public class CadastroVendas extends javax.swing.JFrame {
    
    private Cliente clienteVenda = new Cliente();
    private String cliente = this.cliente;
    private Loja lojaVenda;
    private Produto produto;
    private int selecao;

    /**
     * Creates new form CadastroVendas
     */
    public CadastroVendas() {
        initComponents();
        VendaProduto();
        BuscaGerais();
        

    }
    private void VendaProduto(){
        String[] options = new String[2];
        options[0] = "Carro";
        options[1] = "Jetski";
        selecao = JOptionPane.showOptionDialog(rootPane,"Realizando venda, selecione \n o pro"
                + "produto","Venda", 0,JOptionPane.INFORMATION_MESSAGE,null,options,null);
        
        if(selecao == 0){
            jComboBoxProduto.setSelectedIndex(0);
            jComboBoxProduto.disable();
        } else if (selecao == 1){
            jComboBoxProduto.setSelectedIndex(1);
            jComboBoxProduto.disable();
        }
    }
    private void BuscaCarros(){

        List<Produto> carroFab = new ProdutoDAO().selectCarrosFabMaiorSeisAnos();
            
            carroFab.forEach(produtoConsulta -> {
            jComboBoxModelo.addItem(produtoConsulta.getCarro().getModeloTerrestre().getNomeModelo());});
    }
    private void BuscaJetski(){
                
        List<Produto> jetskiFab = new ProdutoDAO().selectJetskisFabMaiorSeisAnos();
            
            jetskiFab.forEach(objeto -> {
            jComboBoxModelo.addItem(objeto.getJetski().getModeloAquatico().getNomeModelo());});
    }
    private void BuscaGerais(){
        
        jComboBoxFuncionario.addItem(TelaLogin.funcionario.getNome());
            
        List<Loja> lojaLista = new LojaDAO().selectLoja();
            
            lojaLista.forEach(local -> {
            jComboBoxLoja.addItem(String.valueOf(local.getIdLoja()));       
        });
            
    }
    private void ConsultaCliente(){
        List<Cliente> clienteLista = new ClienteDAO().selectCliente();
        
        clienteLista.forEach(objeto -> {
            
                if(objeto.getPessoaFisica().getCpf().equals(jTextFieldCliente.getText())){
                    clienteVenda = objeto;
                    this.cliente = objeto.getPessoaFisica().getCpf();
                jTextFieldCliente.setText(clienteVenda.getPessoaFisica().getNome());
            }         
 
});
        
        if(clienteVenda == null){
            int novoCliente = JOptionPane.showConfirmDialog(rootPane, "Cliente não cadastrado, \n\n Deseja cadastrar ?");
            switch (novoCliente) {
                case 1:
                    dispose();
                    new TipoDeCliente().setVisible(true);
                    break;
                case 0:
                    dispose();
                    new CadastroOrcamentoCarro().setVisible(true);
                    break;
                default:
                    dispose();
                    break;
            }
        }
        
    }
    private void IdentificacaoCarro(){
               
        List<Carro> carroLista = new CarroDAO().selectCarro();
            
            carroLista.forEach(carro -> {
                     if(jComboBoxModelo.getSelectedItem().toString().equals(carro.getModeloTerrestre().getNomeModelo())){
                     jComboBoxIdentificacao.addItem(carro.getPlaca());
                    }
    });
    }
    private void IdentificacaoJetski(){
        
        List<Jetski> jetskiLista = new JetskiDAO().selectJetski();
        
        jetskiLista.forEach(jetski -> {
            
                if(jComboBoxModelo.getSelectedItem().toString().equals(jetski.getModeloAquatico().getNomeModelo())){
                    jComboBoxIdentificacao.addItem(String.valueOf(jetski.getDocumentoMarinha()));
                }
            });
        
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBoxProduto = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jComboBoxLoja = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jComboBoxFuncionario = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldCliente = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButtonVenda = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jComboBoxModelo = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldValor = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jComboBoxIdentificacao = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jComboBoxProduto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Carro", "Jetski" }));
        jComboBoxProduto.setSelectedIndex(-1);
        jComboBoxProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxProdutoActionPerformed(evt);
            }
        });

        jLabel7.setText("Loja");

        jLabel9.setText("Funcionário");

        jLabel8.setText("Cliente");

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButtonVenda.setText("Vender");
        jButtonVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVendaActionPerformed(evt);
            }
        });

        jLabel10.setText("Modelo do Veiculo");

        jComboBoxModelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxModeloActionPerformed(evt);
            }
        });

        jLabel3.setText("Selecione o Produto");

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Venda");

        jButton3.setText("Voltar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton3)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setText("Valor Atual");

        jTextFieldValor.setEditable(false);

        jLabel11.setText("Identificação");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBoxProduto, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBoxModelo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jComboBoxIdentificacao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(10, 10, 10)
                                .addComponent(jTextFieldValor, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBoxLoja, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel9)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jComboBoxFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(23, 23, 23)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTextFieldCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jButton1))
                                    .addComponent(jLabel8))))
                        .addContainerGap(11, Short.MAX_VALUE))))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(jLabel1)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBoxModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextFieldValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jComboBoxIdentificacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel3)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1)
                            .addComponent(jComboBoxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxLoja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jButtonVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ConsultaCliente();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jComboBoxProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxProdutoActionPerformed
        switch (jComboBoxProduto.getSelectedIndex()) {
            case 0:
                jComboBoxModelo.removeAllItems();
                BuscaCarros();
                break;
            case 1:
                jComboBoxModelo.removeAllItems();
                BuscaJetski();
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione um produto!");
                break;
        }
    }//GEN-LAST:event_jComboBoxProdutoActionPerformed

    private void jButtonVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVendaActionPerformed
       
        lojaVenda = new LojaDAO().selectLoja(Integer.parseInt(jComboBoxLoja.getSelectedItem().toString()));
        produto = new ProdutoDAO().selectProdutoCarroPlaca(jComboBoxIdentificacao.getSelectedItem().toString());
        Venda venda = new Venda(produto, TelaLogin.getFuncionario(), clienteVenda, lojaVenda);
        
        new VendaDAO(venda).insert(TelaLogin.funcionario.getIdFuncionario());
        
        JOptionPane.showMessageDialog(rootPane, "Venda realizada com sucesso!");
        dispose();
        new TelaInicial().setVisible(true);
    }//GEN-LAST:event_jButtonVendaActionPerformed

    private void jComboBoxModeloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxModeloActionPerformed
            
        if(jComboBoxProduto.getSelectedItem().toString().equals("Carro")){
            jComboBoxIdentificacao.removeAllItems();
            IdentificacaoCarro();
            produto = new ProdutoDAO().selectProdutoCarroPlaca(jComboBoxIdentificacao.getSelectedItem().toString());
            jTextFieldValor.setText(String.valueOf(produto.getCarro().getValorAtual()));
        } else {
            jComboBoxIdentificacao.removeAllItems();
            IdentificacaoJetski();
            produto = new ProdutoDAO().selectProdutoJetDocumento(Integer.parseInt(jComboBoxIdentificacao.getSelectedItem().toString()));
            jTextFieldValor.setText(String.valueOf(produto.getJetski().getValorAtual()));
        }   
    }//GEN-LAST:event_jComboBoxModeloActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        dispose();
        new TelaInicial().setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed


    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
            java.awt.EventQueue.invokeLater(() -> {
            new CadastroVendas().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButtonVenda;
    private javax.swing.JComboBox<String> jComboBoxFuncionario;
    private javax.swing.JComboBox<String> jComboBoxIdentificacao;
    private javax.swing.JComboBox<String> jComboBoxLoja;
    private javax.swing.JComboBox<String> jComboBoxModelo;
    private javax.swing.JComboBox<String> jComboBoxProduto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextFieldCliente;
    private javax.swing.JTextField jTextFieldValor;
    // End of variables declaration//GEN-END:variables
}
