package br.com.altarent.grafica;

import br.com.altarent.dao.CarroDAO;
import br.com.altarent.dao.ClienteDAO;
import br.com.altarent.dao.ContratoDAO;
import br.com.altarent.dao.LojaDAO;
import br.com.altarent.dao.ModeloTerrestreDAO;
import br.com.altarent.dao.OrcamentoDAO;
import br.com.altarent.modelo.Carro;
import br.com.altarent.modelo.Cliente;
import br.com.altarent.modelo.Diaria;
import br.com.altarent.modelo.Loja;
import br.com.altarent.modelo.Mensal;
import br.com.altarent.modelo.ModeloTerrestre;
import br.com.altarent.modelo.Orcamento;
import br.com.altarent.modelo.Semanal;
import br.com.altarent.modelo.TipoDeContrato;
import java.time.LocalDate;
import java.util.List;
import javax.swing.JOptionPane;

public class CadastroOrcamentoCarro extends javax.swing.JFrame {
          
    private Cliente clienteOrcamento = new Cliente();
    private TipoDeContrato idTipoDeContrato;
    private int idOrcamento = 1;
    private Orcamento orcamentoContrato;
    private String cliente;
    private Semanal semanal;
    private Diaria diaria;
    private Mensal mensal;
    private LocalDate dataRetornoUpdate;
    private int quantidade;
    
    
    

    public CadastroOrcamentoCarro() {
        initComponents();
        BuscaGerais();
        BuscaCarros();
    }
    private void BuscaCarros(){

        List<Object> carroLista = new ModeloTerrestreDAO().select();
            
            carroLista.forEach(objeto -> {ModeloTerrestre modeloTerrestre = (ModeloTerrestre) objeto;
            jComboBoxModeloVeiculo.addItem(modeloTerrestre.getNomeModelo());});
    }
    private void IdentificacaoCarro(){
               
        List<Object> carroLista = new CarroDAO().select();
            
            carroLista.forEach(objeto -> {
                
                Carro carro = (Carro) objeto;
            
                    if(jComboBoxModeloVeiculo.getSelectedItem().toString().equals(carro.getModeloTerrestre().getNomeModelo())){
                     jComboBoxIdentificacao.addItem(carro.getPlaca());
                    }
    });
    }
    private void BuscaGerais(){
        
        jComboBoxFuncionario.addItem(TelaLogin.funcionario.getNome());
            
        List<Object> lojaLista = new LojaDAO().select();
            
            lojaLista.forEach(loja -> {Loja local = (Loja) loja;
            jComboBoxLoja.addItem(String.valueOf(local.getIdLoja()));});
    }
    private void ConsultaCliente(){
        List<Object> cliente = new ClienteDAO().select();
        
        cliente.forEach(objeto -> {
            
            Cliente cl = (Cliente) objeto;
            
            if(jRadioButtonPJ.isSelected() && cl.getPessoaJuridica().getCnpj().equals(jTextFieldCliente.getText())){
                clienteOrcamento = cl;
                this.cliente = cl.getPessoaJuridica().getCnpj();
                jTextFieldCliente.setText(clienteOrcamento.getPessoaJuridica().getRazaoSocial());
                
            } else if(jRadioButtonPF.isSelected() && cl.getPessoaFisica().getCpf().equals(jTextFieldCliente.getText())){
                clienteOrcamento = cl;
                this.cliente = cl.getPessoaFisica().getCpf();
                jTextFieldCliente.setText(clienteOrcamento.getPessoaFisica().getNome());
            }         
 
        ;});
        
        if(clienteOrcamento == null){
            int novoCliente = JOptionPane.showConfirmDialog(rootPane, "Cliente não cadastrado, \n\n Deseja cadastrar ?");
            switch (novoCliente) {
                case 1:
                    dispose();
                    new TipoDeCliente().setVisible(true);
                    break;
                case 0:
                    dispose();
                    new CadastroOrcamentoCarro().setVisible(true);
                    break;
                default:
                    dispose();
                    break;
            }
        }
        
    }
    private void Data(){
        
        int anoEntrega = Integer.parseInt(jComboBoxAnoEntraga.getSelectedItem().toString());
        int mesEntrega = Integer.parseInt(jComboBoxMesEntraga.getSelectedItem().toString());
        int diaEntrega = Integer.parseInt(jComboBoxDiaEntraga.getSelectedItem().toString());
        LocalDate dataEntrega = LocalDate.of(anoEntrega, mesEntrega, diaEntrega);
        
        int anoRetorno = Integer.parseInt(jComboBoxAnoRetorno.getSelectedItem().toString());
        int mesRetorno = Integer.parseInt(jComboBoxMesRetorno.getSelectedItem().toString());
        int diaRetorno = Integer.parseInt(jComboBoxDiaRetorno.getSelectedItem().toString());
        LocalDate dataRetorno = LocalDate.of(anoRetorno, mesRetorno, diaRetorno);  
        
        quantidade = dataRetorno.getDayOfYear() - dataEntrega.getDayOfYear();
        jTextFieldQuantidade.setText(String.valueOf(quantidade));
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jComboBoxDiaEntraga = new javax.swing.JComboBox<>();
        jComboBoxMesEntraga = new javax.swing.JComboBox<>();
        jComboBoxAnoEntraga = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jComboBoxDiaRetorno = new javax.swing.JComboBox<>();
        jComboBoxMesRetorno = new javax.swing.JComboBox<>();
        jComboBoxAnoRetorno = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jComboBoxContrato = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jComboBoxLoja = new javax.swing.JComboBox<>();
        jLabelFuncionario = new javax.swing.JLabel();
        jComboBoxFuncionario = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldCliente = new javax.swing.JTextField();
        jButtonBuscarCliente = new javax.swing.JButton();
        jButtonGerarOrcamento = new javax.swing.JButton();
        jLabelModelo = new javax.swing.JLabel();
        jComboBoxModeloVeiculo = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldFinal = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldQuantidade = new javax.swing.JTextField();
        jComboBoxIdentificacao = new javax.swing.JComboBox<>();
        jLabelModelo1 = new javax.swing.JLabel();
        jRadioButtonPJ = new javax.swing.JRadioButton();
        jRadioButtonPF = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        jButtonData = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel4.setText("Data de Entrega");

        jComboBoxDiaEntraga.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" }));
        jComboBoxDiaEntraga.setSelectedIndex(-1);

        jComboBoxMesEntraga.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        jComboBoxMesEntraga.setSelectedIndex(-1);

        jComboBoxAnoEntraga.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2018", "2019" }));
        jComboBoxAnoEntraga.setSelectedIndex(-1);

        jLabel5.setText("Data de Retorno");

        jComboBoxDiaRetorno.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" }));
        jComboBoxDiaRetorno.setSelectedIndex(-1);

        jComboBoxMesRetorno.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        jComboBoxMesRetorno.setSelectedIndex(-1);

        jComboBoxAnoRetorno.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2018", "2019" }));
        jComboBoxAnoRetorno.setSelectedIndex(-1);

        jLabel6.setText("Tipo de Contrato");

        jComboBoxContrato.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mensal", "Semanal", "Diaria" }));
        jComboBoxContrato.setSelectedIndex(-1);
        jComboBoxContrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxContratoActionPerformed(evt);
            }
        });

        jLabel7.setText("Loja");

        jComboBoxLoja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxLojaActionPerformed(evt);
            }
        });

        jLabelFuncionario.setText("Funcionário");

        jComboBoxFuncionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxFuncionarioActionPerformed(evt);
            }
        });

        jLabel8.setText("Cliente");

        jTextFieldCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldClienteActionPerformed(evt);
            }
        });

        jButtonBuscarCliente.setText("OK");
        jButtonBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBuscarClienteActionPerformed(evt);
            }
        });

        jButtonGerarOrcamento.setText("Gerar");
        jButtonGerarOrcamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGerarOrcamentoActionPerformed(evt);
            }
        });

        jLabelModelo.setText("Modelo do Veiculo");

        jComboBoxModeloVeiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxModeloVeiculoActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(51, 255, 51));

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Orçamento do Carro");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jLabel9.setText("Valor Final");

        jTextFieldFinal.setEditable(false);
        jTextFieldFinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldFinalActionPerformed(evt);
            }
        });

        jLabel10.setText("Quantidade");

        jTextFieldQuantidade.setEditable(false);

        jComboBoxIdentificacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxIdentificacaoActionPerformed(evt);
            }
        });

        jLabelModelo1.setText("Identificação");

        jRadioButtonPJ.setText("Pessoa Juridica");
        jRadioButtonPJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonPJActionPerformed(evt);
            }
        });

        jRadioButtonPF.setText("Pessoa Fisica");
        jRadioButtonPF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonPFActionPerformed(evt);
            }
        });

        jButton1.setText("Voltar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButtonData.setText("Calcula");
        jButtonData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonDataMouseClicked(evt);
            }
        });
        jButtonData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDataActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jTextFieldCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonBuscarCliente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBoxDiaEntraga, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBoxMesEntraga, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBoxAnoEntraga, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(345, 345, 345))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jComboBoxModeloVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBoxIdentificacao, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelModelo)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelModelo1)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelFuncionario)
                                    .addComponent(jComboBoxFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBoxDiaRetorno, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxMesRetorno, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBoxAnoRetorno, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonData)
                                .addGap(11, 11, 11))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jRadioButtonPJ)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jRadioButtonPF)))
                                .addGap(37, 37, 37)
                                .addComponent(jLabel4)
                                .addGap(148, 148, 148)
                                .addComponent(jLabel5))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jComboBoxLoja, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                                    .addComponent(jTextFieldQuantidade))
                                .addGap(22, 22, 22)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jComboBoxContrato, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButtonGerarOrcamento)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton1))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(22, 22, 22)
                                        .addComponent(jLabel6)))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jRadioButtonPJ)
                            .addComponent(jRadioButtonPF))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jLabel4))
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxDiaEntraga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxMesEntraga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxAnoEntraga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonBuscarCliente)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxDiaRetorno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxMesRetorno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxAnoRetorno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonData))))
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelModelo1)
                    .addComponent(jLabelFuncionario)
                    .addComponent(jLabelModelo))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jComboBoxFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxModeloVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBoxIdentificacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel7))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(jLabel6))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jComboBoxContrato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonGerarOrcamento, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxLoja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(9, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonGerarOrcamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGerarOrcamentoActionPerformed
        
       
        int anoEntrega = Integer.parseInt(jComboBoxAnoEntraga.getSelectedItem().toString());
        int mesEntrega = Integer.parseInt(jComboBoxMesEntraga.getSelectedItem().toString());
        int diaEntrega = Integer.parseInt(jComboBoxDiaEntraga.getSelectedItem().toString());
        LocalDate dataEntrega = LocalDate.of(anoEntrega, mesEntrega, diaEntrega);
        
        int anoRetorno = Integer.parseInt(jComboBoxAnoRetorno.getSelectedItem().toString());
        int mesRetorno = Integer.parseInt(jComboBoxMesRetorno.getSelectedItem().toString());
        int diaRetorno = Integer.parseInt(jComboBoxDiaRetorno.getSelectedItem().toString());
        LocalDate dataRetorno = LocalDate.of(anoRetorno, mesRetorno, diaRetorno);     

        Orcamento orcamento = new Orcamento();
        orcamento.setLoja(new Loja());
        orcamento.getLoja().setIdLoja(Integer.parseInt(jComboBoxLoja.getSelectedItem().toString()));
        orcamento.setDataEntrega(dataEntrega);
        orcamento.setDataRetorno(dataRetorno);

        this.dataRetornoUpdate = dataRetorno;

                
        int contrato = JOptionPane.showConfirmDialog(rootPane, "Deseja gerar Contrato ?");
        
        if(contrato == 0){
                this.idOrcamento = new OrcamentoDAO(orcamento).insertOrcamentoCarro(
                        TelaLogin.funcionario.getIdFuncionario(), this.cliente, jComboBoxIdentificacao.getSelectedItem().toString(), idTipoDeContrato);
                this.orcamentoContrato = orcamento;
                new ContratoDAO().insertContratoOrcamentoCarro(TelaLogin.funcionario.getIdFuncionario(), 
                        this.idOrcamento, jComboBoxIdentificacao.getSelectedItem().toString(), this.dataRetornoUpdate, this.idTipoDeContrato.getValor());
                
                JOptionPane.showMessageDialog(rootPane, "Contrato cadastrado com sucesso!!");
        } 
        else if(contrato == 1){
                this.idOrcamento = new OrcamentoDAO(orcamento).insertOrcamentoCarro(
                        TelaLogin.funcionario.getIdFuncionario(), this.cliente, jComboBoxIdentificacao.getSelectedItem().toString(), idTipoDeContrato);
                this.orcamentoContrato = orcamento;
                JOptionPane.showMessageDialog(rootPane, "Orçamento salvo com Sucesso!");
                dispose();
                new TelaInicial().setVisible(true);
        } else{
                dispose();
                new CadastroOrcamentoCarro().setVisible(true);
        }
        
    }//GEN-LAST:event_jButtonGerarOrcamentoActionPerformed

    private void jTextFieldClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldClienteActionPerformed

    private void jButtonBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBuscarClienteActionPerformed
            ConsultaCliente();
    }//GEN-LAST:event_jButtonBuscarClienteActionPerformed

    private void jComboBoxContratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxContratoActionPerformed
        // TODO add your handling code here:
        Data();
        switch (jComboBoxContrato.getSelectedItem().toString()) {
            case "Diaria":
                {
                    diaria = new Diaria(Byte.parseByte(jTextFieldQuantidade.getText()), 
                            new CarroDAO().selectValorAluguel(jComboBoxIdentificacao.getSelectedItem().toString()));
                    diaria.calculaValorCarro();
                    jTextFieldFinal.setText(String.valueOf(diaria.getPrecoFinal()));
                    semanal = new Semanal();
                    semanal.setId(1);
                    mensal = new Mensal();
                    mensal.setId(1);
                    
                    TipoDeContrato tipodecontrato = new TipoDeContrato(diaria, semanal, mensal);
                    tipodecontrato.setValor(diaria);
                    
                    this.idTipoDeContrato = tipodecontrato;
                    break;
                }
            case "Semanal":
                {
                    semanal = new Semanal(Byte.parseByte(jTextFieldQuantidade.getText()), 
                            new CarroDAO().selectValorAluguel(jComboBoxIdentificacao.getSelectedItem().toString()));
                    
                    semanal.calculaValorCarro();
                    jTextFieldFinal.setText(String.valueOf(semanal.getPrecoFinal()));
                    diaria = new Diaria();
                    diaria.setId(1);
                    
                    mensal = new Mensal();
                    mensal.setId(1);
                            
                    TipoDeContrato tipodecontrato = new TipoDeContrato(diaria, semanal, mensal);
                    tipodecontrato.setValor(semanal);
                    
                    this.idTipoDeContrato = tipodecontrato; 
                    break;
                }
            case "Mensal":
                {
                    mensal = new Mensal(Byte.parseByte(jTextFieldQuantidade.getText()), 
                            new CarroDAO().selectValorAluguel(jComboBoxIdentificacao.getSelectedItem().toString()));
                    mensal.calculaValorCarro();
                    jTextFieldFinal.setText(String.valueOf(mensal.getPrecoFinal()));                                    
                    diaria = new Diaria();
                    diaria.setId(1);
                    
                    semanal = new Semanal();
                    semanal.setId(1);
                                    
                    TipoDeContrato tipodecontrato = new TipoDeContrato(diaria, semanal, mensal);
                    tipodecontrato.setValor(mensal);
                    
                    this.idTipoDeContrato = tipodecontrato;
                    break;
                }
        }
    }//GEN-LAST:event_jComboBoxContratoActionPerformed

    private void jComboBoxModeloVeiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxModeloVeiculoActionPerformed
        jComboBoxIdentificacao.removeAllItems();
        IdentificacaoCarro();
    }//GEN-LAST:event_jComboBoxModeloVeiculoActionPerformed

    private void jComboBoxFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxFuncionarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxFuncionarioActionPerformed

    private void jTextFieldFinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldFinalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldFinalActionPerformed

    private void jRadioButtonPJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonPJActionPerformed
    }//GEN-LAST:event_jRadioButtonPJActionPerformed

    private void jComboBoxLojaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxLojaActionPerformed
    }//GEN-LAST:event_jComboBoxLojaActionPerformed

    private void jComboBoxIdentificacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxIdentificacaoActionPerformed
   
    }//GEN-LAST:event_jComboBoxIdentificacaoActionPerformed

    private void jRadioButtonPFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonPFActionPerformed
    }//GEN-LAST:event_jRadioButtonPFActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
        new TelaInicial().setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButtonDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDataActionPerformed
        Data();
    }//GEN-LAST:event_jButtonDataActionPerformed

    private void jButtonDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonDataMouseClicked
        Data();
    }//GEN-LAST:event_jButtonDataMouseClicked

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            new CadastroOrcamentoCarro().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonBuscarCliente;
    private javax.swing.JButton jButtonData;
    private javax.swing.JButton jButtonGerarOrcamento;
    private javax.swing.JComboBox<String> jComboBoxAnoEntraga;
    private javax.swing.JComboBox<String> jComboBoxAnoRetorno;
    private javax.swing.JComboBox<String> jComboBoxContrato;
    private javax.swing.JComboBox<String> jComboBoxDiaEntraga;
    private javax.swing.JComboBox<String> jComboBoxDiaRetorno;
    private javax.swing.JComboBox<String> jComboBoxFuncionario;
    private javax.swing.JComboBox<String> jComboBoxIdentificacao;
    private javax.swing.JComboBox<String> jComboBoxLoja;
    private javax.swing.JComboBox<String> jComboBoxMesEntraga;
    private javax.swing.JComboBox<String> jComboBoxMesRetorno;
    private javax.swing.JComboBox<String> jComboBoxModeloVeiculo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelFuncionario;
    private javax.swing.JLabel jLabelModelo;
    private javax.swing.JLabel jLabelModelo1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButtonPF;
    private javax.swing.JRadioButton jRadioButtonPJ;
    private javax.swing.JTextField jTextFieldCliente;
    private javax.swing.JTextField jTextFieldFinal;
    private javax.swing.JTextField jTextFieldQuantidade;
    // End of variables declaration//GEN-END:variables
}
