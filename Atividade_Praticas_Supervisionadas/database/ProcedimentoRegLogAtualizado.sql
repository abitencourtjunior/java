use altarentdb;

DELIMITER $$
create procedure guardaLog(in idFuncionario int, in tabela varchar(45), in comando varchar(45), in idTupla int)
begin
	set idFuncionario = idFuncionario;
    set tabela = tabela;
    set comando = comando;
    set idTupla = idTupla;
    
    insert into Registro(DataModificacao, Hora, idFuncionario, NomeTabela, TipoDeRegistro, idDaTupla)
    values (now(), now(), idFuncionario, tabela, comando, idTupla);
end $$
DELIMITER ;



insert into Contato (telefoneprincipal, telefonesecundario, `e-mail`) 
values ('481111-2222', '482222-3333', 'null@null.com');

insert into Endereco (NomeRua, CEP, Bairro, Cidade, Complemento, Numero)
values ('Rua Fictícia', '88100-000', 'Longe', 'Cidadezinha', 'Null', '123');

insert into Autenticacao (Usuario, Senha, DataCriacao, TipoUsuario)
values ('a', 'a', now(), 'UsuarioNull');

insert into Disponivel (Disponivel) value ('Null'), ('Disponível'), ('Reservado'), ('Indisponível');

insert into Marca (Nome) values ('Null'), ('Chevrolet'), ('Fiat'), ('Ford'), ('Volkswagen'), ('Sea-Doo'), ('Kawasaki'), ('Yamaha');

insert into EstadoDeUso (EstadoDeUso) value ('Null'), ('Ótimo'), ('Lataria Riscada'), ('Batido'), ('Novo');

insert into Manutencao (TipoManutencao, DataManutencao, Kilometragem, ValorManutencao, Descricao)
values ('Null', now(), '0', 0.0, 'Null'); 

insert into Promocional (Hash, Usado, Validade) 
values ('Null', true, now());

insert into PessoaFisica (Nome, DataNascimento, CPF, Sexo, CNH, CHA, CategoriaHab, Contato_idContato, Endereco_idEndereco)
values ('Fulano', now(), '111.222.333-45', 'M', '123', '321', 'A', 1, 1);

insert into PessoaJuridica (Nome, NomeFantasia, CNPJ, PessoaFisica_idPessoaFisica, Endereco_idEndereco, Contato_idContato, Seguradora)
values ('Fulana', 'Fulana & Beltrana', '00.000.000/0000-00', 1, 1, 1, true);

insert into Cliente (PessoaFisica_idPessoaFisica, PessoaJuridica_idPessoaJuridica, Promocional_idPromocional, Autenticacao_idAutenticacao)
values (1,1,1,1);

insert into Cargo (Cargo, Salario) values ('Null', 0.0), ('Diretor', 3000), ('Gerente', 2500), ('Locador', 1700), ('Mecanico', 1850), ('Lavador', 1200);

insert into Loja (Endereco_idEndereco, Contato_idContato) values (1,1);

insert into Seguro (Valor, PessoaJuridica_idPessoaJuridica) values (0.0, 1);

insert into ModeloAquatico (NomeModelo, PotenciaMotor, TipoMotor, Lugares, TipoDeConducao, TipoDeIgnicao, TipoDeAgua, VolumeTanque, Marca_idMarca)
values ('Null', 0, 0, 0, 'Null', 'Null', 'Null', 0, 1);

insert into ModeloTerrestre (NomeModelo, Cambio, ArCondicionado, Direcao, VidrosEletricos, Portas, TravaEletrica, AirBag, ABS, CapacidadePessoas, LitragemPortaMalas, Marca_idMarca)
values ('Null', 'N', false, 'Null', false, 2, false, false, false, 5, 100, 1);

insert into Jetski (DocumentoMarinha, Cor, Seguro_idSeguro, Manutencao_idManutencao, ValorAluguel, DataFabricacao, ValorDeCompra, ValorAtual, TotalGerado, EstadoDeUso_idEstadoDeUso, ModeloAquatico_idModeloAquatico)
values ('01234567891', 'Null', 1, 1, 0.0, 2010, 0.0, 0.0, 0.0, 1, 1);

insert into Carro (Chassi, DataFabricacao, Placa, Cor, Seguro_idSeguro, Kilometragem, Manutencao_idManutencao, ValorAluguel, Renavam, ValorDeCompra, ValorAtual, TotalGerado, EstadoDeUso_idEstadoDeUso, ModeloTerrestre_idModeloTerrestre)
values ('01234567891', 2010, 'AAA-1234', 'Null', 1, 0, 1, 0.0, '1234567891', 0.0, 0.0, 0.0, 1, 1);

insert into Disponibilidade (Data, Hora, Disponivel_idDisponivel) values (now(), now(), 1);

insert into Produto (Carro_idCarro1, Jetski_idJetski, Disponibilidade_idDisponibilidade) values (1,1,1);

insert into Diaria (QtdeDias, ValorFinal) values (1, 0.0);

insert into Semanal (QtdeSemanas, ValorFinal) values (1, 0.0);

insert into Mensal (QtdeMes, ValorFinal) values (1, 0.0);

insert into TipoDeContrato (Diaria_idDiaria, Semanal_idSemanal, Mensal_idMensal, Valor) values (1,1,1,0.0);

insert into Funcionario (Nome, CPF, DataNascimento, Sexo, Endereco_idEndereco, Contato_idContato, Cargo_idCargo, Autenticacao_idAutenticacao)
values ('Beltrano', '987.654.321-09', now(), 'M', 1, 1, 1, 1);

insert into Venda (Produto_idProduto, Funcionario_idFuncionario, Cliente_idCliente, Loja_idLoja)
values (1,1,1,1);

insert into Orcamento (Produto_idProduto, TipoDeContrato_idTipoDeContrato, Loja_idLoja, Funcionario_idFuncionario, Cliente_idCliente, DataEntrega, DataRetorno)
values (1,1,1,1,1, now(), now());

insert into Contrato (Orcamento_idOrcamento, Venda_idVenda) values (1,1);