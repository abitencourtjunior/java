/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio4;

import java.util.Scanner;

/**
 *
 * @author altamir
 * 
 * 25. Uma companhia de seguros possui um número indeterminado de corretores. A companhia
    paga para o salário de cada corretor na forma de comissão, calculada de acordo com a venda
    mensal do corretor. Essa comissão é de 35% do valor da venda, se esse valor for maior que R$
    3000,00; 20% do valor da venda, se esse valor estiver entre R$ 1500,00 e R$ 3000,00; e 13% do
    valor da venda, se este valor estiver abaixo de R$ 1500,00. Construa um algoritmo que: a)
    Mostre o salário (comissão) de cada corretor; b) Calcule e mostre o total de vendas da
    companhia; Calcule e mostre o total geral de salários (comissões) pagos aos corretores.
 */

public class Exercicio25 {
    
    public static void main(String[] args) {
        
        Scanner n = new Scanner(System.in);
        int  corretores=0, salario=0;
        float  n1=0,n2=0,vendas_max=0,md_salario=0,comissao = 0;
        String  nome, nome_max="a";
        
        System.out.print("Digite a quantidade de  corretores: ");
        corretores = n.nextInt();
            
            for (int i=0;i<corretores;i++){
                    
                    System.out.print("Digite o nome do corretor: ");
                    nome = n.next();
                                                              
                            System.out.print("Digite o valor da venda:");
                            n1 = n.nextFloat();
                    
                                if ( n1 >=3000) {
                                    comissao = n1*35/100;
                                    System.out.println("Você irá ganhar R$ "+comissao+" da venda.");
                                } else if (n1 >= 1500 && n1 < 3000) {
                                    comissao = n1*20/100;
                                    System.out.println("Você irá ganhar R$ "+comissao+" da venda.");
                                } else if (n1 < 1500){
                                    comissao = n1*13/100;
                                    System.out.println("Você irá ganhar R$ "+comissao+" da venda.");
                                }
                            
                                System.out.println("Corretor: "+nome+" o seu salário é de R$ "+comissao);

                                vendas_max = vendas_max + n1;
                                md_salario = md_salario + comissao;

            }
        
        System.out.println("O total de vendas foi de R$"+vendas_max+" o total da comissao foi de: R$ "+md_salario);
        
    }
    
}
