/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio4;

import java.util.Scanner;

/**
 *
 * @author altamir
 */
public class Exercicio19 {
    
    public static void main(String[] args) {
        
        int n1=0,n2=1,aux=0,termo=0;
        Scanner ler = new Scanner(System.in);
        
        System.out.println("Digite o termo desejado: ");
        termo = ler.nextInt();
        
        if (termo == 0){
            System.out.println("O termo desejado é "+n2);
        }
        else {
                      
        for ( int i=0; i < termo;i++){

            aux = n1 + n2;
            n1 = n2;
            n2 = aux;
            
         }
         
        System.out.println("O termo desejado é "+aux);
        
        }
    }
    
}
