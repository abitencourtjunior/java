/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio4;

/**
 *
 * @author altamir
 */
import java.util.Scanner;

public class Exercicio7 {
    
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        int a=0,b=0,c=0;
        
        System.out.println("Digite três numeros inteiros");
        a = ler.nextInt();
        b = ler.nextInt();
        c = ler.nextInt();
        
        if ((a==b)&&(b==c)){
            System.out.println("O triângulo é Equilátero");
        } else if (((a==b)&&(b!=c))||((a==c)&&(a!=b))||((b==c)&&(b!=a))){
            System.out.println("O triângulo é Isóceles");
        } else if ((a!=b)&&(a!=c)){
            System.out.println("O triângulo é escaleno");
        }
    }
    
}
