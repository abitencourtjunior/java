/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio4;

import java.util.Scanner;


/**
 *
 * @author altamir
 */
public class Exercicio17 {
    
    public static void main(String[] args) {
        
        Scanner n = new Scanner(System.in);
        int aux=0;
        float n1=0,n2=0,total=0;
        
        System.out.println("** Calculadora **");
        System.out.println("*****************");
        System.out.println("* 1 - Soma ******");
        System.out.println("* 2 - Subtração *");
        System.out.println("* 3 - Produto ***");
        System.out.println("* 4 - Divisão ***");
        System.out.println("* 5 - Potência **");
        aux = n.nextInt();
        
        switch (aux) {
            case 1: 
                System.out.println("Digite dois valores: ");
                n1 = n.nextFloat();
                n2 = n.nextFloat();
                
                total = n1 + n2;
                System.out.println("O total da operação é de: "+total);
                break;
            case 2:
                System.out.println("Digite dois valores: ");
                n1 = n.nextFloat();
                n2 = n.nextFloat();
                
                total = n1 - n2;
                System.out.println("O total da operação é de: "+total);
                break;
            case 3:
                System.out.println("Digite dois valores: ");
                n1 = n.nextFloat();
                n2 = n.nextFloat();
                
                total = n1 * n2;
                System.out.println("O total da operação é de: "+total);
                break;
            case 4:
                System.out.println("Digite dois valores: ");
                n1 = n.nextFloat();
                n2 = n.nextFloat();
                
                    while (n2==0){
                        System.out.println("Digite novamente o 2º valor: ");
                        n2 = n.nextFloat();
                    }
                
                total = n1 / n2;
                System.out.println("O total da operação é de: "+total);
                break;
                
            case 5: 
                System.out.println("Digite dois valores: ");
                n1 = n.nextFloat();
                n2 = n.nextFloat();
                
               total = (float) Math.pow(n1, n2);
               System.out.println("O total da operação é de: "+total);
               break;
               
            default: 
                System.out.println("Opção inválida!!");
        }
        
    }
    
}
