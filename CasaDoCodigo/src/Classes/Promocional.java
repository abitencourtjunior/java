package Classes;

public interface Promocional {

    boolean aplicarDesconto(double porcentagem);

    default boolean aplicarDescontoDe10Porcento() {
        return aplicarDesconto(0.1);
    }
}
