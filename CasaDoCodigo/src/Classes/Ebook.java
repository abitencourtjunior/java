package Classes;

public class Ebook extends Livro implements Promocional{

    public Ebook(String nome, double valor, String isbn, Autor autor){
        super(nome, valor, isbn, autor);
    }

    @Override
    public boolean aplicarDesconto(double porcentagem) {

        if (porcentagem > 0.15) {
            return false;
        }

        System.out.println("Aplicado Desconto - Ebook");
        this.setValor(this.getValor() - this.getValor() * porcentagem);
        return true;
    }

    @Override
    public boolean aplicarDescontoDe10Porcento() {
        this.setValor(this.getValor() - this.getValor() * 0.1);
        return true;
    }
}
