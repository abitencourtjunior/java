package Classes;

import java.util.ArrayList;
import java.util.List;

public class CarrinhoDeCompras {

    private double total;
    private List<Livro> carrinhoDeCompras = new ArrayList<Livro>();

    public double adicionarLivro(Livro livro){
        System.out.println("Adicionado com sucesso: " + livro.getNome());
        this.carrinhoDeCompras.add(livro);
        this.total += livro.getValor();
        return this.total;
    }

    public void finalizarCompra(){
        System.out.println(" O total dos produtos foi de :"+this.total);
    }


    public void mostrarCarrinho(){
        for (int i=0; i<carrinhoDeCompras.size();i++){
            System.out.println("Produto: "+this.carrinhoDeCompras.get(i).getNome() + "\n" + this.carrinhoDeCompras.get(i).getValor());
        }
    }
}
