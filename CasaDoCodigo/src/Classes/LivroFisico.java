package Classes;

public class LivroFisico extends Livro implements Promocional{

    public LivroFisico(String nome, double valor, String isbn, Autor autor){
        super(nome, valor, isbn, autor);
    }

    @Override
    public boolean aplicarDesconto(double porcentagem) {
        if(porcentagem > 0.3){
            return false;
        }
        this.setValor(this.getValor() - this.getValor() * porcentagem);
        return true;
    }

    public double getTaxaDeImpressao(){
        return this.getValor() * 0.05;
    }

    @Override
    public boolean aplicarDescontoDe10Porcento() {
        return false;
    }
}
