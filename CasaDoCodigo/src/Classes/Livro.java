package Classes;

public abstract class Livro implements Produto{

    private String nome;
    private double valor;
    private String isbn;
    private Autor autor;

    public Livro(String nome, double valor, String isbn, Autor autor){
        this.nome = nome;
        this.valor = valor;
        this.isbn = isbn;
        this.autor = autor;
    }


    public void setNome(String nome){
        this.nome = nome;
    }

    public String getNome(){
        return this.nome;
    }

    public double getValor() {
        return this.valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public void mostraDados(){
        System.out.println(this.nome);
        System.out.println("R$ "+this.valor);
        System.out.println(this.isbn);
        System.out.println(this.autor.getNome());
    }

}
