import Classes.*;

public class RegistroDeVendas {

    public static void main(String[] args) {

        Autor autor = new Autor("Altamir","111.222.333-45","altamir@altamir.com.br");
        LivroFisico teste = new LivroFisico("O começo do Java",40.00,"XMA-0001", autor);
        Ebook teste1 = new Ebook("Procurando a Cobra", 60.00,"XMA-0002", autor);

        System.out.println(teste1.getValor());
        teste1.aplicarDescontoDe10Porcento();
        System.out.println(teste1.getValor());


    }
}
